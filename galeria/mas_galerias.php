<section class="mas_fotogalerias">

<?php
$id_post = get_the_id();

add_filter('posts_fields', function ($fields, $query) {
	global $wpdb;

	if ($query->get('limit_fields')) {
    	$fields = "$wpdb->posts.ID, $wpdb->posts.post_title, $wpdb->posts.post_name";
	}

	return $fields;
}, 10, 2);

add_action('pre_get_posts', function ($query) {
    $query->set('posts_per_page', 3);
    return;
});

$args = array(
	'post_type'                   => 'galeria',
	'posts_per_page'              => 3,
	'post_status'                 => 'publish',
	'post__not_in'                => array($id_post),
	'no_found_rows'               => true,
	'update_post_thumbnail_cache' => true,
	'limit_fields'                => true
);

if (has_term('pepe-antartico', 'categoriagaleria') || has_term('cuartoons', 'categoriagaleria')) {
	if (has_term('pepe-antartico', 'categoriagaleria')) {
		$slug_termino = 'pepe-antartico';
	} else {
		$slug_termino = 'cuartoons';
	}

	$args['tax_query'] = array(
		array(
			'taxonomy' => 'categoriagaleria',
			'field'    => 'slug',
			'terms'    => $slug_termino
		)
	);
} else {
	$args['tax_query'] = array(
		array(
			'taxonomy' => 'categoriagaleria',
			'field'    => 'slug',
			'terms'    => array('pepe-antartico', 'cuartoons'),
			'operator' => 'NOT IN'
		)
	);
}

query_posts($args);

if (have_posts()) :
	if (isset($slug_termino)) {
		$url = get_term_link($slug_termino, 'categoriagaleria');
	} else {
		$url = get_post_type_archive_link('galeria');
	}
?>

	<div class="row">
		<span class="tit">Otras galerías</span>
		<a class="mas_fotog" href="<?php echo $url; ?>">Ver más galerías</a>
	</div>
	<div class="row">

	<?php
	while (have_posts()) : the_post();
	?>

		<article class="column four">
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">

		<?php
		if (has_post_thumbnail()) {
			$tamano_imagen = 'thumbnail';
			$thumb_imagen = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()), $tamano_imagen);
			$url_imagen = $thumb_imagen['0'];
		} else {
			$imagenes = get_field('galeria');

			if ($imagenes) {
				$tamano = 'thumbnail';
				$url_imagen = $imagenes[0]['sizes'][$tamano];
			}
		}

		if (has_post_thumbnail() || $imagenes) :
		?>

				<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $url_imagen; ?>" alt="<?php the_title(); ?>" width="380" height="270" />

		<?php
		endif;
		?>

				<h3><?php the_title(); ?></h3>
			</a>
		</article>

<?php
	endwhile;
	wp_reset_query();
endif;
?>

	</div>
</section>