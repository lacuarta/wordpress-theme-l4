<?php
$imagenes = get_field('galeria');

if ($imagenes) :
	$numero_foto = 1;
	$total_fotos = count($imagenes);
	$numero_aleatorio = rand(0, 9999);
?>

<div id="slider-fotogaleria" class="owl-carousel wrapper">

	<?php
	foreach ($imagenes as $imagen) :
		if (isset($imagen['url'])) :
	    	$url_imagen = $imagen['url'];
	?>

	<div class="foto">

			<?php
			if (is_singular('galeria') && $_GET['vista'] != 'iframe') :
			?>

		<a class="fancybox" href="<?php echo $url_imagen; ?>" rel="galeria" title="<?php if (isset($imagen['description'])) : echo $imagen['description']; endif; ?>">

			<?php
			endif;
			?>

		<img class="lazyOwl" src="<?php echo s3uri(); ?>/img/transparent.gif" data-src="<?php echo $url_imagen; ?>" alt="<?php if (isset($imagen['description'])) : echo $imagen['description']; endif; ?>">

			<?php
			if (is_singular('galeria') && $_GET['vista'] != 'iframe') :
			?>

		</a>

			<?php
			endif;
			?>

		<div  class="row txt_num">
			<div class="bl">
				<p class="column"><?php if (isset($imagen['description'])) : echo $imagen['description']; endif; ?></p>
				<div class="numero column">Foto <?php echo $numero_foto; ?> de <?php echo $total_fotos; ?></div>
			</div>
		</div>
	</div>

	<?php
			$numero_foto++;
		endif;
	endforeach;
	?>

</div>
<div id="slider-miniaturas-<?php the_id(); ?><?php echo $numero_aleatorio; ?>" class="owl-carousel oculta slider-miniaturas">

	<?php
	foreach ($imagenes as $imagen) :
		$tamano = 'thumbnail';
		if (isset($imagen['sizes'])) :
			$url_imagen = $imagen['sizes'][$tamano];
	?>

	<img class="lazyOwl" src="<?php echo s3uri(); ?>/img/transparent.gif" data-src="<?php echo $url_imagen; ?>" alt="" width="380" height="270" />

	<?php
		endif;
	endforeach;
	?>

</div>

	<?php
	if (!isset($_GET['vista']) || $_GET['vista'] != 'iframe') :
	?>

<a id="mostrar-miniaturas-<?php the_id(); ?><?php echo $numero_aleatorio; ?>" href="#" title="Miniaturas" class="bot miniaturas mostrar-miniaturas">Miniaturas</a>

	<?php
	endif;
	?>

<script>
jq(document).ready(function() {
	var sync1 = jq("#slider-fotogaleria");

	sync1.owlCarousel({
		singleItem : true,
	    lazyLoad : true,
        autoHeight: true,
	    navigation: true,

	<?php
	if (!isset($_GET['vista']) || $_GET['vista'] != 'iframe') :
	?>

	    afterAction : syncPosition,

	<?php
	endif;
	?>

	    responsiveClass : true,
        itemsCustom : [
        	[0, 1],
            [450, 1],
            [800, 1],
            [1200, 1]
        ]
	});


	<?php
	if (!isset($_GET['vista']) || $_GET['vista'] != 'iframe') :
	?>

	var sync2 = jq("#slider-miniaturas-<?php the_id(); ?><?php echo $numero_aleatorio; ?>");

	sync2.owlCarousel({
	    items : 6,
	    lazyLoad : true,
	    navigation: true,
	    afterInit : function(el) {
	    	el.find(".owl-item").eq(0).addClass("synced");
	   	}
	});

	function syncPosition(el){
		var current = this.currentItem;
	    jq("#slider-miniaturas-<?php the_id(); ?><?php echo $numero_aleatorio; ?>")
	    	.find(".owl-item")
	      	.removeClass("synced")
	      	.eq(current)
	      	.addClass("synced");

	    if (jq("#slider-miniaturas-<?php the_id(); ?><?php echo $numero_aleatorio; ?>").data("owlCarousel") !== undefined) {
	    	center(current);
	    }
	}

	jq("#slider-miniaturas-<?php the_id(); ?><?php echo $numero_aleatorio; ?>").on("click", ".owl-item", function(e) {
	    e.preventDefault();
	    var number = jq(this).data("owlItem");
	    sync1.trigger("owl.goTo",number);
	});

	function center(number) {
	    var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
	    var num = number;
	    var found = false;
	    for (var i in sync2visible) {
	    	if (num === sync2visible[i]) {
	        	found = true;
	      	}
	    }

	    if (found === false) {
	    	if (num > sync2visible[sync2visible.length-1]) {
	        	sync2.trigger("owl.goTo", num - sync2visible.length+2);
	   		} else {
		    	if (num - 1 === -1) {
		        	num = 0;
		        }
		        sync2.trigger("owl.goTo", num);
	    	}
	    } else if (num === sync2visible[sync2visible.length-1]) {
	    	sync2.trigger("owl.goTo", sync2visible[1]);
	    } else if (num === sync2visible[0]) {
	    	sync2.trigger("owl.goTo", num-1);
	    }
	}

	<?php
	endif;
	?>

});

	<?php
	if (!isset($_GET['vista']) || $_GET['vista'] != 'iframe') :
	?>

jq(document).on('click', '#mostrar-miniaturas-<?php the_id(); ?><?php echo $numero_aleatorio; ?>', function(e) {
	e.preventDefault();

	jq('#slider-miniaturas-<?php the_id(); ?><?php echo $numero_aleatorio; ?>').toggleClass('oculta');
	jq('#mostrar-miniaturas-<?php the_id(); ?><?php echo $numero_aleatorio; ?>').toggleClass('abierto');
});

	<?php
	endif;
	?>

</script>

<?php
endif;
?>