<?php
$imagenes = get_field('galeria');

if ($imagenes) :
?>

<div class="grid">

	<?php
	foreach ($imagenes as $imagen) :
		$tamano = 'formato-xs';
	    $thumb = $imagen['sizes'][$tamano];
	    $url_imagen = $imagen['url'];
	?>

	<div>
		<div class="grid-item">

		<?php
		if (!isset($_GET['vista']) || $_GET['vista'] != 'iframe') :
		?>

			<a class="fancybox" href="<?php echo $url_imagen; ?>" rel="galeria" title="<?php echo $imagen['description']; ?>">

		<?php
		endif;
		?>

				<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $thumb; ?>" alt="<?php echo $imagen['description']; ?>" width="390">

		<?php
		if (!isset($_GET['vista']) || $_GET['vista'] != 'iframe') :
		?>

			</a>

		<?php
		endif;
		?>

		</div>
	</div>

	<?php
	endforeach;
	?>

</div>

<?php
endif;
?>

<script>
jq(document).ready(function() {
	var grid = document.querySelector('.grid');
	var masonry = new Masonry(grid, {transitionDuration: 0});
	masonry.layout();

	function doMasonry() {
		masonry.layout();
	}

	jq('img.lazy').lazyload({
		threshold : 500,
		load: function() {
			jq(this).parent().removeClass('unloaded');
			window.setTimeout(doMasonry, 100);
		}
	});

	var posicion_vertical = jq(window).scrollTop();
	setTimeout(function() { jq(window).scrollTop(posicion_vertical + 2); }, 1000);
});
</script>