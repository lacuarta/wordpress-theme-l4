<?php
get_header();

if (have_posts()) :
	while (have_posts()) : the_post();
?>

<article class="ficha fotogaleria <?php if (isset($_GET['vista']) && $_GET['vista']=='iframe')  :?>iframe<?php endif; ?>">

		<?php
		if (!isset($_GET['vista']) || $_GET['vista'] != 'iframe' ) :
		?>

	<header class="cabecera_ficha row">
		<div class="categoria">Galerías</div>
		<h1><?php the_title(); ?></h1>
	</header>

		<?php
		endif;
		?>

	<div class="cuerpo">

	    <?php
	    if (!isset($_GET['vista']) || $_GET['vista'] != 'iframe') :
	    	$id_post = get_the_id();
	    	$titulo = get_the_title();
	    	$permalink = get_permalink();
	    ?>

		<div class="row opciones_galeria">
			<div class="column eight compartir">
				<?php include(locate_template('content-compartir.php')); ?>
			</div>
			<ul class="tipogal column four">

			<?php
			$vista = isset($_GET['vista']) ? $_GET['vista'] : 'slider';
			?>

				<li class="slider<?php if ($vista =='slider') : echo ' activo'; endif; ?>"><a href="<?php the_permalink(); ?>" title="Slider">Slider</a></li>
				<li class="completa<?php if ($vista =='completa') : echo ' activo'; endif; ?>"><a href="<?php the_permalink(); ?>/?vista=completa" title="Vista completa">Vista completa</a></li>
				<li class="galeria<?php if ($vista =='galeria') : echo ' activo'; endif; ?>"><a href="<?php the_permalink(); ?>/?vista=galeria" title="Galería">Galería</a></li>
			</ul>
		</div>

        <?php
        endif;
        ?>

		<div>
			<div class="row row_gal">

		<?php
		if (isset($_GET['vista']) && $_GET['vista'] == 'iframe') :
		?>

	    	<div class="cabecera_flota">
				<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo s3uri(); ?>/img/logo.png" border="0" alt="<?php bloginfo('name'); ?>" width="205" height="50">
				<span><?php the_title();?></span>
			</div>

		<?php
		endif;

		if ($vista == 'completa') {
			get_template_part('galeria/completa', 'page');
		} elseif ($vista == 'galeria') {
			get_template_part('galeria/galeria', 'page');
		} else {
			get_template_part('galeria/slider', 'page');
		}
		?>

			</div>
			<script>
			jq(".fancybox").fancybox({
				helpers:  {
			        title: {
			            type: ''
			        },
		        	overlay: {
		           		showEarly: false
				    },
				    autoSize: false,
				    padding: 0
				}
			});
			</script>
		</div>
	</div>
</article>

<?php
	endwhile;
endif;

if (!isset($_GET['vista']) || $_GET['vista'] != 'iframe') {
	get_template_part('galeria/mas_galerias', 'page');
	get_footer();
}
?>