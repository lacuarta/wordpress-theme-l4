<header class="header_publirreportajes">
	<div class="header">
		<h1 class="logo">
			<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php bloginfo('name'); ?>">
				<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo s3uri(); ?>/img/logo.png" border="0" alt="<?php bloginfo('name'); ?>" width="206" height="50">
			</a>
		</h1>
		<div class="patrocinador">
			<p>Contenido escrito y pagado por:</p>
			<a href="<?php the_field('url_patrocinador'); ?>" target="_blank">

<?php
$imagen = get_field('logotipo');

if (($imagen)) :
	$tamano_imagen = 'large';
	$url_imagen = $imagen['sizes'][$tamano_imagen];
?>

				<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $url_imagen; ?>" alt="<?php echo $imagen['alt']; ?>" />

<?php
endif;
?>

			 </a>
		</div>
	</div>
</header>