<?php
$id_post = get_the_id();
$post_type = get_post_type();
$titulo = get_the_title();
$permalink = get_permalink();
$ids_slugs = wp_get_post_terms($id_post, 'categoria', array("fields" => "ids"));

if (get_field('utiliza_opta')) :
	wp_enqueue_style('opta-style', 'http://widget.cloud.opta.net/2.0/css/widgets.opta.css');
	wp_enqueue_script('opta-script', 'http://widget.cloud.opta.net/2.0/js/widgets.opta.js');
?>

<script>
var _optaParams = {
    custID:     '83f090439770a6e6cc0cb324504524e7',
    language:   'es'
};
</script>

<?php
endif;

if (get_field('scripts', $id_post)) {
	the_field('scripts', $id_post);
}
?>
<?php
/*
if (get_field('validador_publicidad', $id_post)) {
	$value = get_field('validador_publicidad', $id_post);
	//var_dump($value);
	if($value[0] == 'SI'){
	echo 'apto';
	}else{
	echo 'no apto';
	}
}*/
?>

<div class="bl_sky_completo">

<?php
if (!has_term('', 'especial')) :
	include(locate_template('noticia/noticia-esto_te_interesa.php'));
	$publicidad_tras_header = obtener_publicidad('publicidad_tras_header_categoria');

	if ($publicidad_tras_header) :
?>

	<div class="bl_publi_top bajo">
		<?php echo $publicidad_tras_header; ?>
	</div>

	<?php
	endif;

	$publicidad_sky = obtener_publicidad('publicidad_sky_categoria');

	if ($publicidad_sky) :
	?>

	<div class="row sky" style="position:relative;">
		<div class="publi_sky" style="background:#cdcdcd; position:absolute; left:-180px; top:110px; width:160px; height:600px;">
			<?php echo $publicidad_sky; ?>
		</div>
	</div>

<?php
	endif;
else :
	$publicidad_tras_header = obtener_publicidad('publicidad_tras_header_categoria');

	if ($publicidad_tras_header) :
	?>

	<div class="bl_publi_top bajo">
		<?php echo $publicidad_tras_header; ?>
	</div>

	<?php
	endif;

	$publicidad_sky = obtener_publicidad('publicidad_sky_categoria');

	if ($publicidad_sky) :
	?>

	<div class="row sky" style="position:relative;">
		<div class="publi_sky" style="background:#cdcdcd; position:absolute; left:-180px; top:110px; width:160px; height:600px;">
			<?php echo $publicidad_sky; ?>
		</div>
	</div>

	<?php
	endif;
	?>

	<div class="row cab_especial">

	<?php
	$lista_terminos = wp_get_post_terms($id_post, 'especial', array("fields" => "all"));
	$id_imagen_cabecera = get_field('imagen_cabecera', 'especial_' . $lista_terminos[0]->term_id);

	if ($id_imagen_cabecera) :
		$tamano = 'full';
		$thumb = wp_get_attachment_image_src($id_imagen_cabecera, $tamano)[0];
	?>


			<a href="<?php echo get_term_link($lista_terminos[0]->slug, 'especial'); ?>" alt="<?php echo $lista_terminos[0]->name; ?>"><img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $thumb; ?>" alt="<?php the_title(); ?>" /></a>

	<?php
	endif;

	if (have_rows('menu', 'especial_' . $lista_terminos[0]->term_id)) :
	?>

			<ul>

		<?php
		while (have_rows('menu', 'especial_' . $lista_terminos[0]->term_id)) : the_row();
		?>

				<li><a href="<?php the_sub_field('url'); ?>"><?php the_sub_field('titulo'); ?></a></li>

		<?php
		endwhile;
		?>

			</ul>

	<?php
	endif;
	?>

	</div>

<?php
endif;
?>
<?php if(wp_get_post_terms($id_post, 'categoria', array('fields' => 'slugs'))[0] == 'cero-emisiones'){
			echo "<div class='cabcero_emi'><img src='http://d2mhmd1y9dvsiv.cloudfront.net/wp-content/uploads/sites/5/2017/04/21/cero-emisiones-logo.png'></div>";
			}
			?>
	<article class="ficha">
		<header class="cabecera_ficha row">

<?php
if (have_rows('plantilla')) :
	while (have_rows('plantilla')) : the_row();
		if (get_row_layout('plantilla_directo')) :
			if (have_rows('directo')) :
?>

			<span class="directo">En directo</span>

<?php
			endif;
		endif;
	endwhile;
else :
?>
			
			
			
			<div class="categoria">
				<?php echo get_the_term_list($id_post, 'categoria', '', ', '); ?>
			</div>

<?php
endif;
?>

			<h1 class="titulo_ficha"><?php echo $titulo; ?></h1>

<?php
if (get_field('es_noticia_patrocinada')) :
	$id_imagen_logo = get_field('logo_patrocinador');
	$tamano_logo = 'large';
	$url_logo = wp_get_attachment_image_src($id_imagen_logo, $tamano_logo)[0];
?>

			<div class="patrocinada">
				<span>Este contenido está patrocinado por</span>

	<?php
	if (get_field('url_patrocinador')) :
	?>

				<a href="<?php the_field('url_patrocinador'); ?>" target="_blank">

	<?php
	endif;
	?>

					<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $url_logo; ?>" alt="<?php echo $titulo; ?>" />

	<?php
	if (get_field('url_patrocinador')) :
	?>

			</a>

	<?php
	endif;
	?>

			</div>

<?php
endif;
?>

		</header>
		<div class="row">
			<div class="cuerpo column<?php if (!get_field('tiene_ancho_completo')) : echo ' eight'; endif; ?>">
				<div>
					<?php include(locate_template('content-compartir.php')); ?>
				</div>

<?php
$tiene_ancho_completo = get_field('tiene_ancho_completo');

if ($tiene_ancho_completo) {
	$ancho_video = 1084;
	$alto_video = 723;

	if (get_field('dejar_proporcion_original_imagen_destacada')) {
		$tamano_imagen = 'medium';
	} else {
		$tamano_imagen = 'formato-s';
		$ancho_imagen = 720;
		$alto_imagen = 440;
	}
} else {
	$ancho_video = 704;
	$alto_video = 430;

	if (get_field('dejar_proporcion_original_imagen_destacada')) {
		$tamano_imagen = 'medium';
	} else {
		$tamano_imagen = 'formato-s';
		$ancho_imagen = 720;
		$alto_imagen = 440;
	}
}

if (!get_field('ocultar_imagen_destacada') && !get_field('key_video') && (get_field('imagen_principal') || has_post_thumbnail() || get_field('imagen_destacada_migracion'))) :
	if (get_field('url_imagen_destacada')) :
?>

				<a href="<?php the_field('url_imagen_destacada'); ?>" target="_blank">

	<?php
	endif;

	if ($imagen = get_field('imagen_principal')) :
		$url_imagen = $imagen['sizes'][$tamano_imagen];
	?>

					<div class="img_destacada">
						<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $url_imagen; ?>" alt="<?php echo $titulo; ?>" width="<?php echo $ancho_imagen; ?>" height="<?php echo $alto_imagen; ?>" />
					</div>

	<?php
	elseif (has_post_thumbnail()) :
		$thumb_imagen = wp_get_attachment_image_src(get_post_thumbnail_id($id_post), $tamano_imagen);
		$url_imagen = $thumb_imagen['0'];
	?>

					<div class="img_destacada">
						<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $url_imagen; ?>" alt="<?php echo $titulo; ?>" width="<?php echo $ancho_imagen; ?>" height="<?php echo $alto_imagen; ?>" />
					</div>

	<?php
	elseif (get_field('imagen_destacada_migracion')) :
	?>

					<div class="img_destacada">
						<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo get_field('imagen_destacada_migracion'); ?>" alt="<?php echo $titulo; ?>" width="<?php echo $ancho_imagen; ?>" height="<?php echo $alto_imagen; ?>" />
					</div>

	<?php
	endif;

	if (get_field('url_imagen_destacada')) :
	?>

				</a>

	<?php
	endif;

	if (!get_field('ocultar_lectura_de_foto_y_credito')) :
	?>

					<span class="lectura-foto"><?php the_field('lectura_de_foto_y_credito', get_post_thumbnail_id($id_post)); ?></span>

<?php
	endif;
elseif (get_field('key_video')) :
?>

				<div class="video-container">
					<iframe class="vrudo" src="http://rudo.video/vod/<?php echo get_field('key_video'); ?>" width="<?php echo $ancho_imagen; ?>" height="<?php echo $alto_video; ?>" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true" msallowfullscreen="true" frameborder="0" scrolling="no"></iframe>
				</div>

<?php
endif;
?>

				<h2 class="entradilla"><?php the_field('entradilla'); ?></h2>
				<div class="contenido">
					<div class="autor">

<?php
if (get_field('otro_autor_o_no_tiene')) :
	if (get_field('nombre_autor')) :
?>

						<p><?php the_date('d/m/Y'); ?> - Autor: <span><?php the_field('nombre_autor'); ?></span></p>

<?php
	endif;
else :
?>

						<p><?php the_date('d/m/Y'); ?> - Autor: <span><?php the_author(); ?></span></p>

	<?php
	if (get_the_author_meta('twitter')) :
	?>

						<a href="http://twitter.com/<?php echo get_the_author_meta('twitter'); ?>" target="_blank">@<?php echo get_the_author_meta('twitter'); ?></a>

<?php
	endif;
endif;
?>

					</div>
					<?php the_content(); ?>

<?php
if (get_field('html_migracion')) {
	the_field('html_migracion');
}

$galeria = get_field('galeria');
$galeria_migracion = get_field('otras_imagenes_migracion');

if ($galeria || $galeria_migracion) :
	if ($galeria) {
		$post = $galeria;
		setup_postdata($post);
		$imagenes = get_field('galeria');
	} elseif ($galeria_migracion) {
		$imagenes = explode(',', $galeria_migracion);
	}
?>

					<div class="fotogaleria-noticias">

	<?php
	if (is_array($imagenes)) :
		foreach ($imagenes as $imagen) :
			if ($galeria) {
				$tamano_imagen_slider = 'large';
				$url_imagen_slider = $imagen['sizes'][$tamano_imagen_slider];
			} elseif ($galeria_migracion) {
				$url_imagen_slider = $imagen;
			}
	?>

						<div>
							<img class="lazyOwl" src="<?php echo s3uri(); ?>/img/transparent.gif" data-src="<?php echo $url_imagen_slider;?>"/>
						</div>

	<?php
		endforeach;
	endif;
	?>

					</div>
					<script>
					jq(document).ready(function() {
					    jq(".fotogaleria-noticias").owlCarousel({
					        items: 1,
					        lazyLoad: true,
					        autoHeight:true,
					    	navigation: true,
					    	responsiveClass:true,
					        itemsCustom : [
					            [0, 1],
					            [450, 1],
					            [800, 1],
					            [1200, 1]
					          ]
						});
					});
					</script>

<?php
	if ($galeria) {
		wp_reset_postdata();
	}

endif;
?>

				</div>
				<div class="plantillas">

<?php
if (have_rows('plantilla')) :
	while (have_rows('plantilla')) : the_row();
		$plantilla_tipo = get_row_layout();

	 	switch ($plantilla_tipo) {
			case 'plantilla_directo':
			get_template_part('noticia/noticia-directo', 'page');
			break;
		}
	endwhile;
endif;
?>

				</div>

<!-- version antigua para llamar los relacionados, sigue funcional, se le hace refactor -->
<?php $noticias_relacionadas = get_field('noticias_relacionadas'); ?>
<?php if ($noticias_relacionadas): ?>
	<div class="relacionadas">
		<span>Noticias relacionadas</span>
		<ul class="fototexto">
			<?php foreach ($noticias_relacionadas as $post): ?>
				<?php
				setup_postdata($post);
				get_template_part('portada/modulos/content-l4aldia-noticia', 'page');
				wp_reset_postdata();
				?>
			<?php endforeach; ?>
		</ul>
	</div>
<?php endif; ?>

<!-- version nueva para llamar relacionados -->
<?php $noticiasRelacionadasPorCategoria = get_field('noticias_relacionadas_por_categorias'); ?>
<?php if ($noticiasRelacionadasPorCategoria): ?>
	<div class="relacionadas">
		<span>Noticias relacionadas</span>
		<ul class="fototexto">
			<?php foreach ($noticiasRelacionadasPorCategoria as $post): ?>
				<?php
				setup_postdata($post);
				get_template_part('portada/modulos/content-l4aldia-noticia', 'page');
				wp_reset_postdata();
				?>
			<?php endforeach; ?>
		</ul>
	</div>
<?php endif; ?>

<?php
//$id_autor = get_the_author_meta('ID');
?>

				<div class="face">
					<div class="fb-like" data-href="<?php echo $permalink; ?>" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
					<div id="fb-root"></div>
				</div>
				<script>
				(function(d, s, id) {
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id)) return;
					js = d.createElement(s); js.id = id;
					js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.6";
					fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
				</script>
			</div>

<?php
if (!$tiene_ancho_completo) :
?>

			<div class="sidebar column four">

	<?php
	$publicidad_lateral_derecho_zona_1 = obtener_publicidad('publicidad_lateral_derecho_zona_1_categoria');

	if (is_array($publicidad_lateral_derecho_zona_1)) :
		foreach ($publicidad_lateral_derecho_zona_1 as $publicidad_lateral_derecho) :
	?>

				<div class="bl_publi widget">
					<?php echo $publicidad_lateral_derecho['publicidad']; ?>
				</div>

	<?php
		endforeach;
	endif;
	?>

				<span class="titulo calentitos">Los más calentitos</span>
				<ul>
					<li><a id="mas-vistos" class="tab-los-mas" href="#" class="tit">Los más vistos</a></li>
					<li class="activo"><a id="mas-mejores" class="tab-los-mas" href="#" class="tit">Los más mejores</a></li>
				</ul>
				<script>
				jq(document).on('click', '.tab-los-mas', function(e) {
					e.preventDefault();

					jq(this).parent().parent().find('.activo').removeClass('activo');
					jq(this).parent().addClass('activo');

					if (jq(this).attr('id') == 'mas-vistos') {
						jq('.los-mas-mejores').fadeOut();
						jq('.los-mas-vistos').fadeIn();
					}

					if (jq(this).attr('id') == 'mas-mejores') {
						jq('.los-mas-vistos').fadeOut();
						jq('.los-mas-mejores').fadeIn();
					}
				});
				</script>
				<div class="los-mas-vistos" style="display:none;">

	<?php
	$html = apply_filters('ze_most_visited_html',array('conf'=>'2days_2'));
	echo $html;
	?>

				</div>
				<div class="los-mas-mejores">
					<?php get_template_part('content-los_mas_mejores', 'page'); ?>
				</div>

			</div>

<?php
endif;
?>

		</div>
	</article>

<?php
$publicidad_pie = obtener_publicidad('publicidad_pie_categoria');

if ($publicidad_pie) :
?>

	<div class="bl_publi_pie row">
		<?php echo $publicidad_pie; ?>
	</div>

<?php
endif;

get_template_part('portada/modulos/diario_version_digital', 'page');

$publicidad_pie_2 = obtener_publicidad('publicidad_pie_2_categoria');

if ($publicidad_pie_2) :
?>

	<div class="bl_publi_pie row">
		<?php echo $publicidad_pie_2; ?>
	</div>

<?php
endif;
?>

</div>