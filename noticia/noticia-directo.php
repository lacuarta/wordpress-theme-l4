<?php
if (have_rows('directo')) :
?>

<ul class="platilla_cronologia">

	<?php
	while (have_rows('directo')) : the_row();
	?>

	<li class="crono">
		<div class="cuando column two">

		<?php
		if (get_sub_field('fecha')) :
		?>

			<div class="dia"><?php the_sub_field('fecha'); ?></div>

		<?php
		endif;

		if (get_sub_field('hora')) :
		?>

			<div class="hora"><?php the_sub_field('hora'); ?></div>

		<?php
		endif;
		?>

		</div>
		<div class="column ten">
			<div class="txt">
				<h4><?php the_sub_field('titulo'); ?></h4>

		<?php
		if (get_sub_field('texto')) :
		?>

				<p><?php the_sub_field('texto'); ?></p>

		<?php
		endif;
		?>

			</div>

		<?php
		$id_imagen = get_sub_field('imagen');
		$ancho_imagen = 600;
		$alto_imagen = 400;

		if (!empty($id_imagen)) :
			$tamano = 'formato-s';
		    $thumb = wp_get_attachment_image_src($id_imagen, $tamano)[0];
		?>

			<div class="img">
				<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $thumb; ?>" alt="<?php the_sub_field('titulo'); ?>" width="720" height="440" />
			</div>

		<?php
		endif;

		if (get_sub_field('key_video')) :
		?>
			<div class="video-container">
				<iframe class="vrudo" src="http://rudo.video/vod/<?php echo get_sub_field('key_video'); ?>" width="<?php echo $ancho_imagen; ?>" height="<?php echo $alto_imagen; ?>" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true" msallowfullscreen="true" frameborder="0" scrolling="no"></iframe>
			</div>

		<?php
		endif;
		?>

		</div>
	</li>

	<?php
	endwhile;
	?>

</ul>

<?php
endif;
?>