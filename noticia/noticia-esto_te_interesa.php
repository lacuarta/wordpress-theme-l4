<?php
$args = array(
	'post_type'                   => 'noticia',
	'posts_per_page'              => 5,
	'post__not_in'                => array($id_post),
	'post_status'                 => 'publish',
	'no_found_rows'               => true,
	'update_post_thumbnail_cache' => true,
	'tax_query'                   => array(
		array(
			'taxonomy' => 'categoria',
			'field'    => 'id',
			'terms'    => $ids_slugs
		)
	)
);
query_posts($args);

if (have_posts()) :
?>

<div id="teinteresa">
	<div class="row ampliado">
		<p>Esto te interesa...</p>
		<ul class="destacados">

	<?php
	while (have_posts()) : the_post();
	?>

			<li>

		<?php
		if ($imagen = get_field('imagen_principal')) {
			$tamano_imagen = 'thumbnail';
			$url_imagen = $imagen['sizes'][$tamano_imagen];
		} elseif (has_post_thumbnail()) {
			$tamano_imagen = 'thumbnail';
			$thumb_imagen = wp_get_attachment_image_src(get_post_thumbnail_id(), $tamano_imagen);
			$url_imagen = $thumb_imagen['0'];
		} elseif (get_field('imagen_destacada_migracion')) {
			$url_imagen = get_field('imagen_destacada_migracion');
		} else {
			$url_imagen = ''; // Imagen generica
		}
		?>

				<a href="<?php echo get_permalink(); ?>">
					<h3><?php the_title(); ?></h3>
					<div class="img">
						<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $url_imagen; ?>" alt="<?php the_title(); ?>" width="380" height="270" />
					</div>
				</a>
			</li>

	<?php
	endwhile;
	wp_reset_query();
	?>
	</div>
</div>

<?php
endif;
?>
