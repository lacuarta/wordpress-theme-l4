<?php
if (get_field('imagen_principal') || has_post_thumbnail()) :
	if ($imagen = get_field('imagen_principal')) :
		$tamano_imagen = 'formato-xxl';
		$url_imagen = $imagen['sizes'][$tamano_imagen];
	elseif (has_post_thumbnail()) :
		$tamano_imagen = 'formato-xxl';
		$thumb_imagen = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()), $tamano_imagen);
		$url_imagen = $thumb_imagen['0'];
	endif;
?>

<div class="img<?php if (get_field('key_video')) : echo ' puntero noticia-imagen-' . get_the_id(); endif; ?>">
	<span class="video"></span>
	<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $url_imagen; ?>" alt="<?php the_title(); ?>" />

	<?php
	if (get_field('key_video')) :
	?>

	<div class="video-container noticia-video-<?php the_id(); ?>" style="display:none;"><iframe class="vrudo" src="http://rudo.video/vod/<?php echo get_field('key_video'); ?>" width="1200" height="965" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true" msallowfullscreen="true" frameborder="0" scrolling="no"></iframe></div>
	<script>
	jq('.noticia-imagen-<?php the_id(); ?>').click(function() {
		jq('img', jq(this)).hide();
		jq('span.video', jq(this)).hide();
		jq('.txt').hide();
		jq('.noticia-video-<?php the_id(); ?>').show();
	});
	</script>

	<?php
	endif;
	?>

</div>

<?php
endif;
?>

<div class="txt">
	<h3><?php the_title(); ?></h3>
</div>