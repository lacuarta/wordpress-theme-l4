<?php
get_header();

while (have_posts()) : the_post();
?>

<article id="content">
	<div class="row">
		<div id="post-<?php the_ID(); ?>" class="contenido">
			<h1 class="entry-title"><?php the_title(); ?></h1>
			<div class="entry-content">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
</article>

<?php
endwhile;

get_footer();
?>