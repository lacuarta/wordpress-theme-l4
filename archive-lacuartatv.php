<?php get_header(); ?>
<div id="content" class="portada lacuartatv">

<?php
if (have_posts()) :
?>

	<div class="row">
		<p class="titseccion">La cuarta TV</p>
	</div>
	<div>
		<div id="slider-lacuartatv" class="owl-carousel wrapper row">

	<?php
	$numero_post = 1;

	while (have_posts()) : the_post();
		if ($numero_post == 1) {
			$id_primer_post = get_the_id();
		}
	?>

			<div class="item igualar<?php if ($numero_post == 1) : echo ' seleccionada'; endif; ?>">

		<?php
		if ($imagen = get_field('imagen_principal')) {
			$tamano_imagen = 'thumbnail';
			$url_imagen = $imagen['sizes'][$tamano_imagen];
		} elseif (has_post_thumbnail()) {
			$tamano_imagen = 'thumbnail';
			$thumb_imagen = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()), $tamano_imagen);
			$url_imagen = $thumb_imagen['0'];
		} else {
			$url_imagen = s3uri() . '/img/sin_imagen.png';
		}
		?>

				<a href="<?php the_permalink();?>">
					<div>
						<span class="video"></span>
						<img class="lazyOwl" src="<?php echo s3uri(); ?>/img/transparent.gif" data-src="<?php echo $url_imagen; ?>" alt="<?php the_title(); ?>" width="380" height="270">
					</div>
					<p><?php the_title(); ?></p>
				</a>
			</div>

	<?php
		$numero_post++;
	endwhile;
	?>

		</div>
		<script>
		jq(document).ready(function() {
		    jq("#slider-lacuartatv").owlCarousel({
		        lazyLoad: true,
		    	navigation: true,
		    	responsiveClass:true,
		    	itemsCustom : [
			        [0, 2],
			        [400, 3],
			        [600, 4],
			        [800, 5],
			        [1200, 6]
		      	],
			});
		});
		</script>
	</div>

	<?php
	$publicidad_pre_video = obtener_publicidad('publicidad_pre_video');

	if ($publicidad_pre_video) :
	?>

	<div class="bl_publi_pie row">
		<?php echo $publicidad_pre_video; ?>
	</div>

	<?php
	endif;
	?>

	<div id="video-actual" class="row">

	<?php
	$args = array(
		'p'                           => $id_primer_post,
		'post_type'                   => 'lacuartatv',
		'post_status'                 => 'publish',
		'no_found_rows'               => true,
		'update_post_thumbnail_cache' => true
	);

	$noticias = new WP_Query($args);

	if ($noticias->have_posts()) :
		while ($noticias->have_posts()) : $noticias->the_post();
			get_template_part('content-lacuartatv-video_actual');
		endwhile;
		wp_reset_postdata();
	endif;
	?>

	</div>

	<?php
	$publicidad_post_video = obtener_publicidad('publicidad_post_video');

	if ($publicidad_post_video) :
	?>

	<div class="bl_publi_pie row">
		<?php echo $publicidad_post_video; ?>
	</div>

<?php
	endif;
endif;
?>

</div>
<?php get_footer(); ?>