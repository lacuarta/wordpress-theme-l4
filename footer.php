<?php
if (!is_singular('edicionimpresa')) :
?>

		<div id="slider-papeldigital">
			<div class="row ampliado">
				<p>Edición impresa</p>
			</div>
			<div class="row">
				<div class="owl-carousel">
					<!-- l4 -->
					<div>
						<a href="http://edition.pagesuite-professional.co.uk/html5/reader/production/default.aspx?pubid=a94a1c16-2ebc-4ecc-b2bc-d60709ea4c26" target="_blank">
							<img src="http://edition.pagesuite-professional.co.uk/get_image.aspx?w=200&pbid=a94a1c16-2ebc-4ecc-b2bc-d60709ea4c26" alt="Comerciante" width="94" height="137" />
						</a>
					</div>
					<!-- comerciante -->
					<div>
						<a href="http://edition.pagesuite-professional.co.uk/html5/reader/production/default.aspx?pubid=f88c92ca-1b7b-422d-8394-b8bf8f2c5281" target="_blank">
							<img src="http://edition.pagesuite-professional.co.uk/get_image.aspx?w=200&pbid=f88c92ca-1b7b-422d-8394-b8bf8f2c5281" alt="Comerciante" width="94" height="137" />
						</a>
					</div>
					<!-- constructor -->
					<div>
						<a href="http://edition.pagesuite-professional.co.uk/html5/reader/production/default.aspx?pubid=c460e84c-3320-47e6-89ee-317a4f600e53" target="_blank">
							<img src="http://edition.pagesuite-professional.co.uk/get_image.aspx?w=200&pbid=c460e84c-3320-47e6-89ee-317a4f600e53" alt="Comerciante" width="94" height="137" />
						</a>
					</div>

				</div>
			</div>
		</div>

<?php
endif;
?>

		<footer class="footer">
			<div class="row">
				<div class="column three"><a href="<?php echo esc_url(home_url('/')); ?>" title="<?php bloginfo('name'); ?>"><img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo s3uri(); ?>/img/logo.png" border="0" alt="<?php bloginfo('name'); ?>" width="205" height="50"></a></div>
				<div class="column nine">
					<?php wp_nav_menu(array('theme_location' => 'inferior')); ?>
				</div>
			</div>
			<div class="copesa">
				<div class="row">
					<div class="column two logo_copesa">
						<a href="#" title="grupo copesa"><img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo s3uri(); ?>/img/logo_copesa.png" border="0" alt="grupo copesa" width="95" height="63"></a>
					</div>
					<div class="column ten logos_pie">
						<a target="_blank" class="logo_latercera" href="http://www.latercera.com/" >logo_latercera</a>
						<a target="_blank" class="logo_lacuarta" href="http://www.lacuarta.com/" >logo_lacuarta</a>
						<a target="_blank" class="logo_pulso" href="http://www.pulso.cl/" >logo_pulso</a>
						<a target="_blank" class="logo_lahora" href="http://www.lahora.cl/" >logo_lahora</a>
						<a target="_blank" class="logo_diarioconcepcion" href="http://www.diarioconcepcion.cl/" >logo_diarioconcepcion</a>
						<a target="_blank" class="logo_quepasa" href="http://www.quepasa.cl/" >logo_quepasa</a>
						<a target="_blank" class="logo_paula" href="http://www.paula.cl/" >logo_paula</a>
						<a target="_blank" class="logo_hola" href="#" >logo_hola</a>
						<a target="_blank" class="logo_glamorama" href="http://www.glamorama.cl/" >logo_glamorama</a>
						<a target="_blank" class="logo_biut" href="http://www.biut.cl/" >logo_biut</a>
						<a target="_blank" class="logo_duna" href="http://www.duna.cl/" >logo_duna</a>
						<a target="_blank" class="logo_beethovenfm" href="http://www.beethovenfm.cl/" >logo_beethovenfm</a>
						<a target="_blank" class="logo_radiodisney" href="http://radiodisney.disneylatino.com/la/index.php?c=chile#!/categorias/16/destacados" >logo_radiodisney</a>
						<a target="_blank" class="logo_icarito" href="http://www.icarito.cl/" >logo_icarito</a>
						<a target="_blank" class="logo_zero" href="http://www.radiozero.cl/" >logo_zero</a>
					</div>
				</div>
			</div>
		</footer>
	</div><!-- Fin div.container -->
	
</div>
<?php wp_footer(); ?>
<?php
echo '<section class="modulo_vertical diario_version_digital_v">';
	echo get_site_option('html_diario_version_digital_vertical');
	echo "</section>";?>

	?>


<script>
/*
jq( ".papel_digital_vertical a" ).mouseover(function() {
	var link = jq(this).attr('href');
	var image = jq('img', this).attr("src");
	jq.fancybox.open('<div><a href="'+link+'" target="_blank"><img style="width:700px; height:100%;" src="'+image+'"/></a></div>');

	setTimeout(function() { jq.fancybox.close() }, 2000);
});
*/
$("#slider-papeldigital .owl-carousel").owlCarousel({
	  	items: 5,
	  	itemsDesktop: [1150,5],
      	itemsDesktopSmall: [950,4],
      	itemsTablet: [760,2],
      	itemsMobile: [500,1],
	    lazyLoad: true,
	    navigation: true,
		slideSpeed : 300,
        autoPlay: 2000,
	});
</script>

</body>
</html>