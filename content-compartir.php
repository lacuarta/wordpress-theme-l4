<ul class="redes row">
	<li class="facebook"><a href="https://www.facebook.com/sharer.php?u=<?php echo $permalink; ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" title="Compartir en Facebook" target="_blank"></a></li>
	<li class="twitter"><a href="https://twitter.com/share?url=<?php echo wp_get_shortlink($id_post); ?>&via=lacuarta&text=<?php echo urlencode(html_entity_decode($titulo, ENT_COMPAT, 'UTF-8'));
?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" title="Compartir en Twitter" target="_blank"></a></li>
	<li class="whatsapp"><a href="whatsapp://send?text=<?php echo urlencode(html_entity_decode($titulo, ENT_COMPAT, 'UTF-8')); ?> - <?php echo $permalink; ?>" data-action="share/whatsapp/share">Whatsapp</a></li>
	<li class="mail"><a href="mailto:?subject=Quiero compartir esta noticia contigo de <?php bloginfo('name'); ?>&body=<?php echo $titulo; ?> <?php echo $permalink; ?>" title="Enviar a un amigo" target="_blank">Email</a></li>
</ul>