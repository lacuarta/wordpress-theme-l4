<?php get_header(); ?>
<div id="content" class="portada portada_seccion">

<?php
if ($wp_query->queried_object->parent == 0) : // Categoria padre
	$taxonomia_actual = $wp_query->queried_object->taxonomy;
	$slug_termino_categoria_actual = $wp_query->queried_object->slug;
	$termino_categoria_actual = $wp_query->queried_object;

	$args_portada = array(
		'post_type'      => 'portada',
		'post_status'    => 'publish',
		'posts_per_page' => -1,
		'orderby'        => 'none',
		'no_found_rows'  => true,
		'cache_results'  => false
	);
	query_posts($args_portada);

	if (have_posts()) :
		while (have_posts()) :
			the_post();

			if (get_field('es_portada_categoria')) :
				$termino = get_field('categoria');

				if ($termino->slug == $slug_termino_categoria_actual) :
					include(locate_template('content-portada-categoria.php'));
					break;
				endif;
			endif;
		endwhile;

		wp_reset_query();
	endif;
endif;
?>

</div>
<?php get_footer(); ?>