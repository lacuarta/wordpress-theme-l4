<?php
$publicidad_top = obtener_publicidad('publicidad_top_categoria');

if ($publicidad_top && (!isset($_GET['vista']) || $_GET['vista'] != 'iframe')) :
?>

<div class="bl_publi_top">
	<?php echo $publicidad_top; ?>
</div>

<?php
endif;

if (!is_singular('publirreportajes') && (!isset($_GET['vista']) || $_GET['vista'] != 'iframe'))  :
?>

<header class="header_interior">
	<div class="header">
		<span class="logo"><a href="<?php echo esc_url(home_url('/')); ?>" title="<?php bloginfo('name'); ?>"><img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo s3uri(); ?>/img/logo.png" border="0" alt="<?php bloginfo('name'); ?>" width="206" height="50"></a></span>
	</div>
	<div class="subheader">
		<nav id="nav">
			<span class="bot_nav">Menú</span>
			<div class="nav_v">
				<?php wp_nav_menu(array('theme_location' => 'superior-horizontal', 'items_wrap' => nav_wrap_header_interior())); ?>
				<ul class="redes">
					<li class="twitter"><a href="https://twitter.com/lacuarta" target="_blank" title="Twitter">Twitter</a></li>
					<li class="facebook"><a href="https://www.facebook.com/pages/La-Cuarta-Cibernetica/85079739676" target="_blank" title="Facebook">Facebook</a></li>
					<li class="instagram"><a href="https://www.instagram.com/lacuartacom/" target="_blank" title="Instagram">Instagram</a></li>
					<li class="rss"><a href="<?php echo esc_url(home_url('/')); ?>/feed/?post_type=noticia" target="_blank" title="RSS">RSS</a></li>
				</ul>
			</div>
			<span class="bot_buscar"></span>
		</nav>
	</div>
	<div class="b_google">
		<span class="cerrar"></span>
		<gcse:searchbox-only></gcse:searchbox-only>
	</div>
	<div class="b_google_negro"></div>
</header>
<script>
var elementPosition = jq('body').offset();

jq(window).scroll(function() {
	var head = jq('body');

    if (jq(window).scrollTop() > elementPosition.top) {
        head.addClass('fixed');
    } else {
        head.removeClass('fixed');
    }
});
</script>

	<?php
	$publicidad_skin = obtener_publicidad('publicidad_skin_categoria');

	if ($publicidad_skin) :
		echo $publicidad_skin;
	endif;

	if (!is_singular('noticia')) :
		$publicidad_tras_header = obtener_publicidad('publicidad_tras_header_categoria');

		if ($publicidad_tras_header) :
	?>

<div class="bl_publi_top bajo">
	<?php echo $publicidad_tras_header; ?>
</div>

	<?php
		endif;

		$publicidad_sky = obtener_publicidad('publicidad_sky_categoria');

		if ($publicidad_sky) :
		?>

<div class="bl_sky_completo">
	<div class="row sky" style="position:relative;">
		<div class="publi_sky">
			<?php echo $publicidad_sky; ?>
		</div>
	</div>

<?php
		endif;
	endif;
endif;
?>