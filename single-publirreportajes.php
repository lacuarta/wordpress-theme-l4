<?php
get_header();

if (have_posts()) :
	while (have_posts()) : the_post();
?>

<div id="content">
	<article class="ficha publirreportajes">

		<?php
		if (has_post_thumbnail()) :
			if ($imagen = get_field('imagen_principal')) :
				$tamano_imagen = 'full';
				$url_imagen = $imagen['sizes'][$tamano_imagen];
			elseif (has_post_thumbnail()) :
				$tamano_imagen = 'full';
				$thumb = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()), $tamano_imagen);
				$url_imagen = $thumb['0'];
			endif;
		?>

		<div class="imgfondo" style="background-image:url(<?php echo $url_imagen; ?>);"></div>

		<?php
		endif;
		?>

		<div id="post-<?php the_ID(); ?>" class="contenido">
			<div class="cabecera_publi row">
				<div class="bl_txt">
					<h1 class="entry-title"><?php the_title(); ?></h1>
					<div class="entradilla"><?php the_field('entradilla'); ?></div>
				</div>
			</div>

		<?php
		$id_post = get_the_id();
		$titulo = get_the_title();
		$permalink = get_permalink();

		include(locate_template('content-compartir.php'));
		?>

			<div class="cuerpo_publi row">
				<div class="entry-content">
					<h2 class="bajada"><?php the_field('bajada'); ?></h2>
					<?php the_content(); ?>
				</div>
			</div>

		<?php
		if (have_rows('contenido_bloque')) :
		 	while (have_rows('contenido_bloque')) : the_row();
			 	$contenido_tipo = get_row_layout();

			 	switch ($contenido_tipo) :
					case 'bloque_texto':
		?>


			<div class="bloque-texto">
				<div class="row">
					<div class="entry-content"><?php the_sub_field('texto'); ?></div>
				</div>
			</div>

					<?php
					break;

					case 'bloque_imagen':
					?>

			<div class="ancho">

					<?php
					$imagen = get_sub_field('imagen');

					if ($imagen) :
						$tamano = 'large';
						$thumb = $imagen['sizes'][$tamano];
						$url_imagen = $imagen['url'];
					?>

				<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $thumb; ?>" border="0" alt="" />

					<?php
					endif;
					?>

			</div>

		<?php
					break;
				endswitch;
			endwhile;
		endif;
		?>

		</div>
	</article>
</div>

<?php
	endwhile;
endif;
?>

<?php get_footer(); ?>