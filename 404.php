<?php get_header(); ?>
<div class="header_espacio"></div>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<section class="error-404 not-found">
			<div class="content row">
				<div class="column six">
					<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo s3uri(); ?>/img/error_404.png" border="0" alt="error 404" width="471" height="239">
					<p>Este podría ser el resultado de una página cuya url no está bien digitada, el nombre del archivo haya cambiado o definitivamente no se encuentra disponible en el servidor.</p>
					<div class="b_google" style="display: block;">
						<gcse:searchbox-only></gcse:searchbox-only>
					</div>
					<a href="<?php echo esc_url(home_url('/')); ?>" class="bot">Visita nuestra página principal</a>
				</div>
				<div class="column six">

<?php
$ruletas = wp_cache_get('lacuarta_modulo_ruleta');

if (false === $ruletas) {
	$args = array(
		'post_type'                   => 'ruleta',
		'posts_per_page'              => 1,
		'post_status'                 => 'publish',
		'no_found_rows'               => true,
		'cache_results'               => false,
		'update_post_thumbnail_cache' => true
	);
	$ruletas = get_posts($args);

	wp_cache_set('lacuarta_modulo_ruleta', $ruletas);
}

if ($ruletas) :
	foreach ($ruletas as $ruleta) :
		if (have_rows('diosas', $ruleta->ID)) :
			$genero = 'diosas';
			include(locate_template('portada/modulos/content-superruleta-genero.php'));
			?>
			<div class="wrapviewmodal">
				<button id="closemodal">close</button>
				<div id="viewModal"></div>
			</div>
			<?php
		endif;
	endforeach;
endif;
?>

				</div>
			</div>
		</section>
		<section class="gris">
			<p class="row">O puedes echar un vistazo a...</p>
			<?php get_template_part('portada/modulos/los_mas_calentitos', 'page'); ?>
		</section>
	</main>
</div>
<?php get_footer(); ?>