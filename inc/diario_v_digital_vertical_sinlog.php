<?php
/*
Este archivo guarda en la tabla wp_options el html correspondiente al módulo de "Diario Version Digital"
Fecha creacion; 13/09/2016
Esta pensado para ser invocado desde el cron
Reprogramado por Juan Zamorano para trabajar con pressreader 22-11-2016
 */
include('../../../../wp-config.php');

$wp->init();
$wp->parse_request();
$wp->query_posts();
$wp->register_globals();


$contexto = stream_context_create(array(
    'http' => array(
        'timeout' => 60
        )
    )
);

$xml = file_get_contents('./ftp/Num_Pag/Tot_Pagina.xml', false, $contexto);
if ($xml) {

    $html = '<div class="row"><header><a href="http://diariolacuarta.pressreader.com/" target="_blank"><img src="http://d2mhmd1y9dvsiv.cloudfront.net/wp-content/uploads/sites/5/2017/03/24/Papel-Digital-3.png"></a>';
    $productos = new SimpleXMLElement($xml);

    foreach ($productos as $producto) {
        if ($producto->attributes()->NAME == 'lacuarta') {
            $fecha_edicion = $producto->EDITION_DATE;
            $numero_paginas = $producto->PAGE_NUMBER;
            break;
        }
    }

    if (is_int((int)$fecha_edicion) && is_int((int)$numero_paginas)) {
        $ano = substr($fecha_edicion, 0, 4);
        $mes = substr($fecha_edicion, 4, 2);
        $dia = substr($fecha_edicion, 6, 2);
        $dias = array('Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado');
        $meses = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
        $fecha_actual = $dias[date('w', strtotime($ano . '-' . $mes . '-' . $dia))] . ' ' . $dia . ' de ' . $meses[$mes - 1];

        $html .= '<span>' . $fecha_actual . '</span></header></div><div style="text-align: center; padding: 1rem 0; width: 100%;" class="row"><div class="papel_digital_vertical">';

        for ($i = 1; $i <= $numero_paginas; $i++) {
            if ($i < 10) {
                $numero_pagina_ceros = '0' . $i;
            } elseif ($i < 100) {
                $numero_pagina_ceros = $i;
            } else {
                $numero_pagina_ceros = $i;
            }
			
			$html .= '<div><a href="http://diariolacuarta.pressreader.com/la-cuarta/' . $ano.$mes.$dia . '/page/' . $numero_pagina_ceros . '" target="_blank" title="Ver en Pressreader"><img class="" src="http://cache-thumb1.pressdisplay.com/pressdisplay/docserver/getimage.aspx?cid=eb01&date=&width=300&page=' . $numero_pagina_ceros . '&nocache='.date('dmYH').'" data-src="http://cache-thumb1.pressdisplay.com/pressdisplay/docserver/getimage.aspx?cid=eb01&date=&width=300&page=' . $numero_pagina_ceros . '&nocache='.date('dmYH').'" title="P&aacute;gina ' . $i . '" alt="P&aacute;gina ' . $i . '" height="333" width="267"></a></div>';
        }

        $html .= '</div></div>';
        switch_to_blog(1);
        update_site_option('html_diario_version_digital_vertical', $html);
        restore_current_blog();
    }
}
?>
