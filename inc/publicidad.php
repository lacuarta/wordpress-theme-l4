<?php
/**
 * Obtiene la publicidad
 * @param  string $posicion     La posicion de la publicidad en la pagina
 * @param  int $id_post         El id del post
 * @param  string $post_type    El post type
 * @return string               La publicidad
 */
function obtener_publicidad($posicion) {
	if (is_home()) {
		if ($posicion == 'publicidad_top_categoria') {
			$publicidad = '<!--<div id="div-gpt-ad-1468338424102-17">' .
							'<script type="text/javascript">' .
							'googletag.cmd.push(function() { googletag.display("div-gpt-ad-1468338424102-17"); });' .
							'</script>' .
							'</div>-->' .
							'<div id="div-gpt-ad-1468338424102-18">' .
							'<script type="text/javascript">' .
							'googletag.cmd.push(function() { googletag.display("div-gpt-ad-1468338424102-18"); });' .
							'</script>' .
							'</div>' .
							'<div id="div-gpt-ad-1468338424102-0">' .
							'<script type="text/javascript">' .
							'googletag.cmd.push(function() { googletag.display("div-gpt-ad-1468338424102-0"); });' .
							'</script>' .
							'</div>' .
							'<div id="div-gpt-ad-1468338424102-19">' .
							'<script type="text/javascript">' .
							'googletag.cmd.push(function() { googletag.display("div-gpt-ad-1468338424102-19"); });' .
							'</script>' .
							'</div>';
		} elseif ($posicion == 'publicidad_sky_categoria') {
			$publicidad = '<div id="div-gpt-ad-1468338424102-20">' .
							'<script type="text/javascript">' .
							'googletag.cmd.push(function() { googletag.display("div-gpt-ad-1468338424102-20"); });' .
							'</script>' .
							'</div>';
		} elseif ($posicion == 'publicidad_tras_header_categoria') {
			$publicidad = '<div id="div-gpt-ad-1468338424102-16" style="min-height:1px; min-width:1px;">' .
							'<script type="text/javascript">' .
							'googletag.cmd.push(function() { googletag.display("div-gpt-ad-1468338424102-16"); });' .
							'</script>' .
							'</div>';
		}
	} elseif (is_tax('categoria')) {
		$id_termino_categoria = get_queried_object()->term_id;
		$publicidad = get_field($posicion, 'categoria_' . $id_termino_categoria);
	} elseif (is_singular('portada') && get_field('es_portada_categoria')) {
		$id_termino_categoria = get_field('categoria')->term_id;
		$publicidad = get_field($posicion, 'categoria_' . $id_termino_categoria);
	} elseif (is_singular('noticia')) {
		$terms = get_the_terms(get_the_id(), 'categoria');
		$id_termino_categoria = $terms[0]->term_id;
		$publicidad = get_field($posicion, 'categoria_' . $id_termino_categoria);
	} elseif (is_post_type_archive('lacuartatv') || is_singular('lacuartatv')) {
		if ($posicion == 'publicidad_top_categoria') {
			$publicidad = '<div id="div-gpt-ad-1468339996166-8" style="height:1px; width:1px;">' .
							'<script type="text/javascript">' .
							'googletag.cmd.push(function() { googletag.display("div-gpt-ad-1468339996166-8"); });' .
							'</script>' .
							'</div>' .
							'<div id="div-gpt-ad-1468339996166-9" style="height:1px; width:1px;">' .
							'<script type="text/javascript">' .
							'googletag.cmd.push(function() { googletag.display("div-gpt-ad-1468339996166-9"); });' .
							'</script>' .
							'</div>' .
							'<div id="div-gpt-ad-1468339996166-0">' .
							'<script type="text/javascript">' .
							'googletag.cmd.push(function() { googletag.display("div-gpt-ad-1468339996166-0"); });' .
							'</script>' .
							'</div>' .
							'<div id="div-gpt-ad-1468339996166-10" style="height:1px; width:1px;">' .
							'<script type="text/javascript">' .
							'googletag.cmd.push(function() { googletag.display("div-gpt-ad-1468339996166-10"); });' .
							'</script>' .
							'</div>';
		} elseif ($posicion == 'publicidad_tras_header_categoria') {
			$publicidad = '<div id="div-gpt-ad-1468339996166-7">' .
							'<script type="text/javascript">' .
							'googletag.cmd.push(function() { googletag.display("div-gpt-ad-1468339996166-7"); });' .
							'</script>' .
							'</div>';
		} elseif ($posicion == 'publicidad_sky_categoria') {
			$publicidad = '<div id="div-gpt-ad-1468339996166-11">' .
							'<script type="text/javascript">' .
							'googletag.cmd.push(function() { googletag.display("div-gpt-ad-1468339996166-11"); });' .
							'</script>' .
							'</div>';
		} elseif ($posicion == 'publicidad_pre_video') {
			$publicidad = '<div id="div-gpt-ad-1468339996166-5">' .
							'<script type="text/javascript">' .
							'googletag.cmd.push(function() { googletag.display("div-gpt-ad-1468339996166-5"); });' .
							'</script>' .
							'</div>';
		} elseif ($posicion == 'publicidad_post_video') {
			$publicidad = '<div id="div-gpt-ad-1468339996166-6">' .
							'<script type="text/javascript">' .
							'googletag.cmd.push(function() { googletag.display("div-gpt-ad-1468339996166-6"); });' .
							'</script>' .
							'</div>';
		}
	} elseif (is_post_type_archive('galeria') || is_singular('galeria')) {
		if ($posicion == 'publicidad_top_categoria') {
			$publicidad = '<div id="div-gpt-ad-1468345475964-8" style="height:1px; width:1px;">' .
							'<script type="text/javascript">' .
							'googletag.cmd.push(function() { googletag.display("div-gpt-ad-1468345475964-8"); });' .
							'</script>' .
							'</div>' .
							'<div id="div-gpt-ad-1468345475964-9" style="height:1px; width:1px;">' .
							'<script type="text/javascript">' .
							'googletag.cmd.push(function() { googletag.display("div-gpt-ad-1468345475964-9"); });' .
							'</script>' .
							'</div>' .
							'<div id="div-gpt-ad-1468345475964-0">' .
							'<script type="text/javascript">' .
							'googletag.cmd.push(function() { googletag.display("div-gpt-ad-1468345475964-0"); });' .
							'</script>' .
							'</div>' .
							'<div id="div-gpt-ad-1468345475964-10" style="height:1px; width:1px;">' .
							'<script type="text/javascript">' .
							'googletag.cmd.push(function() { googletag.display("div-gpt-ad-1468345475964-10"); });' .
							'</script>' .
							'</div>';
		} elseif ($posicion == 'publicidad_tras_header_categoria') {
			$publicidad = '<div id="div-gpt-ad-1468345475964-7">' .
							'<script type="text/javascript">' .
							'googletag.cmd.push(function() { googletag.display("div-gpt-ad-1468345475964-7"); });' .
							'</script>' .
							'</div>';
		} elseif ($posicion == 'publicidad_sky_categoria') {
			$publicidad = '<div id="div-gpt-ad-1468345475964-11">' .
							'<script type="text/javascript">' .
							'googletag.cmd.push(function() { googletag.display("div-gpt-ad-1468345475964-11"); });' .
							'</script>' .
							'</div>';
		} elseif ($posicion == 'publicidad_lateral_galeria') {
			$publicidad = '';
		} elseif ($posicion == 'publicidad_pie_categoria') {
			$publicidad = '<div id="div-gpt-ad-1468345475964-5">' .
							'<script type="text/javascript">' .
							'googletag.cmd.push(function() { googletag.display("div-gpt-ad-1468345475964-5"); });' .
							'</script>' .
							'</div>';
		}
	}

	return $publicidad;
}
?>