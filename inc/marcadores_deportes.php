<?php
//los marcadores
$marcadores = array(
	'barracdf_habilitar_barra_home' => get_field('barracdf_habilitar_barra_home', 'option'),
	'barracdf_habilitar_barra_deportes' => get_field('barracdf_habilitar_barra_deportes', 'option'),
	'barracdf_enlace_de_la_nota' => get_field('barracdf_enlace_de_la_nota', 'option'),
	'barracdf_marcador' => get_field('barracdf_marcador', 'option')
);

//validamos que la variable de la categoria exista (es la 4), o la creamos
if(!$id_termino_categoria_actual) {
	$id_termino_categoria_actual = '';
}
?>
	<?php if(get_field('barracdf_habilitar_barra_home', 'option') && is_home() || get_field('barracdf_habilitar_barra_deportes', 'option') && $id_termino_categoria_actual == 4): ?>
		<?php if(!empty($marcadores)): ?>
			<section class="modulo modulo_marcadores_deportes">
				<div>
					<div class="cabecera-cdf">
					<h3>torneo transición <?php echo date('Y'); ?></h3>	
					<p><?php echo get_field('barracdf_fecha_campeonato', 'option'); ?></p>
					<div class="logo-cdf"><img src="http://d2mhmd1y9dvsiv.cloudfront.net/wp-content/uploads/sites/5/2017/09/25/logocdf.png"></div>
					</div>
					<div class="bl">
						<div id="slider-marcadores-deportes" class="owl-carousel wrapper">
							<!-- <?php print_r($marcadores['barracdf_marcador']); ?>-->
							<?php foreach ($marcadores['barracdf_marcador'] as $marcador): ?>
								<?php
								$url_marcador = $marcadores['barracdf_enlace_de_la_nota'] . '/#' . $marcador['barracdf_marcador_ancla'];
								?>
								<div class="item">
									<div class="img">
										<span class="video"></span>
										<a href="<?php echo $url_marcador; ?>">
											<img class="lazyOwl" src="<?php echo s3uri(); ?>/img/transparent.gif" data-src="<?php echo $marcador['barracdf_marcador_imagen']['sizes']['formato-xxs']; ?>" alt="<?php echo $marcador->post_title; ?>">
										</a>
									</div>
									<div class="txt-dep">
										<a href="<?php echo $url_marcador; ?>">

										<div class="txt-division">
											<h3><?php echo $marcador['barracdf_marcador_division']; ?></h3>
										</div>

										<div class="txt-equipo">
											<h3><?php echo $marcador['barracdf_marcador_equipo_1']; ?> VS. 
	
											<?php echo $marcador['barracdf_marcador_equipo_2']; ?></h3>
										</div></a>
 
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</section>
		<?php endif; ?>
	<?php endif; ?>

<script>
jQuery(document).ready(function() {
    jQuery("#slider-marcadores-deportes").owlCarousel({
        lazyLoad: true,
    	navigation: true,
    	nav:true,
    	responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:2
	        },
	        1000:{
	            items:5
	        }
	    }
	});
});
</script>