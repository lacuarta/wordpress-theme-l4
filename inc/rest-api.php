<?php
/**
 * Anade custom fields del post noticia al json generado por la API REST
 * @param  object $data    Los datos
 * @param  object $post    El post
 * @param  array  $request La peticion
 * @return object          Los datos con los nuevos campos
 */
function anadir_custom_fields_noticia_a_json($data, $post, $request) {
	$response_data = $data->get_data();

	if ($request['context'] !== 'view' || is_wp_error($data)) {
   		return $data;
	}

	if (get_field('otro_autor_o_no_tiene', $post->ID)) {
		$response_data['nombre_autor'] = get_field('nombre_autor', $post->ID);
	}

	$thumbnail_id = get_post_thumbnail_id($post->ID);
	$featured_image = wp_get_attachment_image_src($thumbnail_id, 'full');
	$thumbnail = wp_get_attachment_image_src($thumbnail_id);
	$response_data['featured_image_url'] = $featured_image[0];
	$response_data['featured_image_thumbnail_url'] = $thumbnail[0];
	$response_data['bajada'] = get_field('entradilla', $post->ID);
	$response_data['content'] = strip_tags(apply_filters('the_content', get_post_field('post_content', $post->ID)));
	$response_data['key_rudo'] = get_field('key_video', $post->ID);

	$data->set_data($response_data);

	return $data;
}
add_filter('rest_prepare_noticia', 'anadir_custom_fields_noticia_a_json', 10, 3);

/**
 * Anade custom fields del post laterceratv al json generado por la API REST
 * @param  object $data    Los datos
 * @param  object $post    El post
 * @param  array  $request La peticion
 * @return object          Los datos con los nuevos campos
 */
function anadir_custom_fields_lacuartatv_a_json($data, $post, $request) {
	$response_data = $data->get_data();

	if ($request['context'] !== 'view' || is_wp_error($data)) {
   		return $data;
	}

	$response_data['video_url'] = 'http://rudo.video/vod/' . get_field('key_video', $post->ID);

	$data->set_data($response_data);

	return $data;
}
add_filter('rest_prepare_lacuartatv', 'anadir_custom_fields_lacuartatv_a_json', 10, 3);

/**
 * Anade custom fields del post portada al json generado por la API REST
 * @param  object $data    Los datos
 * @param  object $post    El post
 * @param  array  $request La peticion
 * @return object          Los datos con los nuevos campos
 */
function anadir_custom_fields_portada_a_json($data, $post, $request) {
	$response_data = $data->get_data();

	if ($request['context'] !== 'view' || is_wp_error($data)) {
   		return $data;
	}

	$numero_noticia = 1;
	$array_noticias = [];

	if (have_rows('bloques', $post->ID)) {
		 while (have_rows('bloques', $post->ID) && $numero_noticia <= 10) {
		 	the_row();
		 	$bloque_tipo = get_row_layout();

		 	switch ($bloque_tipo) {
				case 'alto_impacto':
					$noticia = get_sub_field('noticia');

					if ($noticia) {
						$array_noticias[$numero_noticia] = array('id' => $noticia->ID, 'titulo' => $noticia->post_title, 'url' => get_permalink($noticia->ID));
						$numero_noticia++;
					}
				break;
			}
		}
	}

	if (have_rows('modulos', $post->ID)) {
		while (have_rows('modulos', $post->ID) && $numero_noticia <= 10) {
			the_row();
			$modulo_tipo = get_row_layout();

			switch ($modulo_tipo) {
				case 'noticias_3_columnas':
					if (have_rows('columna_izquierda')) {
						while (have_rows('columna_izquierda') && $numero_noticia <= 10) {
						 	the_row();
						 	$bloque_tipo = get_row_layout();

						 	if ($bloque_tipo == 'noticia') {
								$noticia = get_sub_field('noticia');

								if ($noticia) {
									$array_noticias[$numero_noticia] = array('id' => $noticia->ID, 'titulo' => $noticia->post_title, 'url' => get_permalink($noticia->ID));
									$numero_noticia++;
								}
							}
						}
					}

					if (have_rows('columna_central')) {
						while (have_rows('columna_central') && $numero_noticia <= 10) {
						 	the_row();
						 	$bloque_tipo = get_row_layout();

						 	if ($bloque_tipo == 'noticia') {
								$noticia = get_sub_field('noticia');

								if ($noticia) {
									$array_noticias[$numero_noticia] = array('id' => $noticia->ID, 'titulo' => $noticia->post_title, 'url' => get_permalink($noticia->ID));
									$numero_noticia++;
								}
							}
						}
					}

					if (have_rows('columna_derecha')) {
						while (have_rows('columna_derecha') && $numero_noticia <= 10) {
						 	the_row();
						 	$bloque_tipo = get_row_layout();

						 	if ($bloque_tipo == 'noticia') {
								$noticia = get_sub_field('noticia');

								if ($noticia) {
									$array_noticias[$numero_noticia] = array('id' => $noticia->ID, 'titulo' => $noticia->post_title, 'url' => get_permalink($noticia->ID));
									$numero_noticia++;
								}
							}
						}
					}
				break;
			}
		}
	}

	$response_data['ultimos_posts'] = $array_noticias;

	$data->set_data($response_data);

	return $data;
}
add_filter('rest_prepare_portada', 'anadir_custom_fields_portada_a_json', 10, 3);
?>