<?php
/**
 * Corta un texto al numero de caracteres especificado
 * @param  string $text              El texto.
 * @param  string $numero_caracteres El numero de caracteres.
 * @return string                    El texto acortado
 */
function corta_texto($texto, $numero_caracteres) {
	$texto = wp_strip_all_tags($texto);

    if (strlen($texto) <= $numero_caracteres + 10) {
    	return $texto;
    }

    $break_pos = strpos($texto, ' ', $numero_caracteres);
    $visible = substr($texto, 0, $break_pos);

    return balanceTags($visible) . '...';
}
?>