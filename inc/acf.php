<?php
/**
 * Head de todas las paginas del Back-End con ACF
 */
function my_acf_admin_head() {
?>

 <script type="text/javascript">
(function($) {
    $(document).ready(function(){
	    $('.acf-field-5644da08e4322 .acf-input').append($('#postdivrich'));
	    $('.acf-field-564d87639a30d .acf-input').append($('#authordiv'));
    });
})(jQuery);
</script>
<style type="text/css">
.acf-field #wp-content-editor-tools {
    background: transparent;
    padding-top: 0;
}
</style>

<?php
}
add_action('acf/input/admin_head', 'my_acf_admin_head');

/**
 * Cambia el orden de los post objects
 * @param  array $args Los argumentos de WP_Query
 * @return array       Los nuevos argumentos de WP_Query
 */
function cambiar_orden_post_objects($args) {
    $args['orderby'] = 'date';
    $args['order'] = 'DESC';

    return $args;
}
add_filter('acf/fields/post_object/query', 'cambiar_orden_post_objects');
?>