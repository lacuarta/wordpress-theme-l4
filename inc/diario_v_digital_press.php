<?php
/*
Este archivo guarda en la tabla wp_options el html correspondiente al módulo de "Diario Version Digital"
Fecha creacion; 13/09/2016
Esta pensado para ser invocado desde el cron
Reprogramado por Juan Zamorano para trabajar con pressreader 22-11-2016
 */
include('../../../../wp-config.php');

echo "Inicio de Proceso - ". date("d-m-Y H:i:s") ."\n";
$wp->init();
$wp->parse_request();
$wp->query_posts();
$wp->register_globals();


$contexto = stream_context_create(array(
    'http' => array(
        'timeout' => 60
        )
    )
);

$xml = file_get_contents('./ftp/Num_Pag/Tot_Pagina.xml', false, $contexto);
if ($xml) {
	echo "Existe archivo /ftp/Num_Pag/Tot_Pagina.xml - ". date("d-m-Y H:i:s") ."\n";
	
    $html = '<div class="row"><header><h3>Diario versión digital</h3>';
    $productos = new SimpleXMLElement($xml);

    foreach ($productos as $producto) {
        if ($producto->attributes()->NAME == 'lacuarta') {
            $fecha_edicion = $producto->EDITION_DATE;
            $numero_paginas = $producto->PAGE_NUMBER;
            break;
        }
    }

    if (is_int((int)$fecha_edicion) && is_int((int)$numero_paginas)) {
        $ano = substr($fecha_edicion, 0, 4);
        $mes = substr($fecha_edicion, 4, 2);
        $dia = substr($fecha_edicion, 6, 2);
        $dias = array('Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado');
        $meses = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
        $fecha_actual = $dias[date('w', strtotime($ano . '-' . $mes . '-' . $dia))] . ' ' . $dia . ' de ' . $meses[$mes - 1];

        $html .= '<span>' . $fecha_actual . '</span></header></div><div style="text-align: center; padding: 2rem 0;" class="row"><div class="slider-diario owl-carousel">';

        for ($i = 1; $i <= $numero_paginas; $i++) {
            if ($i < 10) {
                $numero_pagina_ceros = '0' . $i;
            } elseif ($i < 100) {
                $numero_pagina_ceros = $i;
            } else {
                $numero_pagina_ceros = $i;
            }
			
            $html .= '<div><a href="http://edition.pagesuite-professional.co.uk/html5/reader/production/default.aspx?pubid=a94a1c16-2ebc-4ecc-b2bc-d60709ea4c26&pnum='.$numero_pagina_ceros.'" target="_blank" title="Ver en PageSuite"><img class="" src="http://edition.pagesuite-professional.co.uk/get_image.aspx?w=700&pbid=a94a1c16-2ebc-4ecc-b2bc-d60709ea4c26&pnum='.$numero_pagina_ceros.'" data-src="http://edition.pagesuite-professional.co.uk/get_image.aspx?w=700&pbid=a94a1c16-2ebc-4ecc-b2bc-d60709ea4c26&pnum='.$numero_pagina_ceros.'" title="P&aacute;gina ' . $i . '" alt="P&aacute;gina ' . $i . '" height="333" width="267"></a></div>';

        }

        $html .= '</div></div><script>jq(document).ready(function() { jq(".slider-diario").owlCarousel({ items: 5, lazyLoad: true, addClassActive: true, navigation: true }); });</script>';
        switch_to_blog(5);
        update_site_option('html_diario_version_digital', $html);
		echo "Guardado en base de datos - ". date("d-m-Y H:i:s") ."\n";
        restore_current_blog();
    }
}
echo "Fin de proceso - ". date("d-m-Y H:i:s") ."\n";
?>
