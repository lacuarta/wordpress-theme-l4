<?php
include('../../../../wp-config.php');

$wp->init();
$wp->parse_request();
$wp->query_posts();
$wp->register_globals();

$arrayPapelDigital = getPapelDigital(PAPEL_DIGITAL_TOTAL_PAGINAS_XML);

function getPapelDigital($xml) {

    $successFlag = true;
    $xmlData = '';
    $data = '';

    $fechaHoy = date('dmYH');
    $urlPressReaderThumb = "http://cache-thumb1.pressdisplay.com/pressdisplay/docserver";
    $urlImgPapelDigital = "http://d2mhmd1y9dvsiv.cloudfront.net/wp-content/uploads/sites/5/2017/03/24/Papel-Digital-3.png";
    $urlPressReader = "http://diariolacuarta.pressreader.com";
    $urlPressReaderL4 = "$urlPressReader/la-cuarta";
    $dias = array('Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado');
    $meses = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

    $contexto = stream_context_create(array(
        'http' => array(
            'timeout' => 60
            )
        )
    );

    if($xmlData = file_get_contents($xml, false, $contexto)){

        $res = new SimpleXMLElement($xmlData);

         if((filter_var($res->PRODUCT->EDITION_DATE, FILTER_VALIDATE_INT) !== false) && (filter_var($res->PRODUCT->PAGE_NUMBER, FILTER_VALIDATE_INT) !== false)) {

            //echo "<pre>";print_r("entre!");echo "</pre>";

            list($ano, $mes, $dia) = array(
                date("Y", strtotime((string) $res->PRODUCT->EDITION_DATE)),
                date("m", strtotime((string) $res->PRODUCT->EDITION_DATE)),
                date("d", strtotime((string) $res->PRODUCT->EDITION_DATE))
            );

            $fechaActual = sprintf('%s %d de %s', 
                $dias[date('w', strtotime((string) $res->PRODUCT->EDITION_DATE))],
                $dia,
                $meses[$mes - 1]
            );

            $data =  array(
                'nombre' => (string) $res->PRODUCT->attributes()->NAME,
                'imagen' => $urlImgPapelDigital,
                'url_pressreader' => $urlPressReader,
                'fecha' => array(
                    'raw' => (string) $res->PRODUCT->EDITION_DATE,
                    'format' => $fechaActual,
                    'dia' => $dia,
                    'mes' => $mes,
                    'ano' => $ano,
                ),
                'paginas' => array()
            );

            for ($i = 1; $i <= $res->PRODUCT->PAGE_NUMBER; $i++) {

                $numeroPagina = ($i < 10) ? "0$i":$i;
                
                $data['paginas'][] = array(
                    'titulo' => "P&aacute;gina $i",
                    'url' => "$urlPressReaderL4/{$res->PRODUCT->EDITION_DATE}/page/$numeroPagina", 
                    'img' => "$urlPressReaderThumb/getimage.aspx?cid=eb01&date=&width=300&page=$numeroPagina&nocache=$fechaHoy"
                );
            }

         }
         else {
            $successFlag = false;
         }



    }
    else {
        $successFlag = false;
    }



    echo "<pre>";print_r($data);echo "</pre>";

}