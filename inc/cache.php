<?php
/**
 * Actualiza modulos cuando se crea una nueva noticia
 * @param  int     $post_id El id del post
 * @param  object  $post
 */
function actualizar_categorias($post_id, $post=null) {
    $terminos_categoria = wp_get_post_terms($post_id, 'categoria');

    if (isset($terminos_categoria[0]) && $terminos_categoria[0]->slug == 'ventanita-sentimental') {
        wp_cache_delete('lacuarta_modulo_ventanita_sentimental');
    } elseif (isset($terminos_categoria[0]) && $terminos_categoria[0]->slug == 'de-carne-y-hueso') {
        wp_cache_delete('lacuarta_modulo_carne_hueso');
    } else {
        wp_cache_delete($terminos_categoria[0]->term_id, 'lacuarta_noticias_categoria');
    }

    $terminos_clasificacion = wp_get_post_terms($post_id, 'clasificacion');

    if (isset($terminos_clasificacion[0]) && $terminos_clasificacion[0]->slug == 'los-mas-mejores') {
        wp_cache_delete('lacuarta_los_mas_mejores');
    }
}
add_action('publish_noticia', 'actualizar_categorias', 10, 2);

/**
 * Actualiza el modulo LaCuartaTV cuando se crea un nuevo post de LaCuartaTV
 */
function actualizar_modulo_lacuartatv() {
    wp_cache_delete('lacuarta_modulo_lacuartatv');
    wp_cache_delete('lacuarta_slider_lacuartatv');
}
add_action('publish_lacuartatv', 'actualizar_modulo_lacuartatv');

/**
 * Actualiza el modulo Humor
 * @param  int     $post_id El id del post
 * @param  object  $post
 */
function actualizar_modulo_humor($post_id, $post=null) {
    $terms = wp_get_post_terms($post_id, 'categoriagaleria');

    if (isset($terms[0]) && ($terms[0]->slug == 'pepe-antartico' || $terms[0]->slug == 'cuartoons')) {
        wp_cache_delete($terms[0]->slug, 'lacuarta_modulo_humor');
    }
}
add_action('publish_galeria', 'actualizar_modulo_humor', 10, 2);

/**
 * Actualiza el modulo LaCuartaTV cuando se crea un nuevo post de LaCuartaTV
 */
function actualizar_modulo_widget_horoscopo() {
    wp_cache_delete('lacuarta_modulo_widget_horoscopo');
}
add_action('publish_horoscopo', 'actualizar_modulo_widget_horoscopo');

/**
 * Actualiza el modulo Super Ruleta cuando se crea un nuevo post de Ruleta
 */
function actualizar_modulo_ruleta() {
    wp_cache_delete('lacuarta_modulo_ruleta');
}
add_action('publish_ruleta', 'actualizar_modulo_ruleta');

/**
 * Actualiza modulos cuando se mueve un post a la papelera
 * @param  int     $post_id El id del post
 */
function actualizar_modulos($post_id) {
    global $post_type;

    if ($post_type == 'noticia') {
        actualizar_categorias($post_id);
    } elseif ($post_type == 'lacuartatv') {
        actualizar_modulo_lacuartatv();
    } elseif ($post_type == 'galeria') {
        actualizar_modulo_humor($post_id);
    } elseif ($post_type == 'horoscopo') {
        actualizar_modulo_widget_horoscopo();
    } elseif ($post_type == 'ruleta') {
        actualizar_modulo_ruleta();
    }
}
add_action('wp_trash_post', 'actualizar_modulos', 10, 1);
?>