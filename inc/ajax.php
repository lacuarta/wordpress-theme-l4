<?php
/**
 * Llamada Ajax para filtrar Galeria
 */
function myajax_filtrarGaleria() {
	// Check nonce
	$nonce = $_POST['nonce'];

	if (!wp_verify_nonce($nonce, 'myajax-post-nonce')) {
		die (' ');
	}

	$post_id = $_POST['id'];

	add_filter('posts_fields', function ($fields, $query) {
		global $wpdb;

		if ($query->get('limit_fields')) {
	    	$fields = "$wpdb->posts.ID, $wpdb->posts.post_title";
		}

		return $fields;
	}, 10, 2);

	$args = array(
		'p'                           => $post_id,
		'post_type'                   => 'galeria',
		'post_status'                 => 'publish',
		'no_found_rows'               => true,
		'update_post_thumbnail_cache' => true,
		'limit_fields'                => true
	);

	$galerias = new WP_Query($args);

	if ($galerias->have_posts()) :
		while ($galerias->have_posts()) : $galerias->the_post();
			get_template_part('galeria/slider', 'page');
		endwhile;
	endif;

	exit;
}
add_action('wp_ajax_ajax-filtrarGaleria', 'myajax_filtrarGaleria');
add_action('wp_ajax_nopriv_ajax-filtrarGaleria', 'myajax_filtrarGaleria');

/**
 * Llamada Ajax para filtrar Categoria
 */
function myajax_filtrarCategoria() {
	// Check nonce
	$nonce = $_POST['nonce'];

	if (!wp_verify_nonce($nonce, 'myajax-post-nonce')) {
		die (' ');
	}

	$id_termino = $_POST['id'];
	$noticias_categoria = wp_cache_get($id_termino, 'lacuarta_noticias_categoria');

	if (false === $noticias_categoria) {
		add_filter('posts_fields', function ($fields, $query) {
			global $wpdb;

			if ($query->get('limit_fields')) {
		    	$fields = "$wpdb->posts.ID, $wpdb->posts.post_title, $wpdb->posts.post_name";
			}

			return $fields;
		}, 10, 2);

		$args = array(
			'post_type'                   => 'noticia',
			'posts_per_page'              => 4,
			'post_status'                 => 'publish',
			'no_found_rows'               => true,
			'cache_results'               => false,
			'update_post_thumbnail_cache' => true,
			'limit_fields'                => true,
			'tax_query'                   => array(
				array(
					'taxonomy' => 'categoria',
					'field'    => 'id',
					'terms'    => $id_termino
				)
			)
		);
		$noticias_categoria = new WP_Query($args);
		wp_cache_set($id_termino, $noticias_categoria, 'lacuarta_noticias_categoria');
	}

	if ($noticias_categoria->have_posts()) :
		while ($noticias_categoria->have_posts()) : $noticias_categoria->the_post();
			get_template_part('portada/modulos/content-l4aldia-noticia', 'page');
		endwhile;
	endif;

	exit;
}
add_action('wp_ajax_ajax-filtrarCategoria', 'myajax_filtrarCategoria');
add_action('wp_ajax_nopriv_ajax-filtrarCategoria', 'myajax_filtrarCategoria');

/**
 * Llamada Ajax para filtrar Galeria Humor
 */
function myajax_filtrarHumor() {
	// Check nonce
	$nonce = $_POST['nonce'];

	if (!wp_verify_nonce($nonce, 'myajax-post-nonce')) {
		die (' ');
	}

	$slug_categoria_galeria = $_POST['tipo_humor'];

	$galerias = wp_cache_get($slug_categoria_galeria, 'lacuarta_modulo_humor');

	if (false === $galerias) {
		add_filter('posts_fields', function ($fields, $query) {
			global $wpdb;

			if ($query->get('limit_fields')) {
		    	$fields = "$wpdb->posts.ID, $wpdb->posts.post_name";
			}

			return $fields;
		}, 10, 2);

		$args = array(
			'post_type'                   => 'galeria',
			'posts_per_page'              => 1,
			'post_status'                 => 'publish',
			'no_found_rows'               => true,
			'cache_results'               => false,
			'update_post_thumbnail_cache' => true,
			'limit_fields'                => true,
			'tax_query'                   => array(
				array(
					'taxonomy' => 'categoriagaleria',
					'field'    => 'slug',
					'terms'    => $slug_categoria_galeria
				)
			)
		);
		$galerias = new WP_Query($args);

		wp_cache_set($slug_categoria_galeria, $galerias, 'lacuarta_modulo_humor');
	}

	if ($galerias->have_posts()) :
		while ($galerias->have_posts()) : $galerias->the_post();
			get_template_part('portada/modulos/content-elhumor-galeria', 'page');
		endwhile;
	endif;

	exit;
}
add_action('wp_ajax_ajax-filtrarHumor', 'myajax_filtrarHumor');
add_action('wp_ajax_nopriv_ajax-filtrarHumor', 'myajax_filtrarHumor');
?>