<div class="cap">
	<div class="row">
		<h1 class="portadilla"><?php the_title(); ?></h1>
	</div>
</div>
	<?php
if (get_field('utiliza_opta')) :
	wp_enqueue_style('opta-style', 'http://widget.cloud.opta.net/2.0/css/widgets.opta.css');
	wp_enqueue_script('opta-script', 'http://widget.cloud.opta.net/2.0/js/widgets.opta.js');
?>

<script>
var _optaParams = {
    custID:     '83f090439770a6e6cc0cb324504524e7',
    language:   'es'
};
</script>

<?php
endif;

if (have_rows('bloques')) {
	 while (have_rows('bloques')) {
	 	the_row();
	 	$bloque_tipo = get_row_layout();

	 	switch ($bloque_tipo) {
			case 'html':
			get_template_part('portada/aperturas/html', 'page');
			break;
			case 'alto_impacto':
			include(locate_template('portada/aperturas/alto_impacto.php'));
			break;
			case 'streaming':
			get_template_part('portada/aperturas/streaming', 'page');
			break;
			case 'apertura_vina':
			get_template_part('portada/aperturas/apertura_vina', 'page');
			break;
			case 'diario_version_digital':
			get_template_part('portada/modulos/diario_version_digital', 'page');
			break;
			case 'apertura_guachaca':
			get_template_part('portada/aperturas/apertura_guachaca', 'page');
			break;
			case 'publi_canal':
			get_template_part('portada/aperturas/publi_canal', 'page');
			break;
			case 'envivo':
			get_template_part('portada/aperturas/envivo', 'page');
			break;
		}
	}
}

if (have_rows('modulos')) {
	 while (have_rows('modulos')) {
	 	the_row();
	 	$modulo_tipo = get_row_layout();
	 	switch ($modulo_tipo) {

			case 'programas_l4':
			get_template_part('portada/modulos/programas', 'page');
			break;
			case 'html_datafactory':
			get_template_part('portada/aperturas/html', 'page');
			break;
			case 'apertura_guachaca':
			get_template_part('portada/aperturas/apertura_guachaca', 'page');
			break;
			case 'boleta':
			get_template_part('portada/modulos/boleta', 'page');
			break;
			case 'noticias_3_columnas':
			get_template_part('portada/modulos/noticias_3_columnas', 'page');
			break;
			case 'bloque_streaming':
			get_template_part('portada/modulos/bloque_streaming', 'page');
			break;
			case 'la_cuarta_tv':
			get_template_part('portada/modulos/la_cuarta_tv', 'page');
			break;
			case 'publicidad':
			get_template_part('portada/modulos/publicidad', 'page');
			break;
			case 'html':
			get_template_part('portada/modulos/html', 'page');
			break;
			case 'diario_version_digital':
			get_template_part('portada/modulos/diario_version_digital', 'page');
			break;
			case 'galerias':
			get_template_part('portada/modulos/galerias', 'page');
			break;
			case 'ventanita_sentimental':
			get_template_part('portada/modulos/ventanita_sentimental', 'page');
			break;
			case 'l4aldia':
			get_template_part('portada/modulos/l4aldia', 'page');
			break;
			case 'elhumor':
			get_template_part('portada/modulos/elhumor', 'page');
			break;
			case 'superruleta':
			get_template_part('portada/modulos/superruleta', 'page');
			break;
			case 'widgets':
			get_template_part('portada/modulos/widgets', 'page');
			break;
			case 'los_mas_calentitos':
			get_template_part('portada/modulos/los_mas_calentitos', 'page');
			break;
			case 'reporteros_pop':
			get_template_part('portada/modulos/reporteros_pop', 'page');
			break;
			case 'insertos_publicitarios':
			get_template_part('portada/modulos/insertos_publicitarios', 'page');
			break;
			case 'insertos_segundo_bloque':
			get_template_part('portada/modulos/insertos_segundo_bloque', 'page');
			break;
		}
	}
}
?>

<?php
$publicidad_sky = obtener_publicidad('publicidad_sky_categoria');
if ($publicidad_sky) :
?>
</div>
<?php
endif;
?>