<?php
/**
 * HTML personalizado para los posts mas vistos 
 * @param  array $data  Los posts mas vistos 
 * @return string                   El codigo HTML personalizado de la lista de posts mas vistos
 */
function lista_html_los_mas_lateral_simple($data) {
    $html = '<ul class="fototextosimple">';
    if (is_array ($data)) {
        while (list (, $entry) = each ($data)) {
            $id_post = $entry ['id'];          
            $url = $entry ['link'];
            $titulo = $entry ['title'];

            $html .= '<li>';

            if (get_field('imagen_principal', $id_post) || has_post_thumbnail($id_post) || get_field('imagen_destacada_migracion', $id_post)) {
                if ($imagen = get_field('imagen_principal', $id_post)) {
                    $tamano_imagen = 'thumbnail';
                    $url_imagen = $imagen['sizes'][$tamano_imagen];
                } elseif (has_post_thumbnail($id_post)) {
                    $tamano_imagen = 'thumbnail';
                    $thumb_imagen = wp_get_attachment_image_src(get_post_thumbnail_id($id_post), $tamano_imagen);
                    $url_imagen = $thumb_imagen['0'];
                } elseif (get_field('imagen_destacada_migracion', $id_post)) {
                    $url_imagen = get_field('imagen_destacada_migracion', $id_post);
                }
            } else {
                $url_imagen = s3uri() . '/img/sin_imagen.png';
            }
            
            $html .= "<a href='{$url}' title='"  . esc_attr($titulo) . "'><div class='img'><img class='lazy' src='{$url_imagen}' alt='{$titulo}' width='380' height='270'></div></a>" . "\n";

            $html .= '<h4><a href="'.$url.'">'.$titulo.'</a></h4></li>';
        }
    }
    $html .= '</ul>';
    return $html;
}

/**
 * HTML personalizado para los posts mas vistos 
 * @param  array $data  Los posts mas vistos 
 * @return string                   El codigo HTML personalizado de la lista de posts mas vistos
 */
function lista_html_los_mas_lateral_grande($data) {
    $html = '<ul class="fototexto-verti">';
    if (is_array ($data)) {
        while (list (, $entry) = each ($data)) {
            $id_post = $entry ['id'];          
            $url = $entry ['link'];
            $titulo = $entry ['title'];
            $lista_categorias = get_the_term_list($id_post, 'categoria', '', ', ');

            $html .= '<li><div>';

            if (get_field('imagen_principal', $id_post) || has_post_thumbnail($id_post) || get_field('imagen_destacada_migracion', $id_post)) {
                if ($imagen = get_field('imagen_principal', $id_post)) {
                    $tamano_imagen = 'formato-xxs';
                    $url_imagen = $imagen['sizes'][$tamano_imagen];
                } elseif (has_post_thumbnail($id_post)) {
                    $tamano_imagen = 'formato-xxs';
                    $thumb_imagen = wp_get_attachment_image_src(get_post_thumbnail_id($id_post), $tamano_imagen);
                    $url_imagen = $thumb_imagen['0'];
                } elseif (get_field('imagen_destacada_migracion', $id_post)) {
                    $url_imagen = get_field('imagen_destacada_migracion', $id_post);
                }
                $html .= "<a href='{$url}' title='{$titulo}'><div class='img'><img class='lazy' src='{$url_imagen}' alt='{$titulo}' width='390' height='260'></div></a>" . "\n";
            }

            if ($lista_categorias) {
                if (!get_field('imagen_principal', $id_post) && !has_post_thumbnail($id_post) && !get_field('imagen_destacada_migracion', $id_post)) {
                    $html .= "<span class='not_cat no_imagen'>{$lista_categorias}</span>" . "\n";
                } else {
                    $html .= "<span class='not_cat'>{$lista_categorias}</span>" . "\n";
                }
            }

            $html .= '<h4><a href="'.$url.'">'.$titulo.'</a></h4></div></li>';
        }
    }
    $html .= '</ul>';
    return $html;
}

/**
 * HTML personalizado para los posts mas vistos 
 * @param  array  $data  Los posts mas vistos 
 * @return string       El codigo HTML personalizado de la lista de posts mas vistos
 */
function lista_html_los_mas_home_grande($data) {
    $html = '<ul class="fototextosimple">';
    if (is_array ($data)) {
        $i = 0;
        while (list (, $entry) = each ($data)) {
            if ($i == 2) {
                $html .= "<div class='column four'>" . "\n";
                $html .= "<div class='bl_publi lateral'><div id='div-gpt-ad-1468338424102-11' style='height:250px; width:300px;'><script type='text/javascript'>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1468338424102-11'); }); </script></div></div>" . "\n";
                $html .= "</div>" . "\n";
                $html .= "</div>" . "\n";
                $i++;
            }

            if ($i % 3 == 0) {
                $html .= "<div class='row'>" . "\n";
            }

            $id_post = $entry ['id'];  
            $url = $entry ['link'];
            $titulo = $entry ['title'];
            $lista_categorias = get_the_term_list($id_post, 'categoria', '', ', ');

            $html .= "<div class='column four'>" . "\n";
            $html .= "<article class='noticia_bl " . wp_get_post_terms($id_post, 'categoria', array('fields' => 'slugs'))[0] . "'>" . "\n";
            $html .= "<div class='bl'>" . "\n";

            if (get_field('key_video', $id_post)) {
                $clase = ' puntero noticia-imagen-' . $id_post;
            } else {
                $clase = '';
            }

            $html .= "<a href='" . $url . "'><div class='img{$clase}'>" . "\n";

            if (get_field('fotogaleria', $id_post)) {
                $html .= "<span class='fotogaleria'></span>" . "\n";
            }

            if (get_field('imagen_principal', $id_post) || has_post_thumbnail($id_post) || get_field('imagen_destacada_migracion', $id_post)) {
                if ($imagen = get_field('imagen_principal', $id_post)) {
                    $tamano_imagen = 'formato-xxs';
                    $url_imagen = $imagen['sizes'][$tamano_imagen];
                } elseif (has_post_thumbnail($id_post)) {
                    $tamano_imagen = 'formato-xxs';
                    $thumb_imagen = wp_get_attachment_image_src(get_post_thumbnail_id($id_post), $tamano_imagen);
                    $url_imagen = $thumb_imagen['0'];
                } elseif (get_field('imagen_destacada_migracion', $id_post)) {
                    $url_imagen = get_field('imagen_destacada_migracion', $id_post);
                }
            } else {
                $url_imagen = s3uri() . '/img/sin_imagen.png';
            }

            $html .= "<img class='lazy' src='" . s3uri() . "/img/transparent.gif' data-original='" . $url_imagen . "' alt='" . $titulo . "' width='390' height='260' />" . "\n";
            $html .= "</div></a>" . "\n";
            $html .= "<div class='text'>" . "\n";

            if (get_the_term_list($id_post, 'categoria', '', ', ')) {
                $html .= "<span class='not_cat'>" . get_the_term_list($id_post, 'categoria', '', ', ') . "</span>" . "\n";
            }

            $html .= "<h3><a href='" . $url . "'>" . $titulo . "</a></h3>" . "\n";
            $html .= "</div>" . "\n";
            $html .= "</div>" . "\n";
            $html .= "</article>" . "\n";
            $html .= "</div>" . "\n";

            if (($i + 1) % 3 == 0 && $i > 0) {
                $html .= "</div>" . "\n";
            }

            $i++;

            //$html .= '<li><div><h4><a href="'.$entry ['link'].'">'.$entry ['title'].'</a></h4></div></li>';
        }
    }
    $html .= '</ul>';
    return $html;
}

/*function lista_html_los_mas_vistos($data) {
    $output = '';
    $output .= "<ul class='fototexto-verti'>" . "\n";

    if (is_array($data)) {
        while(list(,$post) = each($data)) {
            $id_post = $post->id;
            $url = get_the_permalink($id_post);
            $lista_categorias = get_the_term_list($id_post, 'categoria', '', ', ');

            $output .= "<li>" . "\n";
            $output .= "<div>" . "\n";

            if (get_field('imagen_principal', $id_post) || has_post_thumbnail($id_post) || get_field('imagen_destacada_migracion', $id_post)) {
                if ($imagen = get_field('imagen_principal', $id_post)) {
                    $tamano_imagen = 'formato-xxs';
                    $url_imagen = $imagen['sizes'][$tamano_imagen];
                } elseif (has_post_thumbnail($id_post)) {
                    $tamano_imagen = 'formato-xxs';
                    $thumb_imagen = wp_get_attachment_image_src(get_post_thumbnail_id($id_post), $tamano_imagen);
                    $url_imagen = $thumb_imagen['0'];
                } elseif (get_field('imagen_destacada_migracion', $id_post)) {
                    $url_imagen = get_field('imagen_destacada_migracion', $id_post);
                }
                $output .= "<a href='{$url}' title='{$post->title}'><div class='img'><img class='lazy' src='{$url_imagen}' alt='{$post->title}' width='390' height='260'></div></a>" . "\n";
            }

            if ($lista_categorias) {
                if (!get_field('imagen_principal', $id_post) && !has_post_thumbnail($id_post) && !get_field('imagen_destacada_migracion', $id_post)) {
                    $output .= "<span class='not_cat no_imagen'>{$lista_categorias}</span>" . "\n";
                } else {
                    $output .= "<span class='not_cat'>{$lista_categorias}</span>" . "\n";
                }
            }

            $output .= "<h4><a href='{$url}' title='"  . esc_attr($post->title) . "'>{$post->title}</a></h4>" . "\n";
            $output .= "</div>" . "\n";
            $output .= "</li>" . "\n";
        }
    }

    $output .= "</ul>" . "\n";
    return $output;
*/

    /*$output = '';
    $output .= "<ul class='fototexto-verti'>" . "\n";

    foreach ($posts_mas_vistos as $post) {
        $id_post = $post->id;
        $url = get_the_permalink($id_post);
        $lista_categorias = get_the_term_list($id_post, 'categoria', '', ', ');

        $output .= "<li>" . "\n";
        $output .= "<div>" . "\n";

        if (get_field('imagen_principal', $id_post) || has_post_thumbnail($id_post) || get_field('imagen_destacada_migracion', $id_post)) {
            if ($imagen = get_field('imagen_principal', $id_post)) {
                $tamano_imagen = 'formato-xxs';
                $url_imagen = $imagen['sizes'][$tamano_imagen];
            } elseif (has_post_thumbnail($id_post)) {
                $tamano_imagen = 'formato-xxs';
                $thumb_imagen = wp_get_attachment_image_src(get_post_thumbnail_id($id_post), $tamano_imagen);
                $url_imagen = $thumb_imagen['0'];
            } elseif (get_field('imagen_destacada_migracion', $id_post)) {
                $url_imagen = get_field('imagen_destacada_migracion', $id_post);
            }
            $output .= "<a href='{$url}' title='{$post->title}'><div class='img'><img class='lazy' src='{$url_imagen}' alt='{$post->title}' width='390' height='260'></div></a>" . "\n";
        }

        if ($lista_categorias) {
            if (!get_field('imagen_principal', $id_post) && !has_post_thumbnail($id_post) && !get_field('imagen_destacada_migracion', $id_post)) {
                $output .= "<span class='not_cat no_imagen'>{$lista_categorias}</span>" . "\n";
            } else {
                $output .= "<span class='not_cat'>{$lista_categorias}</span>" . "\n";
            }
        }

        $output .= "<h4><a href='{$url}' title='"  . esc_attr($post->title) . "'>{$post->title}</a></h4>" . "\n";
        $output .= "</div>" . "\n";
        $output .= "</li>" . "\n";
    }

    $output .= "</ul>" . "\n";
    return $output;*/
//}

/**
 * HTML personalizado para los posts mas vistos del plugin Wordpress Popular Posts para la Home
 * @param  array $posts_mas_vistos  Los posts mas vistos
 * @param  array $instance          Las opciones del plugin
 * @return string                   El codigo HTML personalizado de la lista de posts mas vistos
 */
/*function lista_html_los_mas_calentitos($posts_mas_vistos, $instance) {
    if ($instance['range'] == 'daily') {
        $output = '';
        $output .= "<ul class='fototextosimple'>" . "\n";

        foreach ($posts_mas_vistos as $post) {
            $id_post = $post->id;
            $url = get_the_permalink($id_post);

            $output .= "<li>" . "\n";

            if (get_field('imagen_principal', $id_post) || has_post_thumbnail($id_post) || get_field('imagen_destacada_migracion', $id_post)) {
                if ($imagen = get_field('imagen_principal', $id_post)) {
                    $tamano_imagen = 'thumbnail';
                    $url_imagen = $imagen['sizes'][$tamano_imagen];
                } elseif (has_post_thumbnail($id_post)) {
                    $tamano_imagen = 'thumbnail';
                    $thumb_imagen = wp_get_attachment_image_src(get_post_thumbnail_id($id_post), $tamano_imagen);
                    $url_imagen = $thumb_imagen['0'];
                } elseif (get_field('imagen_destacada_migracion', $id_post)) {
                    $url_imagen = get_field('imagen_destacada_migracion', $id_post);
                }
            } else {
                $url_imagen = s3uri() . '/img/sin_imagen.png';
            }

            $output .= "<div class='img'><a href='{$url}' title='"  . esc_attr($post->title) . "'><img class='lazy' src='{$url_imagen}' alt='{$post->title}' width='380' height='270'></a></div>" . "\n";
            $output .= "<h4><a href='{$url}' title='"  . esc_attr($post->title) . "'>{$post->title}</a></h4>" . "\n";
            $output .= "</li>" . "\n";
        }

        $output .= "</ul>" . "\n";
    } elseif ($instance['range'] == 'weekly') {
        $output = '';
        $i = 0;

        foreach ($posts_mas_vistos as $post) {
            if ($i == 2) {
                $output .= "<div class='column four'>" . "\n";
                $output .= "<div class='bl_publi lateral'><div id='div-gpt-ad-1468338424102-11' style='height:250px; width:300px;'><script type='text/javascript'>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1468338424102-11'); }); </script></div></div>" . "\n";
                $output .= "</div>" . "\n";
                $output .= "</div>" . "\n";
                $i++;
            }

            if ($i % 3 == 0) {
                $output .= "<div class='row'>" . "\n";
            }

            $id_post = $post->id;
            $url = get_the_permalink($id_post);
            $lista_categorias = get_the_term_list($id_post, 'categoria', '', ', ');

            $output .= "<div class='column four'>" . "\n";
            $output .= "<article class='noticia_bl " . wp_get_post_terms($id_post, 'categoria', array('fields' => 'slugs'))[0] . "'>" . "\n";
            $output .= "<div class='bl'>" . "\n";

            if (get_field('key_video', $id_post)) {
                $clase = ' puntero noticia-imagen-' . $id_post;
            } else {
                $clase = '';
            }

            $output .= "<a href='" . $url . "'><div class='img{$clase}'>" . "\n";

            if (get_field('fotogaleria', $id_post)) {
                $output .= "<span class='fotogaleria'></span>" . "\n";
            }

            if (get_field('imagen_principal', $id_post) || has_post_thumbnail($id_post) || get_field('imagen_destacada_migracion', $id_post)) {
                if ($imagen = get_field('imagen_principal', $id_post)) {
                    $tamano_imagen = 'formato-xxs';
                    $url_imagen = $imagen['sizes'][$tamano_imagen];
                } elseif (has_post_thumbnail($id_post)) {
                    $tamano_imagen = 'formato-xxs';
                    $thumb_imagen = wp_get_attachment_image_src(get_post_thumbnail_id($id_post), $tamano_imagen);
                    $url_imagen = $thumb_imagen['0'];
                } elseif (get_field('imagen_destacada_migracion', $id_post)) {
                    $url_imagen = get_field('imagen_destacada_migracion', $id_post);
                }
            } else {
                $url_imagen = s3uri() . '/img/sin_imagen.png';
            }

            $output .= "<img class='lazy' src='" . s3uri() . "/img/transparent.gif' data-original='" . $url_imagen . "' alt='" . get_the_title() . "' width='390' height='260' />" . "\n";
            $output .= "</div></a>" . "\n";
            $output .= "<div class='text'>" . "\n";

            if (get_the_term_list($id_post, 'categoria', '', ', ')) {
                $output .= "<span class='not_cat'>" . get_the_term_list($id_post, 'categoria', '', ', ') . "</span>" . "\n";
            }

            $output .= "<h3><a href='" . $url . "'>" . $post->title . "</a></h3>" . "\n";
            $output .= "</div>" . "\n";
            $output .= "</div>" . "\n";
            $output .= "</article>" . "\n";
            $output .= "</div>" . "\n";

            if (($i + 1) % 3 == 0 && $i > 0) {
                $output .= "</div>" . "\n";
            }

            $i++;
        }
    }

    return $output;
}*/
?>