<?php
$banner = get_sub_field('banner');
$titulo = get_sub_field('titulo');
//$bajada = get_sub_field('bajada');
$notas_relacionadas = get_sub_field('notas_relacionadas');
//$imagen = get_sub_field('imagen');
$html = get_sub_field('html');

if ($notas_relacionadas) {
	$tamano_imagen = 'formato-l';
	$ancho_imagen = 800;
    $alto_imagen = 640;
} else {
	$tamano_imagen = 'formato-xxl';
	$ancho_imagen = 1200;
    $alto_imagen = 965;
}

$url_imagen = $imagen['sizes'][$tamano_imagen];
?>

<!-- waypoint para cerrar la ventana flotante -->
<div id="caja-streaming-cierra"></div>

<div class="apertura streaming">
	<div class="banner row">
<?php
if ($banner) :
	$tamano_imagen_banner = 'large';
	$url_imagen_banner = $banner['sizes'][$tamano_imagen_banner];

?>

		<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $url_imagen_banner; ?>" alt="">

<?php
endif;
?>

	</div>

	<div class="titulo_envivo"><h3><?php echo $titulo; ?></h3></div>

	<div class="row txt_num">
		<div class="column eight igualar<?php if (!$notas_relacionadas) : echo ' ancho_completa'; endif; ?>">

			<div class="video">
				<div id="flotante-streaming" class="not_vid noticia-video"><div onclick="javascript:cerrar_div_streaming();" class="cerrar"></div><?php echo $html; ?></div>
			</div>
		</div>

<?php
if ($notas_relacionadas) :
?>

		<div class="column four igualar">
			<ul>

	<?php
	foreach($notas_relacionadas as $post) :
		setup_postdata($post);
		$imgThumb = get_the_post_thumbnail_url($post->ID,'formato-xs');
	?>

				<li>
					<h4>
						<a href="<?php the_permalink();?>" title="">
							<img src="<?php echo $imgThumb; ?>" width="100" height="100" style="float:left; padding: 5px 10px 5px 5px; max-width: 100px; max-height: 100px;">
							<?php the_title(); ?>
						</a>
					</h4>
					<p><?php the_field('entradilla'); ?></p>
					<a href="<?php the_permalink();?>" title="Ver más" class="vermas">Ver más</a>
				</li>
	<?php
	endforeach;
	wp_reset_postdata();
	?>

			</ul>
		<!-- waypoint para abrir la ventana flotante -->
		<div id="caja-streaming-abre"></div>
		</div>

<?php
endif;
?>
	</div>
</div>

<!--solo si es desktop el video nos sigue la que te persigue -->
<?php if(!wp_is_mobile()): ?>
	<script>
	var divVideoFlotante = document.getElementById("flotante-streaming");

	function cerrar_div_streaming() {
		divVideoFlotante.classList.add('not_vid');
	  	divVideoFlotante.classList.add('noticia-video');
	  	divVideoFlotante.classList.remove('float-streaming-home');
	  	waypointApertura.destroy();
	}

	//waypoint que abre hace flotar el streaming
	var waypointApertura = new Waypoint({
	  element: document.getElementById('caja-streaming-abre'),
	  handler: function() {
		divVideoFlotante.classList.remove('not_vid');
		divVideoFlotante.classList.remove('noticia-video');
		divVideoFlotante.classList.add('float-streaming-home');
	  }
	});

	//waypoint que regresa a su estado normal el streaming
	var waypointCierre = new Waypoint({
	  element: document.getElementById('caja-streaming-cierra'),
	  handler: function() {
	  	divVideoFlotante.classList.add('not_vid');
	  	divVideoFlotante.classList.add('noticia-video');
	  	divVideoFlotante.classList.remove('float-streaming-home');
	  }
	});
	</script>
<?php endif; ?>