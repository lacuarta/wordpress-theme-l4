<?php
$galeria = get_sub_field('galeria');
$estilo = get_sub_field('estilo');

if ($galeria) {
	$post = $galeria;
	setup_postdata($post);
	$id_post = get_the_id();
	$permalink = get_permalink();
	$titulo = get_the_title();
	$imagenes = get_field('galeria');

	if ($numero_columnas == 1) {
		if ($estilo == 'imagen-izquierda' || $estilo == 'imagen-izquierda clara') {
			$tamano_imagen = 'formato-m';
			$ancho_imagen = 600;
			$alto_imagen = 680;
		} else {
			$tamano_imagen = 'formato-xxl';
			$ancho_imagen = 1200;
			$alto_imagen = 965;
		}
	} else {
		$tamano_imagen = 'formato-m';
		$ancho_imagen = 600;
		$alto_imagen = 680;
	}

	if ($imagenes) {
		$url_imagen = $imagenes[0]['sizes'][$tamano_imagen];
	}

	wp_reset_postdata();
}

if (get_sub_field('tiene_titulo_portada_alternativo')) {
	$titulo = get_sub_field('titulo_portada');
}

if (get_sub_field('tiene_entradilla_portada_alternativa')) {
	$entradilla = get_sub_field('entradilla_portada');
}

if (get_sub_field('tiene_imagen_portada_alternativa')) {
	$imagen = get_sub_field('imagen_portada');
	$url_imagen = $imagen['sizes'][$tamano_imagen];
}
?>

<article class="apertura apertura_destacado ancho <?php echo $estilo; ?> <?php echo wp_get_post_terms($id_post, 'categoria', array('fields' => 'slugs'))[0]; ?><?php if (get_sub_field('video_relacionado')) : echo ' videorel'; endif; ?>">
	<div class="bl">
		<div class="bl_fondo">
			<a href="<?php echo $permalink; ?>">
				<div class="fondo_deg"></div>
				<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $url_imagen; ?>" alt="<?php the_title(); ?>" width="<?php echo $ancho_imagen; ?>" height="<?php echo $alto_imagen; ?>" />
			</a>
			<div class="bl_txt">
				<div class="txt">
					<span class="not_cat">
						<a href="<?php echo get_post_type_archive_link('galeria'); ?>">Galerías</a>
					</span>
					<h1><a href="<?php echo $permalink; ?>"><?php echo $titulo; ?></a></h1>

<?php
if (get_sub_field('mostrar_entradilla') && $entradilla) :
?>

					<div class="entradilla"><?php echo $entradilla; ?></div>

<?php
endif;
?>

				</div>
				<ul class="redes row">
					<li class="facebook">
						<a href="https://www.facebook.com/sharer.php?u=<?php echo $permalink; ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" title="Compartir en Facebook" target="_blank">Facebook</a>
					</li>
					<li class="twitter">
						<a href="https://twitter.com/share?url=<?php echo wp_get_shortlink($id_post); ?>&via=lacuarta&text=<?php echo urlencode(html_entity_decode($titulo, ENT_COMPAT, 'UTF-8')); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" title="Compartir en Twitter" target="_blank">Twitter</a>
					</li>
				</ul>

<?php
$video_relacionado = get_sub_field('video_relacionado');

if ($video_relacionado) :
	$post = $video_relacionado;
	setup_postdata($post);
?>

				<div class="video_relacionado">

	<?php
	if (get_field('imagen_principal') || has_post_thumbnail()) :
		if ($imagen = get_field('imagen_principal')) :
			$tamano_imagen = 'thumbnail';
			$url_imagen = $imagen['sizes'][$tamano_imagen];
		elseif (has_post_thumbnail()) :
			$tamano_imagen = 'thumbnail';
			$thumb_imagen = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()), $tamano_imagen);
			$url_imagen = $thumb_imagen['0'];
		endif;
	?>

					<a href="<?php the_permalink(); ?>">
						<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $url_imagen; ?>" alt="<?php the_title(); ?>" width="380" height="270" />
					</a>
					<div class="rel_txt">
			            <span>VIDEO RELACIONADO</span>
						<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					</div>

	<?php
	endif;
	?>

				</div>

<?php
	wp_reset_postdata();
endif;
?>

			</div>
		</div>
	</div>
</article>