<?php
/*
$noticia = get_sub_field('noticia');
$estilo = get_sub_field('estilo');

if ($noticia) {
	$post = $noticia;
	setup_postdata($post);
	$id_post = get_the_id();
	$permalink = get_permalink();
	$lista_categorias = get_the_term_list($id_post, 'categoria', '', ', ');
	$titulo = get_the_title();
	$entradilla = get_field('entradilla');

	if ($numero_columnas == 1) {
		if ($estilo == 'imagen-izquierda' || $estilo == 'imagen-izquierda clara')  {
			$tamano_imagen = 'formato-m';
			$ancho_imagen = 600;
			$alto_imagen = 680;
			$ancho_video = 750;
			$alto_video = 850;
		} else {
			$tamano_imagen = 'formato-xxl';
			$ancho_imagen = 1200;
			$alto_imagen = 965;
			$ancho_video = 1120;
			$alto_video = 900;
		}
	} else {
		$tamano_imagen = 'formato-m';
		$ancho_imagen = 600;
		$alto_imagen = 680;
		$ancho_video = 561;
		$alto_video = 650;
	}

	if (get_field('imagen_principal') || has_post_thumbnail() || get_field('imagen_destacada_migracion')) {
		if ($imagen = get_field('imagen_principal')) {
			$url_imagen = $imagen['sizes'][$tamano_imagen];
		} elseif (has_post_thumbnail()) {
			$thumb_imagen = wp_get_attachment_image_src(get_post_thumbnail_id($id_post), $tamano_imagen);
			$url_imagen = $thumb_imagen['0'];
		} elseif (get_field('imagen_destacada_migracion')) {
	        $url_imagen = get_field('imagen_destacada_migracion');
	    }
	} else {
		$url_imagen = s3uri() . '/img/no_imagen_top2.jpg';
	}

	wp_reset_postdata();
}

if (get_sub_field('tiene_titulo_portada_alternativo')) {
	$titulo = get_sub_field('titulo_portada');
}

if (get_sub_field('tiene_entradilla_portada_alternativa')) {
	$entradilla = get_sub_field('entradilla_portada');
}

if (get_sub_field('tiene_imagen_portada_alternativa')) {
	$imagen = get_sub_field('imagen_portada');
	$url_imagen = $imagen['sizes'][$tamano_imagen];
}
*/



$arrayNotas = get_query_var('notas_apertura_doble');
?>

<?php if ($arrayNotas[1]): ?>

	<?php $thumb_imagen_nota1 = wp_get_attachment_image_src(get_post_thumbnail_id($arrayNotas[0]->ID), 'formato-xxl')[0]; ?>
	<?php $thumb_imagen_nota2 = wp_get_attachment_image_src(get_post_thumbnail_id($arrayNotas[1]->ID), 'formato-m'); ?>



	<div class="row">

		<div class="column eight igualar">
			<article class="apertura apertura_destacado ancho imagen-izquierda clara cronica">

				<span class="not_cat"><?php echo get_the_term_list($arrayNotas[0]->ID, 'categoria', '', ', '); ?></span>
				
				<div class="bl fondo-blanco-not">
					<div class="bl_fondo">

						<a href="<?php echo  get_permalink($arrayNotas[0]->ID); ?>">
							<div class="crop"><img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($arrayNotas[0]->ID), 'formato-xxl')[0]; ?>" alt="<?php echo $arrayNotas[0]->post_title; ?>" width="360" height="680" style="display: inline;"></div>
			            </a>

						<div class="bl_txt">
							<div class="txt">

								<h1><a href="<?php echo  get_permalink($arrayNotas[0]->ID); ?>"><?php echo $arrayNotas[0]->post_title; ?></a></h1>
								<div class="entradilla"><?php echo get_field('entradilla', $arrayNotas[0]->ID); ?></div>

							</div>

							<ul class="redes">
								<li class="facebook">
									<a href="https://www.facebook.com/sharer.php?u=<?php echo get_permalink($arrayNotas[0]->ID); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" title="Compartir en Facebook" target="_blank"></a>
								</li>
								<li class="twitter">
									<a href="https://www.facebook.com/sharer.php?u=<?php echo get_permalink($arrayNotas[0]->ID); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" title="Compartir en Twitter" target="_blank"></a>
								</li><p>comparte:</p>
							</ul>
					    </div>	    
					</div>		
				</div>
			</article>
		</div>

		<div class="column four igualar">

			<div class="bl_publi lateral">

				<!-- /124506296/La_Cuarta/LC_portada/Portada_300x100 -->
				<div id='div-gpt-ad-1468338424102-21' style='width:300px;height:auto;'>
					<script>
					googletag.cmd.push(function() { googletag.display('div-gpt-ad-1468338424102-21'); });
					</script>
				</div>
			</div>

			<div class="bl_publi lateral">

				<!-- /124506296/La_Cuarta/LC_portada/Portada_300x250 -->
				<div id='div-gpt-ad-1468338424102-22' style='width:300px;height:auto;'>
					<script>
					googletag.cmd.push(function() { googletag.display('div-gpt-ad-1468338424102-22'); });
					</script>
				</div>

			</div>

			<article class="apertura apertura_destacado ancho imagen-izquierda apertura-derecha cronica">

				<span class="not_cat"><?php echo get_the_term_list($arrayNotas[1]->ID, 'categoria', '', ', '); ?></span>

				<div class="bl fondo-blanco-not">

					<div class="img">
					
						<a href="<?php echo get_permalink($arrayNotas[1]->ID); ?>">
							<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($arrayNotas[1]->ID), 'formato-m')[0]; ?>" alt="<?php echo $arrayNotas[1]->post_title; ?>" width="390" height="" style="display: inline;">
						</a>

					</div>
					
					<div class="text">

						<h3><a href="<?php echo get_permalink($arrayNotas[1]->ID); ?>"><?php echo $arrayNotas[1]->post_title; ?></a></h3>

						<ul class="redes row">
							<li class="facebook">
								<a href="https://www.facebook.com/sharer.php?u=<?php echo get_permalink($arrayNotas[1]->ID); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" title="Compartir en Facebook" target="_blank"></a>
							</li>
							<li class="twitter">
								<a href="https://twitter.com/share?url=<?php echo wp_get_shortlink($arrayNotas[1]->ID); ?>&via=lacuarta&text=<?php echo urlencode(html_entity_decode($arrayNotas[1]->post_title, ENT_COMPAT, 'UTF-8')); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" title="Compartir en Twitter" target="_blank"></a>
							</li><p>comparte:</p>
						</ul>	

					</div>
				</div>
			</article>
		</div>
	</div>

<?php endif; ?>
