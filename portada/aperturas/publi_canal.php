
<style>
.publicidad_publi_canal{
	width: 100%;
	display: block;
	margin: 0 auto;
}
.banner_publi_canal {
    width: 100%;
    margin-bottom: 1rem;
	background-color: #fff;
}
.banner_mobile {
	display: inline;
}
.banner_desktop {
	display: none;
}
.banner_publi_canal img{
	width: 100%;
}
.apertura-publi_canal {
	width: 100% !important;
}
.apertura-publi_canal h1{
	font: 3rem/0.9 "nationalweb-semibold", Arial, Sans-serif !important;
}
.apertura-publi_canal .bl_txt {
    width: 80% !important;
}
.contenedor_publi_canal {
	background-color: #f0eeed;
    padding: 0;
	padding-bottom: 2rem;
	border-bottom: 3px solid #c8140f;
}
.fondo_video {
    padding: 1rem;
    background-color: #e81561 !important;
}
.fondo_video h3 {
    color: #f7f0cc !important;
}
.box_publi_canal {
	-webkit-box-shadow: 9px 10px 44px -16px rgba(0,0,0,0.55);
	-moz-box-shadow: 9px 10px 44px -16px rgba(0,0,0,0.55);
	box-shadow: 9px 10px 44px -16px rgba(0,0,0,0.55);
    padding: 1rem;
    background-color: #fff;
    border: 1px solid #e1dfde;
    border-radius: 5px;
}
.auspicio {
	border: 4px solid #fef73f;
}
.texto_publi_canal {
	color: #444;
}
.texto_publi_canal h3 {
    padding-right: 1rem;
    margin: 0;
    text-decoration: none;
    outline: none;
    -webkit-transition: 0.2s ease;
    -moz-transition: 0.2s ease;
    -o-transition: 0.2s ease;
    transition: 0.2s ease;
    font: 1.5rem/1 "nationalweb-semibold", Arial, Sans-serif;
}
.texto_publi_canal h3 a{
	color: #444;
}
.texto_publi_canal h3:hover{
    -webkit-transition: 0.2s ease;
    -moz-transition: 0.2s ease;
    -o-transition: 0.2s ease;
    transition: 0.2s ease;
	opacity: .7;
}

.storify {
	height: 400px;
	display: block;
}

.auspiciado-por {
    background-color: #7a858e;
    width: 100%;
    padding: 1px .8rem;
    display: table;
}
.auspiciado-por h4{
    font: 0.75rem "Roboto", Arial, Sans-serif;
    color: #fff;
    margin: 6px;
	float: left;
}
.auspiciado-por img{
	width: 70px;
	float: right;
}

@media screen and (min-width: 700px){
	.logo_publi_canal {
		width: 50%;
	}
}
@media screen and (min-width: 600px){
	.banner_mobile {
		display: none !important;
	}
	.banner_desktop {
		display: inline !important;
	}
}
</style>

<section class="apertura">
	<!--BANNER CABEZAL-->
	<section class="modulo fondo-blanco contenedor_publi_canal">
		<div class="row">
	<?php
	if (have_rows('contenido_principal_publi_canal')){
		while (have_rows('contenido_principal_publi_canal')) {
			the_row();
			$tipo_columna = get_row_layout();
			switch ($tipo_columna) {
				case 'banner_top':
					get_template_part('portada/aperturas/publi_canal/imagen_banner', 'page');
				break;
				case 'noticia':
					get_template_part('portada/aperturas/publi_canal/nota_apertura', 'page');
				break;
				case 'html_stream':
					get_template_part('portada/aperturas/publi_canal/htmlstreaming', 'page');
				break;
				case 'html':
					get_template_part('portada/aperturas/publi_canal/html_marcador', 'page');
				break;
				case 'ondemand':
					get_template_part('portada/aperturas/publi_canal/ondemand', 'page');
				break;
				case 'publicidad_top':
					get_template_part('portada/aperturas/publi_canal/publicidad_top_bottom', 'page');
				break;
			}
		}
	}
	if (have_rows('contenidos_publi_canal')){
		$cont = 0;
		while (have_rows('contenidos_publi_canal')) {
			if( $cont == 0){
?>
	<div class="row">
<?php
			}
			the_row();
			$tipo_columna = get_row_layout();
				switch ($tipo_columna) {
					case 'noticia':
						get_template_part('portada/aperturas/publi_canal/nota_contenido', 'page');
					break;
					case 'publicidad':
						get_template_part('portada/aperturas/publi_canal/publicidad', 'page');
					break;
					case 'html':
						get_template_part('portada/aperturas/publi_canal/html', 'page');
					break;
				}
			$cont ++;
			if( $cont == 3){
?>
</div>
<?php
				$cont = 0;
			}
		}
		if($cont != 0){
?>
</div>
<?php
		}
	}
	if (have_rows('publicidad_bottom')){
		while (have_rows('publicidad_bottom')) {
			the_row();
			$tipo_columna = get_row_layout();
			get_template_part('portada/aperturas/copa_confed/publicidad_top_bottom', 'page');

		}
	}
?>

	</section>
</section>
