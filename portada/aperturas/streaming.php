<?php
$banner = get_sub_field('banner');
$titulo = get_sub_field('titulo');
$bajada = get_sub_field('bajada');
$notas_relacionadas = get_sub_field('notas_relacionadas');
$imagen = get_sub_field('imagen');
$html = get_sub_field('html');

if ($notas_relacionadas) {
	$tamano_imagen = 'formato-l';
	$ancho_imagen = 800;
    $alto_imagen = 640;
} else {
	$tamano_imagen = 'formato-xxl';
	$ancho_imagen = 1200;
    $alto_imagen = 965;
}

$url_imagen = $imagen['sizes'][$tamano_imagen];
?>

<div class="apertura streaming">
	<div class="banner row">
<?php
if ($banner) :
	$tamano_imagen_banner = 'large';
	$url_imagen_banner = $banner['sizes'][$tamano_imagen_banner];
	
?>

		<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $url_imagen_banner; ?>" alt="">

<?php
endif;
?>

	</div>
	<div class="row txt_num">
		<div class="column eight igualar<?php if (!$notas_relacionadas) : echo ' ancho_completa'; endif; ?>">
			<div class="txt">
				<span class="endirecto">Streaming / En directo</span>
				<span class="video"></span>
				<div>
					<h1><?php echo $titulo; ?></h1>
					<p><?php echo $bajada; ?></p>
				</div>
			</div>
			<div class="video">
				<div class="oculta">
					<div class="fondo_deg"></div>
					<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $url_imagen; ?>" alt="<?php the_title(); ?>" width="<?php echo $ancho_imagen; ?>" height="<?php echo $alto_imagen; ?>" />
				</div>
				<div class="not_vid noticia-video" style="display:none;"><?php echo $html; ?></div>
				<script>
				jq('.txt .video').click(function() {
					jq('.noticia-video').show();
					jq('.oculta').hide();
				});
				</script>
			</div>
		</div>

<?php
if ($notas_relacionadas) :
?>

		<div class="column four igualar">
			<ul>

	<?php
	foreach($notas_relacionadas as $post) :
		setup_postdata($post);
	?>

				<li>
					<h4><a href="<?php the_permalink();?>" title=""><?php the_title(); ?></a></h4>
					<p><?php the_field('entradilla'); ?></p>
					<a href="<?php the_permalink();?>" title="Ver más" class="vermas">Ver más</a>
				</li>
	<?php
	endforeach;
	wp_reset_postdata();
	?>

			</ul>
		</div>

<?php
endif;
?>

	</div>
</div>