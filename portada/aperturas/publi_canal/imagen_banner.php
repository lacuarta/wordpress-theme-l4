<?php

$imagen = get_sub_field('imagen_banner');
//var_dump(print_r($imagen,true));
?>
<div class="banner_publi_canal">
    <?php
    if ($imagen) :
    	$tamano_imagen_banner = 'medium_large';
    	$url_imagen_banner = $imagen['sizes'][$tamano_imagen_banner];
        $nombre_imagen_banner = $imagen['name'];
    ?>
		<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $url_imagen_banner; ?>" alt="<?php echo $nombre_imagen_banner?>">
    <?php
    endif;
    ?>
</div>

<?php
wp_reset_postdata();
?>
