<?php

$noticia = get_sub_field('nota_apertura');
if($noticia){
	$post = $noticia;
	setup_postdata($post);
	$id_post = get_the_id();
	$permalink = get_permalink();
	$lista_categorias = get_the_term_list($id_post, 'categoria', '', ', ');
	$titulo = get_the_title();
	$entradilla = get_field('entradilla');
	$tamano_imagen = 'formato-m';
	//$ancho_imagen = 1200;
	$ancho_imagen = 600;
	//$alto_imagen = 965;
	$alto_imagen = 680;
	if (get_field('imagen_principal') || has_post_thumbnail() || get_field('imagen_destacada_migracion')) {
		if ($imagen = get_field('imagen_principal')) {
			$url_imagen = $imagen['sizes'][$tamano_imagen];
		} elseif (has_post_thumbnail()) {
			$thumb_imagen = wp_get_attachment_image_src(get_post_thumbnail_id($id_post), $tamano_imagen);
			$url_imagen = $thumb_imagen['0'];
		} elseif (get_field('imagen_destacada_migracion')) {
			$url_imagen = get_field('imagen_destacada_migracion');
		}
		} else {
	$url_imagen = s3uri() . '/img/no_imagen_top2.jpg';
	}
?>
<article class="apertura apertura_destacado ancho imagen-izquierda clara deportiva">
	<div class="bl">
		<div class="bl_fondo">
			<!--LINK NOTA PRINCIPAL-->
			<a href="<?php echo $permalink; ?>"><div class="fondo_deg"></div>
				<!--IMAGEN NOTA PRINCIPAL-->
				<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $url_imagen ?>" alt="<?php $titulo; ?>" width="<?php echo $ancho_imagen; ?>" height="<?php echo $alto_imagen;?>">
			</a>
			<div class="bl_txt">
				<div class="txt">
					<?php
						if ($lista_categorias) :
					?>
					<span class="not_cat"><?php echo $lista_categorias; ?></span>
					<?php
					endif;
					?>
					<!--NUEVAMENTE EL LINK LA NOTA PRINCIPAL M�S T�TULO-->
					<h1><a href="<?php echo $permalink; ?>"><?php the_title();  ?></a></h1>					
					<ul class="redes row">
						<li class="facebook">
							<a href="https://www.facebook.com/sharer.php?u=<?php echo $permalink; ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" title="Compartir en Facebook" target="_blank">Facebook</a>
						</li>
						<li class="twitter">
							<a href="https://twitter.com/share?url=<?php echo wp_get_shortlink($id_post); ?>&via=lacuarta&text=<?php echo urlencode(html_entity_decode($titulo, ENT_COMPAT, 'UTF-8')); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" title="Compartir en Twitter" target="_blank">Twitter</a>
						</li>
					</ul>
					
				</div>
			</div>
		</div>
	</div>
</article>
</div>

<?php
}
wp_reset_postdata();
?>