<?php
$moduloreina = get_sub_field('modulo_reina');
?>

<section class="modulo">
	<div class="row box-vina">
		<div class="banner-vina"><img src="http://especiales.lacuarta.cl/LaCuarta/vina-2017/la-cuarta-en-la-5ta.png" class="banner1" alt="La Cuarta en la Quinta"></div>

<!--------------------------------PUBLICIDAD------------------------------------------------------------------------>

	<?php if (have_rows('contenido_principal')) {
				while (have_rows('contenido_principal')) {
				the_row();
				$tipo_columna = get_row_layout();

					switch ($tipo_columna) {
					case 'noticia':
					$nota_principal = get_sub_field('nota_apertura');
					break;
					case 'html':
					$html = get_sub_field('htmlstreaming');
					break;
					case 'ondemand':
					$htmlondemand = get_sub_field('html_ondemand');
					$titulondemand = get_sub_field('titulondemand');
					$fotondemand = get_sub_field('foto_ondemand');
					break;
				}
			}
		} 
	?>
<!------------------------------- Inicio STREAMING-------------------------------------------------------------------------->	
	<?php if($html){ ?>
	<div class="publi-vina">
		<script src='https://www.googletagservices.com/tag/js/gpt.js'>
		googletag.pubads().definePassback('/124506296/La_Cuarta/LC_especiales/streaming-festival_top', [[830,60],[320,50]])
						.setClickUrl('%%CLICK_URL_UNESC%%')
						.display();
		</script>
	</div>
	<div class="senal-vivo"><h2>Señal en Vivo</h2></div>
	<div class="streaming">
		<?php echo $html; ?>
	</div>
	<div class="publi-vina">
		<script src='https://www.googletagservices.com/tag/js/gpt.js'>
			googletag.pubads().definePassback('/124506296/La_Cuarta/LC_especiales/streaming-festival_bot', [[830,60],[320,50]])
							.setClickUrl('%%CLICK_URL_UNESC%%')
							.display();
		</script>
	</div>
<!------------------------------- Fin STREAMING-------------------------------------------------------------------------->
<!-------------------------------Inicio nota principal--------------------------------------------------------------->   
   <?php }elseif($nota_principal){
		$post = $nota_principal;
		setup_postdata($post);
		$id_post = get_the_id();
		$permalink = get_permalink();
		$titulo = get_the_title();
		$entradilla = get_field('entradilla');
		$tamano_imagen = 'formato-xxl';
			$ancho_imagen = 1200;
			$alto_imagen = 965;
		if (get_field('imagen_principal') || has_post_thumbnail() || get_field('imagen_destacada_migracion')) {
			if ($imagen = get_field('imagen_principal')) {
				$url_imagen = $imagen['sizes'][$tamano_imagen];
			} elseif (has_post_thumbnail()) {
				$thumb_imagen = wp_get_attachment_image_src(get_post_thumbnail_id($id_post), $tamano_imagen);
				$url_imagen = $thumb_imagen['0'];
			} elseif (get_field('imagen_destacada_migracion')) {
				$url_imagen = get_field('imagen_destacada_migracion');
			}
			} else {
		$url_imagen = s3uri() . '/img/no_imagen_top2.jpg';
		}
		
	?>

	<article class="apertura apertura_destacado ancho imagen-completa sin-margen">
		<div class="bl">
			<div class="bl_fondo">
				<a href="<?php echo $permalink; ?>">
					<div class="fondo_deg"></div>
					<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $url_imagen ?>" alt="<?php $titulo; ?>" width="<?php echo $ancho_imagen; ?>" height="<?php echo $alto_imagen;?>" style="display:inline;"/>
				</a>
				<div class="bl_txt">
						<div class="txt">
							<h1><a href="<?php echo $permalink; ?>"><?php the_title();  ?></a></h1>
						</div>
				 </div>
			</div>	
		</div>
	</article>
	<?php 
	wp_reset_postdata(); }else{ 

	if ($fotondemand) {
		$tamano_imagen_ondemand = 'formato-xxl';
		$ancho_imagen_ondemand = 1200;
		$alto_imagen_ondemand = 965;
	}

	$url_imagen_ondemand = $fotondemand['sizes'][$tamano_imagen_ondemand];	?>	
	<div class="apertura streaming ondemand-vina">
		<div class="row txt_num">
			<div class="column eight igualar ancho_completa" style="">
				<div class="txt">
					<span class="video"></span>
					<div>
						<h1><?php echo $titulondemand; ?></h1>
					</div>
				</div>
				<div class="video">
					<div class="oculta">
						<div class="fondo_deg"></div>
						<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $url_imagen_ondemand; ?>" alt="<?php the_title(); ?>" width="<?php echo $ancho_imagen_ondemand; ?>" height="<?php echo $alto_imagen_ondemand; ?>" />
					</div>
					<div class="not_vid noticia-video" style="display:none;"><?php echo $htmlondemand; ?></div>
					<script>
					jq('.txt .video').click(function() {
						jq('.noticia-video').show();
						jq('.oculta').hide();
					});
					</script>
				</div>
			</div>
		</div>	
	</div>	
	<?php } ?>
	
<!---------------------------------------------------Fin nota principal------------------------------------------------->	
<!-------------------------------------------------- Inicio Notas relacionadas streaming------------------------------>
	
	
	<?php
		$notas_relacionadas = get_sub_field('notas_relacionadas_apertura');
		$contenido = $notas_relacionadas;
		$contador = 0;
		$pos = 3;
	foreach($contenido as $post) :
	    setup_postdata($post);
		$id_post = get_the_id();
		$permalink = get_permalink();
		$titulo = get_the_title();
		$entradilla = get_field('entradilla');
	if ($notas_relacionadas) {
       $tamano_imagen = 'formato-xxs';
		$ancho_imagen = 390;
		$alto_imagen = 260;
	} else {
	$tamano_imagen = 'formato-xs';
		$ancho_imagen = 390;
		$alto_imagen = false;
	}
	if (get_field('imagen_principal') || has_post_thumbnail() || get_field('imagen_destacada_migracion')) {
		if ($imagen = get_field('imagen_principal')) {
			$url_imagen = $imagen['sizes'][$tamano_imagen];
		} elseif (has_post_thumbnail()) {
			$thumb_imagen = wp_get_attachment_image_src(get_post_thumbnail_id($id_post), $tamano_imagen);
			$url_imagen = $thumb_imagen['0'];
		} elseif (get_field('imagen_destacada_migracion')) {
	        $url_imagen = get_field('imagen_destacada_migracion');
	    }
	} else {
		$url_imagen = s3uri() . '/img/no_imagen_top2.jpg';
	}	
	?>
	 <?php if($contador % 4 == 0){
		echo '<div class="notas-vina-sec">';
		
	}?>
			<article class="box-notas-vina <?php if ($bloque_tipo == 'publirreportaje') : echo ' publirreportaje'; elseif (!$ocultar_categorias) : echo ' ' . wp_get_post_terms($id_post, 'categoria', array('fields' => 'slugs'))[0]; endif; ?>">
				<div class="columna">
					<a href="<?php echo $permalink; ?>">
						<img class="imagen-vina lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $url_imagen ?>" alt="<?php $titulo; ?>" width="<?php echo $ancho_imagen; ?>" height="auto">
					</a>
					<?php 
					$categories = get_the_terms($id_post,"categoria");
					if ( ! empty( $categories ) ) {?>
					<div class="text-cat">  
						<?php echo '<a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';?>
					</div>
					<?php }?>
					<a href="<?php the_permalink();?>" title=""><h3><?php the_title();  ?></h3></a>
				</div>
			</article>
		<?php 
		

		
		if($contador == 3 || $contador == 7 || $contador == 11 || $contador == 15 || $contador == 19 || $contador == 23 ){
		echo '</div>'; 
		}		
		?>
	<?php
	$contador++;
	endforeach;
	wp_reset_postdata();
	?>
		
		
	
<!-------------------------------------------------- Fin Notas relacionadas streaming------------------------------>
	</div>	
<!-------------------------------------------------- Inicio Módulo reina  ------------->	
	<?php
	
		if($moduloreina == true){
			?>
	<div class="row box-reina">
				<div class="logo-reina">
					<img src="http://especiales.lacuarta.cl/LaCuarta/vina-2017/logo-reina-2.png" alt="Logo Reina de Viña"></div>
				<?php if (have_rows('contenido_principal_reina')) {
						while (have_rows('contenido_principal_reina')) {
						the_row();
						$tipo_columna = get_row_layout();

							switch ($tipo_columna) {
							case 'publicidad':
							$publicidad_reina = get_sub_field('banner_reina');
							break;
							case 'video_reina':
							$video_reina = get_sub_field('html_video_reina');
							$link_nota = get_sub_field('link_nota');
							$titulo = get_sub_field('titulo');
							break;
							}
						}
					} 
					if($publicidad_reina){	
				?>	
				<div class="publi-vina2"><?php echo $publicidad_reina;?></div>
			
			
			 <?php }else{ ?>
			
			<article class="nota-prin-reina">
				<div class="embed-container-dps-rudo-iframe-16x9">
						<?php echo $video_reina; ?>
				</div>	
				<a href="<?php echo $link_nota; ?>">
					<h3><?php echo $titulo; ?></h3>
				</a>
			</article>
			<?php } ?>
					
			<div class="notas-reina">
			<?php
				$notas_relacionadas_reina = get_sub_field('notas_relacionadas_reina');
				$noticias = $notas_relacionadas_reina;
				foreach($noticias as $post) :
				setup_postdata($post);
				$id_post = get_the_id();
				$permalink = get_permalink();
				$titulo = get_the_title();
				$entradilla = get_field('entradilla');
				if ($notas_relacionadas_reina) {
				$tamano_imagen = 'formato-xxs';
				$ancho_imagen = 390;
				$alto_imagen = 260;
					}else {
				$tamano_imagen = 'formato-xs';
				$ancho_imagen = 390;
				$alto_imagen = false;
				}
				if (get_field('imagen_principal') || has_post_thumbnail() || get_field('imagen_destacada_migracion')) {
					if ($imagen = get_field('imagen_principal')) {
						$url_imagen = $imagen['sizes'][$tamano_imagen];
					} elseif (has_post_thumbnail()) {
						$thumb_imagen = wp_get_attachment_image_src(get_post_thumbnail_id($id_post), $tamano_imagen);
						$url_imagen = $thumb_imagen['0'];
					} elseif (get_field('imagen_destacada_migracion')) {
						$url_imagen = get_field('imagen_destacada_migracion');
					}
				}else{
					$url_imagen = s3uri() . '/img/no_imagen_top2.jpg';
				}?>		
				
					<div class="video-reina">
								<a href="<?php echo $permalink; ?>">
									<img class="imagen-vina lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $url_imagen ?>" alt="<?php the_title(); ?>" width="<?php echo $ancho_imagen; ?>" height="auto"  style="display: inline;">
								
									<h4><?php the_title(); ?></h4>
								</a>
					</div>
<?php	
endforeach;
wp_reset_postdata();	
?>
			</div>
	</div>	
<?php }?>
</section>
