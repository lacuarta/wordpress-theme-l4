<?php

$noticia = get_sub_field('noticia_cont_conf');
if($noticia){
	$post = $noticia;
	setup_postdata($post);
	$id_post = get_the_id();
	$permalink = get_permalink();
	$titulo = get_the_title();
	$entradilla = get_field('entradilla');
	$tamano_imagen = 'formato-xs';
	$ancho_imagen = 390;
	$alto_imagen = false;
	if (get_field('imagen_principal') || has_post_thumbnail() || get_field('imagen_destacada_migracion')) {
		if ($imagen = get_field('imagen_principal')) {
			$url_imagen = $imagen['sizes'][$tamano_imagen];
		} elseif (has_post_thumbnail()) {
			$thumb_imagen = wp_get_attachment_image_src(get_post_thumbnail_id($id_post), $tamano_imagen);
			$url_imagen = $thumb_imagen['0'];
		} elseif (get_field('imagen_destacada_migracion')) {
			$url_imagen = get_field('imagen_destacada_migracion');
		}
		} else {
	$url_imagen = s3uri() . '/img/no_imagen_top2.jpg';
	}
?>

	<!--NOTA SECUNDARIA-->
	<div class="column four">
		<article class="noticia_bl box_copaconfe">
			<div class="bl">
				<!--LINK NOTA SECUNDARIA-->
				<a href="<?php echo $permalink; ?>">
					<!--LINK IMAGEN NOTA SECUNDARIA-->
					<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $url_imagen ?>" alt="<?php $titulo; ?>" width="<?php echo $ancho_imagen; ?>" height="<?php echo $alto_imagen;?>" style="display:inline;">
				</a>						
				<div class="text texto_copaconfe">
					<!--NUEVAMENTE EL LINK NOTA MÁS TÍTULO-->
					<h3><a href="<?php echo $permalink; ?>"><?php the_title();  ?></a></h3>
				</div>
			</div>
		</article>
	</div>


<?php
}
wp_reset_postdata();
?>