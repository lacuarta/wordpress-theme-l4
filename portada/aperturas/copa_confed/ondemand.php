<?php
$htmlondemand = get_sub_field('html_ondemand');
$titulondemand = get_sub_field('titulo_ondemand');
$fotondemand = get_sub_field('foto_ondemand');

if ($fotondemand) {
		$tamano_imagen_ondemand = 'formato-xxl';
		$ancho_imagen_ondemand = 1200;
		$alto_imagen_ondemand = 965;
}

$url_imagen_ondemand = $fotondemand['sizes'][$tamano_imagen_ondemand];	
?>
<div class="apertura streaming ondemand-vina">
	<div class="row txt_num">
		<div class="column eight igualar ancho_completa" style="">
			<div class="txt">
				<span class="video"></span>
				<div>
					<h1><?php echo $titulondemand; ?></h1>
				</div>
			</div>
			<div class="video">
				<div class="oculta">
					<div class="fondo_deg"></div>
					<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $url_imagen_ondemand; ?>" alt="<?php the_title(); ?>" width="<?php echo $ancho_imagen_ondemand; ?>" height="<?php echo $alto_imagen_ondemand; ?>" />
				</div>
				<div class="not_vid noticia-video" style="display:none;"><?php echo $htmlondemand; ?></div>
				<script>
				jq('.txt .video').click(function() {
					jq('.noticia-video').show();
					jq('.oculta').hide();
				});
				</script>
			</div>
		</div>
	</div>	
</div>	