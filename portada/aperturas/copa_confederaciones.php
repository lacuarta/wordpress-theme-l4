
<style>
.publicidad_confe{
	width: 100%;
	display: block;
	margin: 0 auto;
}
.banner_copaconfe {
    width: 100%;
    margin-bottom: 1rem;
	background-color: #fff;
}
.banner_mobile {
	display: inline;
}
.banner_desktop {
	display: none;
}
.banner_copaconfe img{
	width: 100%;
}
.apertura-confede {
	width: 100% !important;
}
.apertura-confede h1{
	font: 3rem/0.9 "nationalweb-semibold", Arial, Sans-serif !important;
}
.apertura-confede .bl_txt {
    width: 80% !important;
}
.contenedor_copaconfe {
	background-color: #f0eeed;
    padding: 0;
	padding-bottom: 2rem;
	border-bottom: 3px solid #c8140f;
}
.fondo_video {
    padding: 1rem;
    background-color: #e81561 !important;
}
.fondo_video h3 {
    color: #f7f0cc !important;
}
.box_copaconfe {
	-webkit-box-shadow: 9px 10px 44px -16px rgba(0,0,0,0.55);
	-moz-box-shadow: 9px 10px 44px -16px rgba(0,0,0,0.55);
	box-shadow: 9px 10px 44px -16px rgba(0,0,0,0.55);
    padding: 1rem;
    background-color: #fff;
    border: 1px solid #e1dfde;
    border-radius: 5px;
}
.auspicio {
	border: 4px solid #fef73f;
}
.texto_copaconfe {
	color: #444;
}
.texto_copaconfe h3 {
    padding-right: 1rem;	
    margin: 0;
    text-decoration: none;
    outline: none;
    -webkit-transition: 0.2s ease;
    -moz-transition: 0.2s ease;
    -o-transition: 0.2s ease;
    transition: 0.2s ease;
    font: 1.5rem/1 "nationalweb-semibold", Arial, Sans-serif;
}
.texto_copaconfe h3 a{
	color: #444;
}
.texto_copaconfe h3:hover{
    -webkit-transition: 0.2s ease;
    -moz-transition: 0.2s ease;
    -o-transition: 0.2s ease;
    transition: 0.2s ease;
	opacity: .7;
}

.storify {
	height: 400px;
	display: block;
}

.auspiciado-por {
    background-color: #7a858e;
    width: 100%;
    padding: 1px .8rem;
    display: table;
}
.auspiciado-por h4{
    font: 0.75rem "Roboto", Arial, Sans-serif;
    color: #fff;
    margin: 6px;
	float: left;
}
.auspiciado-por img{
	width: 70px;
	float: right;
}

@media screen and (min-width: 700px){
	.logo_copaconfe {
		width: 50%;
	}
}
@media screen and (min-width: 600px){
	.banner_mobile {
		display: none !important;
	}
	.banner_desktop {
		display: inline !important;
	}
}
</style>

<section class="apertura">
	<!--BANNER CABEZAL-->
	<section class="modulo fondo-blanco contenedor_copaconfe">
		<div class="row">
			<div class="banner_copaconfe">
				<img src="http://d2mhmd1y9dvsiv.cloudfront.net/wp-content/uploads/sites/5/2017/06/13/cabezal-confe-desktop.png" alt="Copa Confederaciones" class="banner_desktop">
				<img src="http://d2mhmd1y9dvsiv.cloudfront.net/wp-content/uploads/sites/5/2017/06/13/cabezal-confe-mobile.png" alt="Copa Confederaciones" class="banner_mobile">
			</div>
	<?php 
	if (have_rows('contenido_principal_copa')){
		while (have_rows('contenido_principal_copa')) {
			the_row();
			$tipo_columna = get_row_layout();
			switch ($tipo_columna) {
				case 'noticia':
					get_template_part('portada/aperturas/copa_confed/nota_apertura', 'page');
				break;
				case 'html_stream':
					get_template_part('portada/aperturas/copa_confed/htmlstreaming', 'page');
				break;
				case 'html':
					get_template_part('portada/aperturas/copa_confed/html_marcador', 'page');
				break;
				case 'ondemand':
					get_template_part('portada/aperturas/copa_confed/ondemand', 'page');
				break;
				case 'publicidad_top':
					get_template_part('portada/aperturas/copa_confed/publicidad_top_bottom', 'page');
				break;
			}
		}
	}
	if (have_rows('contenidos_confederaciones')){
		$cont = 0;
		while (have_rows('contenidos_confederaciones')) {
			if( $cont == 0){
?>
	<div class="row">
<?php
			}
			the_row();
			$tipo_columna = get_row_layout();
				switch ($tipo_columna) {
					case 'noticia':
						get_template_part('portada/aperturas/copa_confed/nota_contenido', 'page');
					break;				
					case 'publicidad':
						get_template_part('portada/aperturas/copa_confed/publicidad', 'page');
					break;
					case 'html':
						get_template_part('portada/aperturas/copa_confed/html', 'page');
					break;					
				}
			$cont ++;
			if( $cont == 3){
?>
</div>
<?php
				$cont = 0;
			}
		}
		if($cont != 0){
?>
</div>
<?php
		}
	}
	if (have_rows('publicidad_bottom')){
		while (have_rows('publicidad_bottom')) {
			the_row();
			$tipo_columna = get_row_layout();
			get_template_part('portada/aperturas/copa_confed/publicidad_top_bottom', 'page');
			
		}
	}
?>

	</section>
</section>
