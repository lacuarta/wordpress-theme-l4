<?php
if ($bloque_tipo == 'publirreportaje') {
	$noticia = get_sub_field('publirreportaje');
} else {
	$noticia = get_sub_field('noticia');
}

if ($noticia) {
	$post = $noticia;
	setup_postdata($post);
	$id_post = get_the_id();
	$permalink = get_permalink();
	$titulo = get_the_title();

	if ($bloque_tipo == 'publirreportaje') {
		$bajada = get_field('bajada');
	} else {
		$bajada = get_field('entradilla');
	}

	$ancho_video = 350;

	if ($igualar_alturas_imagenes) {
		$tamano_imagen = 'formato-xxs';
		$ancho_imagen = 390;
		$alto_imagen = 260;
		$alto_video = 250;
	} else {
		$tamano_imagen = 'formato-xs';
		$ancho_imagen = 390;
		$alto_imagen = false;
		$alto_video = false;
	}

	if (get_field('imagen_principal') || has_post_thumbnail() || get_field('imagen_destacada_migracion')) {
		if ($imagen = get_field('imagen_principal')) {
			$url_imagen = $imagen['sizes'][$tamano_imagen];
		} elseif (has_post_thumbnail()) {
			$thumb_imagen = wp_get_attachment_image_src(get_post_thumbnail_id($id_post), $tamano_imagen);
			$url_imagen = $thumb_imagen['0'];
		} elseif (get_field('imagen_destacada_migracion')) {
			$url_imagen = get_field('imagen_destacada_migracion');
		}
	} else {
		$url_imagen = s3uri() . '/img/sin_imagen.png';
	}

	wp_reset_postdata();
}

if (get_sub_field('tiene_titulo_portada_alternativo')) {
	$titulo = get_sub_field('titulo_portada');
}

if (get_sub_field('tiene_bajada_portada_alternativa')) {
	$bajada = get_sub_field('bajada_portada');
}

if (get_sub_field('tiene_imagen_portada_alternativa')) {
	$imagen = get_sub_field('imagen_portada');
	$url_imagen = $imagen['sizes'][$tamano_imagen];
}
?>

<article class="noticia_bl<?php if ($bloque_tipo == 'publirreportaje') : echo ' publirreportaje'; elseif (!$ocultar_categorias) : echo ' ' . wp_get_post_terms($id_post, 'categoria', array('fields' => 'slugs'))[0]; endif; ?>">
	<div class="bl">
		<div class="img<?php if (get_field('key_video', $id_post)) : echo ' puntero noticia-imagen-' . $id_post; endif; ?>">

<?php
if (get_field('key_video', $id_post)) :
?>

	   		<span class='video'></span>

<?php
endif;

if (get_field('fotogaleria', $id_post)) :
?>

	      	<span class="fotogaleria"></span>

<?php
endif;

if ($bloque_tipo == 'publirreportaje') :
?>

			<span class="publirreportaje">Publirreportaje</span>

<?php
endif;
?>

			<a href="<?php echo $permalink; ?>">
			<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $url_imagen; ?>" alt="<?php the_title(); ?>" width="<?php echo $ancho_imagen; ?>" height="<?php echo $alto_imagen; ?>" />
			</a>

<?php
if (get_field('key_video', $id_post)) :
?>

			<div class="not_vid noticia-video-<?php echo $id_post; ?>" style="display:none;"><iframe class="vrudo" src="http://rudo.video/vod/<?php echo get_field('key_video', $id_post); ?>" width="<?php echo $ancho_video; ?>" height="<?php echo $alto_video; ?>" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true" msallowfullscreen="true" frameborder="0" scrolling="no"></iframe></div>
			<script>
			jq('.noticia-imagen-<?php echo $id_post; ?> .video').click(function() {
				jq('img', jq(this)).hide();
				jq('.noticia-video-<?php echo $id_post; ?>').show();
			});
			</script>

<?php
endif;
?>

		</div>
		<div class="text">

<?php
if (!$ocultar_categorias && get_the_term_list($id_post, 'categoria', '', ', ')) :
?>

			<span class="not_cat">
				<?php echo get_the_term_list($id_post, 'categoria', '', ', '); ?>
			</span>

<?php
endif;
?>

			<h3><a href="<?php echo $permalink; ?>"><?php echo $titulo; ?></a></h3>

<?php
if (get_sub_field('mostrar_bajada')) :
?>

			<div class="entradilla"><?php echo $bajada; ?></div>

<?php
endif;
?>

			<ul class="redes row">
				<li class="facebook">
					<a href="https://www.facebook.com/sharer.php?u=<?php echo $permalink; ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" title="Compartir en Facebook" target="_blank">Facebook</a>
				</li>
				<li class="twitter">
					<a href="https://twitter.com/share?url=<?php echo wp_get_shortlink($id_post); ?>&via=lacuarta&text=<?php echo urlencode(html_entity_decode($titulo, ENT_COMPAT, 'UTF-8')); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" title="Compartir en Twitter" target="_blank">Twitter</a>
				</li>
			</ul>
		</div>
	</div>
</article>