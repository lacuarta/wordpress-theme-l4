<section class="modulo laldia">
	<div class="row">
		<div class="column eight">

<?php
$terminos = get_sub_field('categorias');

if ($terminos) :
	$i = 1;
?>

			<ul class="pestanas">

	<?php
	foreach ($terminos as $termino) :
		if ($i == 1) {
			$slug_primer_termino = $termino->slug;
			$id_primer_termino = $termino->term_id;
		}
	?>

				<li class="<?php echo $termino->slug; ?><?php if ($i == 1) : echo ' actiu'; endif; ?>"><a href="#" id="termino-<?php echo $termino->term_id; ?>" class="filtro-categoria"><?php echo $termino->name; ?></a></li>

	<?php
		$i++;
	endforeach;
	?>

			</ul>
			<div class="respons <?php echo $slug_primer_termino; ?>">
				<span class="arrow"></span>
				<select id="select-categoria">

	<?php
	foreach ($terminos as $termino) :
	?>

					<option value="<?php echo $termino->term_id; ?>"><?php echo $termino->name; ?></option>

	<?php
	endforeach;
	?>

				</select>
			</div>

<?php
endif;
?>

			<div class="tab">
				<div id="tab-1" class="pest-content">
					<ul class="fototexto">

<?php
$noticias_categoria = wp_cache_get($id_primer_termino, 'lacuarta_noticias_categoria');

if (false === $noticias_categoria) {
	$args = array(
		'post_type'                   => 'noticia',
		'posts_per_page'              => 4,
		'post_status'                 => 'publish',
		'no_found_rows'               => true,
		'cache_results'               => false,
		'update_post_thumbnail_cache' => true,
		'tax_query'                   => array(
			array(
				'taxonomy' => 'categoria',
				'field'    => 'id',
				'terms'    => $id_primer_termino
			)
		)
	);

	$noticias_categoria = new WP_Query($args);

	wp_cache_set($id_primer_termino, $noticias_categoria, 'lacuarta_noticias_categoria');
}

if ($noticias_categoria->have_posts()) :
	while ($noticias_categoria->have_posts()) : $noticias_categoria->the_post();
		get_template_part('portada/modulos/content-l4aldia-noticia', 'page');
	endwhile;
	wp_reset_postdata();
endif;
?>

					</ul>
				</div>
			</div>
			<script>
			jq(document).on('click', '.filtro-categoria', function(e) {
				e.preventDefault();

				jq(this).parent().parent().find('.actiu').removeClass('actiu');
				jq(this).parent().addClass('actiu');
			    jq.post(
			        PT_Ajax.ajaxurl,
			        {
			            action : 'ajax-filtrarCategoria',
			            id : (this.id).substring(8),
			            nonce : PT_Ajax.nonce
			        },
			        function(response) {
			           jq('#tab-1 .fototexto').hide().html(response).fadeIn();
			       }
			    );
			    return false;
			});

			jq(document).on('change', '#select-categoria', function(e) {
				e.preventDefault();

				jq(this).parent().removeClass().addClass('respons').addClass(slug(jq('option:selected', this).text()));

			    jq.post(
			        PT_Ajax.ajaxurl,
			        {
			            action : 'ajax-filtrarCategoria',
			            id : jq(this).val(),
			            nonce : PT_Ajax.nonce
			        },
			        function(response) {
			           jq('#tab-1 .fototexto').hide().html(response).fadeIn();
			       }
			    );
			    return false;
			});

			var slug = function(str) {
				str = str.replace(/^\s+|\s+$/g, ''); // trim
				str = str.toLowerCase();
				var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
				var to   = "aaaaaeeeeeiiiiooooouuuunc------";

				for (var i=0, l=from.length ; i<l ; i++) {
				    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
				}

				str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
					.replace(/\s+/g, '-') // collapse whitespace and replace by -
				    .replace(/-+/g, '-'); // collapse dashes

				return str;
			};
			</script>
		</div>
		<div class="column four">
			<div class='losmas'>
	       		<p>Los más de <span>La cuarta</span></p>
	       		<ul>
	       			<li><a id="mas-vistos" class="tab-los-mas" href="#" class="tit">Los más vistos</a></li>
	       			<li class="activo"><a id="mas-mejores" class="tab-los-mas" href="#" class="tit">Los más mejores</a></li>
	       		</ul>
	       		<script>
	       		jq(document).on('click', '.tab-los-mas', function(e) {
	       			e.preventDefault();

	       			jq(this).parent().parent().find('.activo').removeClass('activo');
	       			jq(this).parent().addClass('activo');

	       			if (jq(this).attr('id') == 'mas-vistos') {
	       				jq('.los-mas-mejores').fadeOut();
	       				jq('.los-mas-vistos').fadeIn();
	       			}

	       			if (jq(this).attr('id') == 'mas-mejores') {
	       				jq('.los-mas-vistos').fadeOut();
	       				jq('.los-mas-mejores').fadeIn();
	       			}
	       		});
	       		</script>
	       		<div class="los-mas-vistos" style="display:none;">

<?php
$html = apply_filters('ze_most_visited_html',array('conf'=>'2days'));
echo $html;
?>

				</div>
				<div class="los-mas-mejores">
					<?php get_template_part('content-los_mas_mejores', 'page'); ?>
				</div>
			</div>
		</div>
	</div>
</section>