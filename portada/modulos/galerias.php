<section class="modulo modulo_galerias">
	<div class="row ampliado">
		<span>Galerías</span>
		<a href="<?php echo get_post_type_archive_link('galeria'); ?>" class="vermas">Ver más galerías</a>
	</div>

<?php
if (have_rows('galerias')) :
	$i = 0;

	while (have_rows('galerias')) :	the_row();
		$galeria = get_sub_field('galeria');

		if ($galeria) :
			$post = $galeria;
			setup_postdata($post);

			if ($i == 0) :
	?>

	<div class="row">
		<div id="galeria-actual" class="row">

				<?php
				add_filter('posts_fields', function ($fields, $query) {
					global $wpdb;

					if ($query->get('limit_fields')) {
				    	$fields = "$wpdb->posts.ID, $wpdb->posts.post_title";
					}

					return $fields;
				}, 10, 2);

				$args = array(
					'p'                           => get_the_id(),
					'post_type'                   => 'galeria',
					'post_status'                 => 'publish',
					'no_found_rows'               => true,
					'update_post_thumbnail_cache' => true,
					'limit_fields'                => true
				);

				$galerias = new WP_Query($args);

				if ($galerias->have_posts()) :
					while ($galerias->have_posts()) : $galerias->the_post();
						get_template_part('galeria/slider', 'page');
					endwhile;
				endif;
				?>

		</div>
		<ul>

			<?php
			endif;

			$imagenes = get_field('galeria');

			if ($imagenes) :
				$tamano = 'medium';
				$thumb = $imagenes[0]['sizes'][$tamano];
			?>

			<li<?php if ($i == 0) : echo ' class="seleccionada"'; endif; ?>>
				<a href="#" id="fotogaleria-<?php the_id(); ?>" class="filtro-galeria">
					<div>
						<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $thumb; ?>" alt="<?php the_title(); ?>" />
					</div>
					<p><?php the_title(); ?></p>
				</a>
			</li>

	<?php
			endif;
			wp_reset_postdata();
		endif;
		$i++;
	endwhile;
	?>

		</ul>
	</div>
	<script>
	jq(document).on('click', '.filtro-galeria', function(e) {
		e.preventDefault();

		jq(this).parent().parent().find('.seleccionada').removeClass('seleccionada');
		jq(this).parent().addClass('seleccionada');
	    jq.post(
	        PT_Ajax.ajaxurl,
	        {
	            action : 'ajax-filtrarGaleria',
	            id : (this.id).substring(12),
	            nonce : PT_Ajax.nonce
	        },
	        function(response) {
	           jq('#galeria-actual').hide().html(response).fadeIn();
	       }
	    );
	    return false;
	});
	</script>

<?php
endif;
?>

</section>