<?php
$horoscopos = wp_cache_get('lacuarta_modulo_widget_horoscopo');

if (false === $horoscopos) {
	$args = array(
		'post_type'      => 'horoscopo',
		'posts_per_page' => 1,
		'post_status'    => 'publish',
		'no_found_rows'  => true,
		'cache_results'  => false
	);
	$horoscopos = get_posts($args);

	wp_cache_set('lacuarta_modulo_widget_horoscopo', $horoscopos);
}

if ($horoscopos) :
	foreach ($horoscopos as $horoscopo) :
		$foto_autor = get_field('foto_autor', $horoscopo->ID);
		$tamano_foto_autor = 'thumbnail';
		$url_foto_autor = $foto_autor['sizes'][$tamano_foto_autor];
?>

<div id="slider-horoscopo" class="column six horoscopo igualar owl-carousel">

		<?php
		if (have_rows('signos_del_zodiaco', $horoscopo->ID)) :
			while (have_rows('signos_del_zodiaco', $horoscopo->ID)) : the_row();
				$imagen_zodiaco = get_sub_field('imagen');
				$tamano_imagen_zodiaco = 'thumbnail';
				$url_imagen_zodiaco = $imagen_zodiaco['sizes'][$tamano_imagen_zodiaco];
		?>

	<div>
		<p class="titwidget">Horóscopo</p>
		<img class="lazyOwl" src="<?php echo s3uri(); ?>/img/transparent.gif" data-src="<?php echo $url_imagen_zodiaco; ?>" width="380" height="270" />
		<div class="datos">
			<p class="nombre"><?php the_sub_field('signo_del_zodiaco'); ?></p>
			<p class="fecha"><?php the_sub_field('fechas'); ?></p>
			<p class="explica"><?php the_sub_field('texto'); ?></p>
			<div class="autor row">
				<div class="img">
					<img src="<?php echo $url_foto_autor; ?>" width="380" height="270" />
				</div>
				<div>
					<p>por <?php the_field('autor', $horoscopo->ID); ?></p>
					<span class="twitter"><a href="https://twitter.com/<?php the_field('twitter', $horoscopo->ID); ?>" target="_blank"><?php the_field('autor', $horoscopo->ID); ?></a></span>
					<span class="telef"><?php the_field('telefono', $horoscopo->ID); ?></span>
				</div>
			</div>
		</div>
		</div>

		<?php
			endwhile;
		endif;
		?>

	</div>
</div>
<script>
jq(document).ready(function() {
    jq("#slider-horoscopo").owlCarousel({
        items: 1,
        lazyLoad: true,
        addClassActive: true,
    	navigation: true,
    	responsiveClass:true,
    	itemsCustom : [
	        [0, 1],
	        [500, 1],
	        [600, 1],
	        [800, 1],
	        [1200, 1]
      	],
	});
});
</script>

<?php
	endforeach;
endif;
?>