<li class="<?php echo wp_get_post_terms(get_the_id(), 'categoria', array('fields' => 'slugs'))[0]; ?>">

<?php
if (get_field('imagen_principal') || has_post_thumbnail() || get_field('imagen_destacada_migracion')) :
?>

	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">

	<?php
	if ($imagen = get_field('imagen_principal')) :
		$tamano_imagen = 'thumbnail';
		$url_imagen = $imagen['sizes'][$tamano_imagen];

		if (isset($_POST['action']) && $_POST['action'] == 'ajax-filtrarCategoria') :
	?>

		<img src="<?php echo $url_imagen; ?>" alt="<?php the_title(); ?>" width="380" height="270" />

		<?php
		else:
		?>

		<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $url_imagen; ?>" alt="<?php the_title(); ?>" width="380" height="270" />

	<?php
		endif;
	elseif (has_post_thumbnail()) :
		$tamano_imagen = 'thumbnail';
		$thumb_imagen = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()), $tamano_imagen);
		$url_imagen = $thumb_imagen['0'];

		if (isset($_POST['action']) && $_POST['action'] == 'ajax-filtrarCategoria') :
	?>

		<img src="<?php echo $url_imagen; ?>" alt="<?php the_title(); ?>" width="380" height="270" />

		<?php
		else:
		?>

		<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $url_imagen; ?>" alt="<?php the_title(); ?>" width="380" height="270" />

	<?php
		endif;
	elseif (get_field('imagen_destacada_migracion')) :
		if (isset($_POST['action']) && $_POST['action'] == 'ajax-filtrarCategoria') :
	?>

		<img src="<?php echo get_field('imagen_destacada_migracion'); ?>" alt="<?php echo $titulo; ?>" width="380" height="270" />

		<?php
		else :
		?>

		<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo get_field('imagen_destacada_migracion'); ?>" alt="<?php echo $titulo; ?>" width="380" height="270" />

	<?php
		endif;
	endif;
	?>

	</a>

<?php
endif;
?>

	<div class="text">
		<span class="not_cat"><?php echo get_the_term_list(get_the_id(), 'categoria', '', ', '); ?></span>
		<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>

<?php
if (get_field('entradilla')) :
?>
		
		<p><?php echo corta_texto(get_field('entradilla'), 200); ?></p>

<?php
else :
?>		
		<p><?php echo corta_texto(get_the_content(), 200); ?></p>

<?php
endif;
?>

	</div>
</li>