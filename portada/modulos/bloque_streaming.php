<section class="modulo <?php the_sub_field('estilo'); ?>">
<?php
$logotipo = get_sub_field('logo');
if ($logotipo && (get_sub_field('estilo') == 'fondo-blanco-logo' || get_sub_field('estilo') == 'fondo-oscuro-logo')) :
	$url_imagen_logo = $logotipo['url'];
?>

	<div class="row ampliado titulo">
		<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $url_imagen_logo; ?>" border="0">
	</div>

<?php
endif;

?>

	<div class="row">
		<div class="column twelve">
<?php
if (have_rows('html_streaming')) {
	if(count('html_streaming') > 0){
		while (have_rows('html_streaming')) {
			the_row();
			$bloque_tipo = get_row_layout();
				switch ($bloque_tipo) {
						case 'html':
						$html = get_sub_field('html');
						break;
					}
		}
	}	
}
?>
<div class="streaming-bloque"> 
	 <?php echo $html; ?>
</div>	 


		</div>
	</div>
</section>