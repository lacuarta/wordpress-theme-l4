<?php
$id_imagen_ruleta_genero = get_field('imagen_ruleta_' . $genero, $ruleta->ID);
$tamano_imagen_ruleta_genero = 'full';
$url_imagen_ruleta_genero = wp_get_attachment_image_src($id_imagen_ruleta_genero, $tamano_imagen_ruleta_genero)[0];
?>

<div id="roulette-<?php echo $genero; ?>" class="contenedorruleta">
	<div class="container">
	    <div class="fondoRuleta"></div>
	    <div class="roulette" style="background-image:url(<?php echo $url_imagen_ruleta_genero; ?>)!important;"></div>
	    <img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo s3uri(); ?>/img/silueta_<?php echo $genero; ?>.png" width="224" height="364" />
	    <div class="flecha"></div>
	    <button class="spinner mobile"><span>Girar</span><div class="pointer"></div></button>
	</div>
	<script>

<?php
$lista_id_genero = [];

while (have_rows($genero, $ruleta->ID)) {
	the_row();

	if ($genero == 'diosas') {
		$id_galeria_genero = get_sub_field('diosa');
	} else {
		$id_galeria_genero = get_sub_field('dios');
	}

	$lista_id_genero[] = $id_galeria_genero;
}

if (!empty($lista_id_genero)) :
	if ($genero == 'diosas') :
?>

	var prices = [

	<?php
	else :
	?>

	var prices2 = [

	<?php
	endif;

	foreach ($lista_id_genero as $id_galeria_genero) :
	?>

		{
			name: '<iframe src="<?php echo get_permalink($id_galeria_genero); ?>?vista=iframe" frameborder="0" height="650" scrolling="no" width="1000" allowtransparency="opaque"></iframe>'
		},

	<?php
	endforeach;
	?>

	];

<?php
endif;

if (!empty($lista_id_genero)) :
	if ($genero == 'diosas') :
?>

	var pricesmobile = [

	<?php
	else :
	?>

	var pricesmobile2 = [

	<?php
	endif;

	foreach ($lista_id_genero as $id_galeria_genero) :
	?>

		{
			name: '<?php echo get_permalink($id_galeria_genero); ?>?vista=completa'
		},

	<?php
	endforeach;
	?>

	];

<?php
endif;
?>

	</script>
</div>

<?php
wp_enqueue_script('jquery-ui-script', s3uri() . '/js/jquery-ui.js', array('jquery'), '1.11.4');

if ($genero == 'diosas') {
	wp_enqueue_script('ruleta-script-diosas', s3uri() . '/js/roulettecode.js', array('jquery'));
	wp_enqueue_script('ruleta-mobile-script-diosas', s3uri() . '/js/roulettecodemobile.js', array('jquery'));
} else {
	wp_enqueue_script('ruleta-script-dioses', s3uri() . '/js/roulettecode2.js', array('jquery'));
	wp_enqueue_script('ruleta-mobile-script-dioses', s3uri() . '/js/roulettecodemobile2.js', array('jquery'));
}
?>

<script>
jq(document).ready(function() {
	var width = window.innerWidth || document.documentElement.clientWidth;

if (width < 700) {
	var urljs = '<?php echo s3uri(); ?>/js/micodemobile.js';
} else {
	var urljs = '<?php echo s3uri(); ?>/js/micode.js';
}

	jq.getScript(urljs).done(function(script, textStatus) {

<?php
if ($genero == 'diosas') :
?>

		configruleta1('#roulette-diosas');

	<?php
else :
	?>

		configruleta2('#roulette-dioses');

<?php
endif;
?>

	});
});

</script>