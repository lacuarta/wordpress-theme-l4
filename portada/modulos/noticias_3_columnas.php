<section class="modulo <?php the_sub_field('diseno'); ?>">
	
<!-- /124506296/La_Cuarta/LC_portada/_skycraper-B -->
<div id='div-gpt-ad-1468338424102-23'>
	<script>
	googletag.cmd.push(function() { googletag.display('div-gpt-ad-1468338424102-23'); });
	</script>
</div>

<?php
$logotipo = get_sub_field('logotipo');

if ($logotipo && (get_sub_field('diseno') == 'fondo-blanco-logo' || get_sub_field('diseno') == 'fondo-oscuro-logo')) :
	$url_imagen_logo = $logotipo['url'];
?>

	<div class="row ampliado titulo">
		<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $url_imagen_logo; ?>" border="0">
	</div>

<?php
endif;

$ocultar_categorias = get_sub_field('ocultar_categorias_de_bloque');
$igualar_alturas_imagenes = get_sub_field('igualar_alturas_imagenes_de_bloque');
?>

	<div class="row">
		<div class="column four">

<?php
if (have_rows('columna_izquierda')) {
	while (have_rows('columna_izquierda')) {
		the_row();
		$bloque_tipo = get_row_layout();

		include(locate_template('portada/modulos/content-columna.php'));
	}
}
?>

		</div>
		<div class="column four">

<?php
if (have_rows('columna_central')) {
	while (have_rows('columna_central')) {
		the_row();
		$bloque_tipo = get_row_layout();

	 	include(locate_template('portada/modulos/content-columna.php'));
	}
}
?>

		</div>
		<div class="column four">

<?php
if (have_rows('columna_derecha')) {
	while (have_rows('columna_derecha')) {
		the_row();
		$bloque_tipo = get_row_layout();

	 	include(locate_template('portada/modulos/content-columna.php'));
	}
}
?>

		</div>
	</div>
</section>