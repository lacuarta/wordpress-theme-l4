<section class="modulo modulo_la_cuarta_tv">
	<div>
		<header class="row ampliado">
			<a href="http://www.lacuarta.com/canal/4tv" title="La Cuarta TV"><span>LacuartaTV</span></a>
			<a href="http://www.lacuarta.com/canal/4tv" title="Ver más vídeos" class="vermas">Ver más vídeos</a>
		</header>

<?php
$noticias_lacuartatv = wp_cache_get('lacuarta_modulo_lacuartatv');

if (false === $noticias_lacuartatv) {
	add_filter('posts_fields', function ($fields, $query) {
		global $wpdb;

		if ($query->get('limit_fields')) {
	    	$fields = "$wpdb->posts.ID, $wpdb->posts.post_title";
		}

		return $fields;
	}, 10, 2);

	$args = array(
		'post_type'                   => 'lacuartatv',
		'posts_per_page'              => 10,
		'post_status'                 => 'publish',
		'no_found_rows'               => true,
		'cache_results'               => false,
		'update_post_thumbnail_cache' => true,
		'limit_fields'                => true,
	);
	$noticias_lacuartatv = get_posts($args);

	wp_cache_set('lacuarta_modulo_lacuartatv', $noticias_lacuartatv);
}

if ($noticias_lacuartatv) :
?>

		<div class="bl">
			<div id="slider-lacuartatv" class="owl-carousel wrapper">

	<?php
	foreach ($noticias_lacuartatv as $noticia) :

		//categoria para la barra de videos L4TV
		$categoriaNota = (wp_get_post_terms($noticia->ID, 'canales')[0]->name) ? wp_get_post_terms($noticia->ID, 'canales')[0]->name:'Sin Categoría';
		
		if ($imagen = get_field('imagen_principal', $noticia->ID)) {
			$tamano_imagen = 'formato-xxs';
			$url_imagen = $imagen['sizes'][$tamano_imagen];
		} elseif (has_post_thumbnail($noticia->ID)) {
			$tamano_imagen = 'formato-xxs';
			$thumb_imagen = wp_get_attachment_image_src(get_post_thumbnail_id($noticia->ID), $tamano_imagen);
			$url_imagen = $thumb_imagen['0'];
		}
	?>

				<div class="item">

		<?php
		if (get_field('imagen_principal', $noticia->ID) || has_post_thumbnail($noticia->ID)) :
		?>

					<div class="img">
						<!--<span class="video"></span>-->
						<a href="<?php echo get_permalink($noticia->ID); ?>"><img class="lazyOwl" src="<?php echo s3uri(); ?>/img/transparent.gif" data-src="<?php echo $url_imagen; ?>" alt="<?php echo $noticia->post_title; ?>"></a>
					</div>

		<?php
		endif;
		?>		

					<div class="cuartatv-txt">
						<div class="categoriatv"><a href="/canal/4tv"><span><?php echo $categoriaNota; ?></span></a></div>
						<h3><a href="<?php echo get_permalink($noticia->ID); ?>"><?php echo $noticia->post_title; ?></a></h3>
					</div>
				</div>

	<?php
	endforeach;
	?>

			</div>
			<script>
			jq(document).ready(function() {
			    jq("#slider-lacuartatv").owlCarousel({
			        lazyLoad: true,
			    	navigation: true,
			    	responsiveClass:true,
			    	itemsCustom : [
				        [0, 1],
				        [500, 2],
				        [600, 3],
				        [800, 4],
				        [1200, 4],
				        [1500, 6]
			      	],
				});
			});
			</script>

	<?php
endif;
?>

		</div>
	</div>
</section>