<section class="modulo superruleta">
	<div class="row">

<?php
$ruletas = wp_cache_get('lacuarta_modulo_ruleta');

if (false === $ruletas) {
	$args = array(
		'post_type'                   => 'ruleta',
		'posts_per_page'              => 1,
		'post_status'                 => 'publish',
		'no_found_rows'               => true,
		'cache_results'               => false,
		'update_post_thumbnail_cache' => true
	);
	$ruletas = get_posts($args);

	wp_cache_set('lacuarta_modulo_ruleta', $ruletas);
}

if ($ruletas) :
	foreach ($ruletas as $ruleta) :
		if (have_rows('diosas', $ruleta->ID)) {
			$genero = 'diosas';
			include(locate_template('portada/modulos/content-superruleta-genero.php'));
		}

		if (have_rows('dioses', $ruleta->ID)) {
			$genero = 'dioses';
			include(locate_template('portada/modulos/content-superruleta-genero.php'));
		}
		?>

		<div class="wrapviewmodal">
			<button id="closemodal">close</button>
			<div id="viewModal"></div>
		</div>
		<?php

	endforeach;
endif;
?>

	</div>
</section>