<div class="modulo publicidad">
	<div class="bl_publi row">
			<h1>Calcular tu Boleta</h1>
		
			<label>Ingrese el monto a consultar</label>
			<div class="input-list style-1 clearfix">
				<input type="text" id="valor">
			</div>
			<div id="textLiq">
				<p>Si el monto fue pactado en valores liquidos, la boleta se realiza por</p>
				<span>$</span><label id="crea-liq"></label>
				<p>y recibirás</p>
				<span>$</span><label id="rec-liq"></label>
			</div>
			<div id="textBrut">
				<p>Si el monto fue pactado en valores brutos, la boleta se realiza por</p>
				<span>$</span><label id="crea-brut"></label>
				<p>y recibirás</p>
				<span>$</span><label id="rec-brut"></label>
			</div>
	</div>
	
</div>