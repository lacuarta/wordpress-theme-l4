
<div class="row box-reina">
				<div class="logo-reina">
					<img src="http://especiales.lacuarta.cl/LaCuarta/vina-2017/logo-reina-2.png" alt="Logo Reina de Viña"></div>
					
<?php $video_principal = get_sub_field('video_principal');
	  $vid = $video_principal;	
	  
if ($video_principal) {

	$post = $video_principal;
	setup_postdata($post);
	$id_post = get_the_id();
	$permalink = get_permalink();
	
	$titulo = get_the_title();
	//$ancho_video = 350;

	if ($video_principal) {
		$tamano_imagen = 'formato-xxs';
		$ancho_imagen = 390;
		$alto_imagen = 260;
		$alto_video = 250;
	} else {
		$tamano_imagen = 'formato-xs';
		$ancho_imagen = 390;
		$alto_imagen = false;
		$alto_video = false;
	}
    
	if (get_field('imagen_principal') || has_post_thumbnail() || get_field('imagen_destacada_migracion')) {
					if ($imagen = get_field('imagen_principal')) {
						$url_imagen = $imagen['sizes'][$tamano_imagen];
					} elseif (has_post_thumbnail()) {
						$thumb_imagen = wp_get_attachment_image_src(get_post_thumbnail_id($id_post), $tamano_imagen);
						$url_imagen = $thumb_imagen['0'];
					} elseif (get_field('imagen_destacada_migracion')) {
						$url_imagen = get_field('imagen_destacada_migracion');
					}
				}else{
					$url_imagen = s3uri() . '/img/no_imagen_top2.jpg';
				}
?>		
			<article class="nota-prin-reina">
				<!--<a href="<?php //echo $permalink;?>">-->
					<div>
						<span class="video noticia-imagen-<?php echo $id_post; ?> "></span>
						<img class="lazy"src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $url_imagen; ?>" alt="<?php the_title(); ?>" width="<?php echo $ancho_imagen; ?>" height="auto">
					</div>
					<p><?php the_title(); ?></p>
				<!--</a>-->	
			</article>	
			
<?php
if (get_field('key_video', $id_post)) :
?>

			<div class="not_vid noticia-video-<?php echo $id_post; ?>" style="display:none;"><iframe class="vrudo" src="http://rudo.video/vod/<?php echo get_field('key_video', $id_post); ?>" width="<?php echo $ancho_video; ?>" height="<?php echo $alto_video; ?>" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true" msallowfullscreen="true" frameborder="0" scrolling="no"></iframe></div>
			<script>
			jq('.noticia-imagen-<?php echo $id_post; ?>.video').click(function() {
			//alert('cliiiiiiick');
				jq('img', jq(this)).hide();
				jq('.noticia-video-<?php echo $id_post; ?>').show();
			});
			</script>

<?php
endif;
?>	
	
<?php			
	wp_reset_postdata();
}?>			
			
			<div class="notas-reina">
			<?php
				$videos_relacionados = get_sub_field('videos_relacionados');
				$videos = $videos_relacionados;
				foreach($videos as $post) :
				setup_postdata($post);
				$id_post = get_the_id();
				$permalink = get_permalink();
				$titulo = get_the_title();
				$entradilla = get_field('entradilla');
				if ($videos_relacionados) {
				$tamano_imagen = 'formato-xxs';
				$ancho_imagen = 390;
				$alto_imagen = 260;
					}else {
				$tamano_imagen = 'formato-xs';
				$ancho_imagen = 390;
				$alto_imagen = false;
				}
				if (get_field('imagen_principal') || has_post_thumbnail() || get_field('imagen_destacada_migracion')) {
					if ($imagen = get_field('imagen_principal')) {
						$url_imagen = $imagen['sizes'][$tamano_imagen];
					} elseif (has_post_thumbnail()) {
						$thumb_imagen = wp_get_attachment_image_src(get_post_thumbnail_id($id_post), $tamano_imagen);
						$url_imagen = $thumb_imagen['0'];
					} elseif (get_field('imagen_destacada_migracion')) {
						$url_imagen = get_field('imagen_destacada_migracion');
					}
				}else{
					$url_imagen = s3uri() . '/img/no_imagen_top2.jpg';
				}?>		
				
					<div class="video-reina">
								<a href="<?php echo $permalink; ?>">
									<img class="imagen-vina lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $url_imagen ?>" alt="<?php the_title(); ?>" width="<?php echo $ancho_imagen; ?>" height="auto"  style="display: inline;">
								
									<h4><?php the_title(); ?></h4>
								</a>
					</div>
<?php	
endforeach;
wp_reset_postdata();	
?>
			</div>
	</div>