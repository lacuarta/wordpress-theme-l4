<?php
$imagenes = get_field('galeria');

if ($imagenes) :
?>

<div class="vinetas">

	<?php
	foreach ($imagenes as $imagen) :
	    $tamano = 'formato-xl';
		$thumb = $imagen['sizes'][$tamano];
	?>

	<div class="item">
		<a href="<?php the_permalink(); ?>">

		<?php
		if (isset($_POST['action']) && $_POST['action'] == 'ajax-filtrarHumor') :
		?>

			<img src="<?php echo $thumb; ?>" alt="" height="230" />

		<?php
		else :
		?>

			<img data-lazy="<?php echo $thumb; ?>" alt="" height="230" />

		<?php
		endif;
		?>

		</a>
	</div>

	<?php
	endforeach;
	?>

</div>
<script>
jq(document).ready(function() {
	jq('.vinetas').slick({
	    autoplay: true,
	    centerMode: true,
	    slidesToShow: 1,
	    variableWidth: true,
	    arrows: true,
	    slidesToScroll: 1,
	    infinite: true,

	<?php
	if (!isset($_POST['action']) || $_POST['action'] != 'ajax-filtrarHumor') :
	?>

	    lazyLoad: 'ondemand'

	<?php
	endif;
	?>

	});
});

</script>

<?php
endif;
?>