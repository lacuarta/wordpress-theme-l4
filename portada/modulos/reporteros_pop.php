<section class="modulo reporterospop">
	<div class="row ampliado">
		<p><span>Reporteros PoP</span> en las Redes Sociales</p>
	</div>
	<div class="popslide row">

<?php
error_reporting(E_ALL);
$reporteros_pop = get_users('orderby=display_name&role=reportero-pop');

foreach ($reporteros_pop as $usuario) :
	$avatar = get_field('avatar', 'user_' . $usuario->ID);

	if ($avatar) :
		$tamano_avatar = 'large';
		$url_avatar = $avatar['sizes'][$tamano_avatar];
?>

		<div><a href="http://twitter.com/<?php echo get_the_author_meta('twitter', $usuario->ID); ?>" target="_blank"><img src="<?php echo $url_avatar; ?>" alt="<?php echo $usuario->display_name; ?>" width="500" height="500" /></a></div>

<?php
	endif;
endforeach;
?>

	</div>
</section>
<?php wp_enqueue_script('slick-script', s3uri() . '/js/slick.min.js', array('jquery'), '1.5.9'); ?>
<script type="text/javascript">
jq(document).ready(function() {
	jq('.popslide').slick({
    	rows: 3,
	    slidesPerRow: 4,
	    dots: true,
	    lazyLoad: 'ondemand'
	});
});
</script>