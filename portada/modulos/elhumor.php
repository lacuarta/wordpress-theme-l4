<section class="modulo elhumor">
	<div class="row">
		<ul class="pestanas oscuro">
			<li class="actiu"><a id="humor-pepe-antartico" class="filtro-humor" href="#">Pepe Antártico</a></li>
			<li><a id="humor-cuartoons" class="filtro-humor" href="#">Cuartoons</a></li>
		</ul>
	</div>
	<div id="humor-actual">

<?php
$galerias = wp_cache_get('pepe-antartico', 'lacuarta_modulo_humor');

if (false === $galerias) {
	add_filter('posts_fields', function ($fields, $query) {
		global $wpdb;

		if ($query->get('limit_fields')) {
	    	$fields = "$wpdb->posts.ID, $wpdb->posts.post_name";
		}

		return $fields;
	}, 10, 2);

	$args = array(
		'post_type'                   => 'galeria',
		'posts_per_page'              => 1,
		'post_status'                 => 'publish',
		'no_found_rows'               => true,
		'cache_results'               => false,
		'update_post_thumbnail_cache' => true,
		'limit_fields'                => true,
		'tax_query'                   => array(
			array(
				'taxonomy' => 'categoriagaleria',
				'field'    => 'slug',
				'terms'    => 'pepe-antartico'
			)
		)
	);
	$galerias = new WP_Query($args);

	wp_cache_set('pepe-antartico', $galerias, 'lacuarta_modulo_humor');
}

if ($galerias->have_posts()) :
	while ($galerias->have_posts()) : $galerias->the_post();
		get_template_part('portada/modulos/content-elhumor-galeria', 'page');
	endwhile;
	wp_reset_postdata();
endif;
?>

	</div>
	<script>
	jq(document).on('click', '.filtro-humor', function(e) {
		e.preventDefault();

		jq(this).parent().parent().find('.actiu').removeClass('actiu');
		jq(this).parent().addClass('actiu');
	    jq.post(
	        PT_Ajax.ajaxurl,
	        {
	            action : 'ajax-filtrarHumor',
	            tipo_humor : (this.id).substring(6),
	            nonce : PT_Ajax.nonce
	        },
	        function(response) {
	           jq('#humor-actual').hide().html(response).fadeIn();
	       }
	    );
	    return false;
	});
	</script>
	<?php wp_enqueue_script('slick-script', s3uri() . '/js/slick.min.js', array('jquery'), '1.5.9'); ?>
</section>