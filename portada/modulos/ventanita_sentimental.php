<section class="modulo ventanita">
	<div class="row">
		<div class="column four">
			<img class="lazy logoventanita1" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo s3uri(); ?>/img/logo_ventanita.jpg" alt="" width="277" height="387" />
			<img class="lazy logoventanita2" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo s3uri(); ?>/img/logo_ventanita2.png" alt="" width="323" height="95" />
		</div>
		<div class="column eight">

<?php
$noticias_ventanita_sentimental = wp_cache_get('lacuarta_modulo_ventanita_sentimental');

if (false === $noticias_ventanita_sentimental) {
	add_filter('posts_fields', function ($fields, $query) {
		global $wpdb;

		if ($query->get('limit_fields')) {
	    	$fields = "$wpdb->posts.ID, $wpdb->posts.post_title, $wpdb->posts.post_name, $wpdb->posts.post_content";
		}

		return $fields;
	}, 10, 2);

	$args = array(
		'post_type'                   => 'noticia',
		'posts_per_page'              => 4,
		'post_status'                 => 'publish',
		'no_found_rows'               => true,
		'cache_results'               => false,
		'update_post_thumbnail_cache' => true,
		'limit_fields'                => true,
		'tax_query'                   => array(
			array(
				'taxonomy' => 'categoria',
				'field'    => 'slug',
				'terms'    => 'ventanita-sentimental'
			)
		)
	);
	$noticias_ventanita_sentimental = get_posts($args);

	wp_cache_set('lacuarta_modulo_ventanita_sentimental', $noticias_ventanita_sentimental);
}

if ($noticias_ventanita_sentimental) :
?>

			<ul class="cajas">

	<?php
	foreach ($noticias_ventanita_sentimental as $noticia) :
	?>

				<li class="igualar">
					<h3><a href="<?php echo get_permalink($noticia->ID); ?>" title="<?php echo $noticia->post_title; ?>"><?php echo $noticia->post_title; ?></a></h3>

		<?php
		if (get_field('entradilla', $noticia->ID)) :
		?>

					<p><?php echo corta_texto(get_field('entradilla', $noticia->ID), 200); ?></p>

		<?php
		else :
		?>

					<p><?php echo corta_texto($noticia->post_content, 200); ?></p>

		<?php
		endif;
		?>

					<a href="<?php echo get_permalink($noticia->ID); ?>" title="Seguir leyendo">Seguir leyendo</a>
				</li>
	<?php
	endforeach;
	?>

			</ul>

<?php
endif;
?>

		</div>
	</div>
</section>