<?php
$los_mas_mejores = wp_cache_get('lacuarta_los_mas_mejores');

if (false === $los_mas_mejores) {
	$args = array(
		'post_type'                   => 'noticia',
		'posts_per_page'              => 6,
		'post_status'                 => 'publish',
		'no_found_rows'               => true,
		'cache_results'               => false,
		'update_post_thumbnail_cache' => true,
		//se agrega filtro de publicidad (apta o no apta)
		'meta_query' => array(
			array(
				'key'		=> 'validador_publicidad',
				'value'		=> 'SI',
				'compare'	=> 'LIKE'
			)
		),
		//se agrega filtro de fecha para mostrar noticias de un maximo de 30 dias de antiguedad
		'date_query'    => array(
	        'column'  => 'post_date',
	        'after'   => '- 30 days'
	    ),
		'tax_query'                   => array(
			array(
				'taxonomy' => 'clasificacion',
				'field'    => 'slug',
				'terms'    => 'los-mas-mejores'
			)
		)
	);

	$los_mas_mejores = new WP_Query($args);

	wp_cache_set('lacuarta_los_mas_mejores', $los_mas_mejores);
}

//banners que pondremos insertos en el menu
$publicidad_lateral_derecho_zona_2 = obtener_publicidad('publicidad_lateral_derecho_zona_2_categoria');
if (is_array($publicidad_lateral_derecho_zona_2)) { 
	foreach ($publicidad_lateral_derecho_zona_2 as $publicidad_lateral_derecho) {
		$banners[] = "<div class='bl_publi widget'>{$publicidad_lateral_derecho['publicidad']}</div>";
	}
}

if ($los_mas_mejores->have_posts()) :
?>

<ul class="<?php if (is_singular('noticia')) : echo 'fototexto-verti'; else : echo 'fototextosimple'; endif; ?>">

	<?php
	$i = 1;
	$x = 0;
	while ($los_mas_mejores->have_posts()) : $los_mas_mejores->the_post();
	?>

	<li>
		<?php
		if (is_singular('noticia')) :
		?>

		<div>

		<?php
		endif;

		if (get_field('imagen_principal') || has_post_thumbnail() || get_field('imagen_destacada_migracion')) :
			if ($imagen = get_field('imagen_principal')) {
				$tamano_imagen = 'thumbnail';
				$url_imagen = $imagen['sizes'][$tamano_imagen];
			} elseif (has_post_thumbnail()) {
	            $tamano_imagen = 'thumbnail';
	            $thumb_imagen = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()), $tamano_imagen);
	            $url_imagen = $thumb_imagen['0'];
	        } elseif (get_field('imagen_destacada_migracion')) {
	        	$url_imagen = get_field('imagen_destacada_migracion');
	        }
        ?>

        	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><div class="img"><img class="lazy" src="<?php echo $url_imagen; ?>" alt="<?php the_title(); ?>" width="380" height="270"></div></a>

        <?php
        endif;

        if (is_singular('noticia') && get_the_term_list($id_post, 'categoria', '', ', ')) :
        	if (!get_field('imagen_principal') && !has_post_thumbnail() && !get_field('imagen_destacada_migracion')) :
        ?>

			<span class="not_cat no_imagen"><?php echo get_the_term_list($id_post, 'categoria', '', ', '); ?></span>

			<?php
			else :
			?>

			<span class="not_cat"><?php echo get_the_term_list($id_post, 'categoria', '', ', '); ?></span>

		<?php
			endif;
		endif;
		?>

        	<h4><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>

        <?php
		if (is_singular('noticia')) :
		?>

		</div>

		<?php
		endif;
		?>

	</li>

	<?php
	if($i%2 == 0) {
		if(isset($banners[$x])) {
			echo $banners[$x];
		}
		++$x; //aumentamos el indice de los banners
	}
	$i++; //aumentamos el indice general para validar
	?>

	<?php
	endwhile;
	?>

</ul>

<?php
	wp_reset_postdata();
endif;
?>