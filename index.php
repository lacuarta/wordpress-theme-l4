<?php get_header(); ?>
<div id="content" class="portada">


<section class="modulo diario_version_digital" id="diario_home">
	<?php echo get_site_option('html_diario_version_digital'); ?>
</section>



<section class="modulo papel_digital_btn">
	<a href="http://edition.pagesuite-professional.co.uk/html5/reader/production/default.aspx?pubid=a94a1c16-2ebc-4ecc-b2bc-d60709ea4c26" target="_blank"><img src="http://edition.pagesuite-professional.co.uk/get_image.aspx?w=300&pbid=a94a1c16-2ebc-4ecc-b2bc-d60709ea4c26" alt="La Cuarta"><div class="papel_digital_titulo"><h3>Lee el diario de hoy aquí</h3></div>
	</a>
</section>

<?php if(wp_is_mobile()): ?>
	<div class="bl_publi_top" style="text-align: center; padding: 20px 0px; background-color: #f1f1f1;">
	<?php echo obtener_publicidad('publicidad_top_categoria'); ?>
</div>
<?php endif; ?>





<?php include(locate_template('inc/marcadores_deportes.php')); ?>

<?php
$args_portada = array(
	'post_type'      => 'portada',
	'post_status'    => 'publish',
	'posts_per_page' => -1,
	'orderby'        => 'none',
	'no_found_rows'  => true
);
query_posts($args_portada);

if (have_posts()) {
	$es_portada_home = false;

	while (have_posts()) {
		the_post();
		$es_portada_categoria = get_field('es_portada_categoria');

		if (!$es_portada_categoria) {
			get_template_part('content-portada', 'page');
			break;
		}
	}
}
?>

</div>
<?php get_footer(); ?>