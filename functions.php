<?php
// Eliminar el metatag generator de la version de Wordpress
remove_action('wp_head', 'wp_generator');

/**
 * Comprueba si el titulo del sitio web esta vacio
 * @param  string $title El titulo del sitio web
 * @return string        El titulo del sitio web
 */
function blankslate_title($title) {
	if ($title == '') {
		return 'Untitled';
	}
	else {
		return $title;
	}
}
add_filter('the_title', 'blankslate_title');

/**
 * Filtro del titulo del sitio web
 * @param  string $title El titulo del sitio web
 * @return string        El titulo del sitio web
 */
function blankslate_filter_wp_title($title)
{
	return $title . esc_attr(get_bloginfo('name'));
}
add_filter('wp_title', 'blankslate_filter_wp_title');

if (!function_exists('lacuarta_setup')) :
/**
 * Set up defaults
 */
function lacuarta_setup() {
	// Registra los menus
	register_nav_menus(array(
		'superior-horizontal' => 'Superior horizontal',
		'inferior'            => 'Inferior'
		)
	);

	// Activa soporte para post thumbnails
	add_theme_support('post-thumbnails');

	// Eliminar el metatag generator de la version de Wordpress
	remove_action('wp_head', 'wp_generator');

	header_remove('X-Powered-By');

	// Registra tamanos de imagen adicionales
	add_image_size('formato-xxl', 1200, 965, true);
	add_image_size('formato-xl', 0, 230, true);
	add_image_size('formato-l', 800, 640, true);
	add_image_size('formato-m', 600, 680, true);
	add_image_size('formato-s', 720, 440, true);
	add_image_size('formato-xs', 390);
	add_image_size('formato-xxs', 390, 260, true);
}
endif;
add_action('after_setup_theme', 'lacuarta_setup');

/**
 * Devuelve get_template_directory_uri() basado en s3 si el plugin esta activado
 */
function s3uri() {
	global $as3cf;

	if ($as3cf) {
    //if(false) { // Para centauri
		if ($as3cf->get_setting('domain') == 'cloudfront') {
    		$protocol = 'http://';
        	$template = str_replace('%2F', '/', rawurlencode(get_template()));

    		if (is_ssl()) {
        		$protocol = 'https://';
       		}

    		return $protocol.$as3cf->get_setting('cloudfront') . '/wp-content/themes/' . $template;
    	}
	} else {
    	return get_template_directory_uri();
   	}
}

/**
 * Anade hojas de estilos y scripts del Front-End
 */
function lacuarta_scripts() {
	// wp_enqueue_style('main-style', get_stylesheet_uri());
	wp_enqueue_style('owl-style',  s3uri() . '/css/owl.carousel.css');
	wp_enqueue_style('reset-style', s3uri() . '/css/reset.css');
	wp_enqueue_style('frame-style', s3uri() . '/css/frame_bhp.css');
	wp_enqueue_style('base', s3uri() . '/css/base.css');
	wp_enqueue_style('especiales', s3uri() . '/css/especiales.css');
	wp_enqueue_style('canales', s3uri() . '/css/canales.css');
	wp_enqueue_style('fancybox-style', s3uri() . '/css/jquery.fancybox.css');

	if (!is_admin()) {
		wp_deregister_script('jquery');
		wp_register_script('jquery', s3uri() . '/js/jquery-1.11.2.min.js', false, null);
		wp_enqueue_script('jquery');
	}

	wp_enqueue_script('lazyload-script', s3uri() . '/js/jquery.lazyload.min.js', array('jquery'), '1.9.3');
	//wp_enqueue_script('papeldigitalweb-script', 'http://papeldigital.info/settings_last.js');
	wp_enqueue_script('papeldigital-script', s3uri() . '/js/papeldigital.js');
	wp_enqueue_script('carousel-script', s3uri() . '/js/owl.carousel.min.js', array('jquery'));

	wp_enqueue_script('waypoints-script', s3uri() . '/js/jquery.waypoints.min.js', array('jquery'));
	wp_enqueue_script('fancybox-script', s3uri() . '/js/jquery.fancybox.js', array('jquery'), '2.1.5');

	if (!is_singular('publirreportajes')) {
		wp_enqueue_script('functions-script', s3uri() . '/js/functions.js', array('jquery'));
	}

	if (is_home() || is_post_type_archive('galeria') || is_post_type_archive('lacuartav') || is_singular('portada') || is_singular('lacuartatv') || is_tax('categoria') || is_tax('etiqueta'))  {
		wp_enqueue_script('igualar-script', s3uri() . '/js/jquery.matchHeight-min.js', array('jquery'), '0.5.2');
	}

	if (is_singular('galeria')) {
		
		if (isset($_GET['vista']) && $_GET['vista'] == 'galeria') {
			wp_enqueue_script('mansory', s3uri() . '/js/mansory.pkgd.min.js', array('jquery'), '3.3.2');
		}
	}

	if (is_home() || is_tax('categoria') || is_singular('portada') || is_singular('noticia') || is_singular('galeria') || is_singular('publirreportajes')) {
		wp_enqueue_script('ajax', s3uri() . '/js/ajax.js', array());
		wp_localize_script('ajax', 'PT_Ajax', array(
			'ajaxurl' => admin_url('admin-ajax.php'),
			'nonce' => wp_create_nonce('myajax-post-nonce'))
		);
	}
}
add_action('wp_enqueue_scripts', 'lacuarta_scripts');

/**
 * Anade el atributo async al script
 * @param  string $tag    El tag
 * @param  string $handle El script
 * @return string         El tag modificado
 */
function anadir_atributo_async($tag, $handle) {
    if ('papeldigitalweb-script' !== $handle) {
    	return $tag;
    }

    return str_replace(' src', ' async="async" src', $tag);
}
add_filter('script_loader_tag', 'anadir_atributo_async', 10, 2);

/**
 * Elimina hojas de estilos de plugins
 */
function lacuarta_deregister_styles() {
	wp_deregister_style('wp-pagenavi');
}
add_action('wp_print_styles', 'lacuarta_deregister_styles', 100);

/**
 * Desactiva el heartbeat para solucionar problemas de CPU
 */
function deregister_heartbeat() {
	global $pagenow;

	if ('post.php' != $pagenow && 'post-new.php' != $pagenow) {
		wp_deregister_script('heartbeat');
	}
}
add_action('init', 'deregister_heartbeat', 1);

/**
 * Elimina menus del administrador y los customs fields de las paginas y entradas
 */
function eliminar_menu_administrador_custom_fields_paginas() {
	remove_menu_page('edit.php');
	remove_menu_page('edit-comments.php');
	remove_meta_box('postcustom', 'page', 'normal'); //Soluciona problema de carga de nuevas paginas
	remove_meta_box('postcustom', 'post', 'normal'); //Soluciona problema de carga de nuevas entradas
}
add_action('admin_menu', 'eliminar_menu_administrador_custom_fields_paginas');

/**
 * Elimina items del toolbar
 * @param  object $wp_admin_bar El toolbar
 */
function eliminar_item_toolbar($wp_admin_bar) {
	$wp_admin_bar->remove_node('comments');
	$wp_admin_bar->remove_node('new-post');
}
add_action('admin_bar_menu', 'eliminar_item_toolbar', 999);

/**
 * Permite el uso de tags en el editor del content
 * @param  array $multisite_tags Los tags del multisite
 * @return array                 Los tags del multisite modificados
 */
function allow_multisite_tags($multisite_tags) {
    $multisite_tags['iframe'] = array(
        'src'             => true,
        'width'           => true,
        'height'          => true,
        'align'           => true,
        'class'           => true,
        'name'            => true,
        'id'              => true,
        'frameborder'     => true,
        'seamless'        => true,
        'srcdoc'          => true,
        'sandbox'         => true,
        'allowfullscreen' => true
    );

    return $multisite_tags;
}
add_filter('wp_kses_allowed_html','allow_multisite_tags', 1);

/**
 * Anade items estaticos al menu del header interior
 * @return string Codigo HTML del menu
 */
function nav_wrap_header_interior() {
	$wrap  = '<ul id="%1$s" class="%2$s">';
	$wrap .= '<li><a href="' . site_url() . '">' . __('Home', 'supera') . '</a></li>';
	$wrap .= '%3$s';
	$wrap .= '</ul>';

	return $wrap;
}

/**
 * Limita las busquedas solo a titulos del post
 * @param  string $search    La busqueda en la clausula WHERE
 * @param  object &$wp_query La query actual
 * @return string            La busqueda modificada
 */
function buscar_solo_por_titulo($search, &$wp_query) {
    global $wpdb;

    if (empty($search)) {
        return $search;
    }

    $q = $wp_query->query_vars;
    $n = !empty($q['exact']) ? '' : '%';
    $search = '';
    $searchand = '';

    foreach ((array) $q['search_terms'] as $term) {
        $term = esc_sql( like_escape($term));
        $search .= "{$searchand}($wpdb->posts.post_title LIKE '{$n}{$term}{$n}')";
        $searchand = ' AND ';
    }

    if (!empty($search)) {
        $search = " AND ({$search}) ";
        if (! is_user_logged_in())
            $search .= " AND ($wpdb->posts.post_password = '') ";
    }

    return $search;
}
if (is_admin()) {
	add_filter('posts_search', 'buscar_solo_por_titulo', 500, 2);
}

/**
 * Modifica queries de posts
 * @param  object $query La query
 */
function lacuarta_post_queries($query) {
	global $wp_query;

    if (is_post_type_archive('galeria')) {
    	$query->set('posts_per_page', 12);

    	if (!has_term('pepe-antartico', 'categoriagaleria') && !has_term('cuartoons', 'categoriagaleria')) {
        	$query->set('tax_query', array(array('taxonomy' => 'categoriagaleria', 'field' => 'slug', 'terms' => array('pepe-antartico', 'cuartoons'), 'operator' => 'NOT IN')));
        }

        return;
    } elseif (is_post_type_archive('lacuartatv')) {
        $query->set('posts_per_page', 10);
        $query->set('post_status', 'publish');
        $query->set('no_found_rows', true);

        return;
    } elseif (is_home()) {
    	$query->set('no_found_rows', true);

    	return;
    }
}
if (!is_admin()) {
	add_action('pre_get_posts', 'lacuarta_post_queries');
}

/**
 * Elimina las clausulas GROUP BY de las queries principales en el Front-End
 */
add_filter('posts_groupby', function($groupby) {
	if (!is_admin()) {
		$groupby = '';
    }

    return $groupby;
});

/**
 * Modifica las clausulas de la query
 * @param  array $clauses Las clausulas
 * @param  array $query   La query
 * @return array          Las clausulas modificadas
 */
function modifica_clausulas_query($clauses, $query) {
	if (!is_admin() && is_main_query()) {
		if (is_home()) {
		    $clauses['join'] = '';
		    $clauses['where'] = '';
		    $clauses['orderby'] = '';
		    $clauses['limits'] = 'LIMIT 0, 1';
		}
	}

	return $clauses;
}
add_filter('posts_clauses', 'modifica_clausulas_query', 10, 2);

/**
 * Desactiva la busqueda por defecto de WordPress
 * @param  object  $query La query de busqueda
 * @param  boolean $error Si se debe redireccionar a la pagina 404
 */
function desactiva_busqueda_defecto_wordpress($query, $error = true) {
	if (is_search()) {
		$query->is_search = false;
		$query->query_vars[s] = false;
		$query->query[s] = false;

		if ($error == true) {
			$query->is_404 = true;
		}
	}
}
if (!is_admin()) {
	add_action('parse_query', 'desactiva_busqueda_defecto_wordpress');
	add_filter('get_search_form', create_function('$a', "return null;"));
}

/**
 * Anade los usuarios de custom roles al dropdown del post author
 * @param  string $output HTML del dropdown de post author
 * @return string         HTML del dropdown de post author modificado con todos los usuarios
 */
function sobreescribe_post_author_dropdown($output)
{
    global $post, $user_ID;

	if (!preg_match('/post_author_override/', $output)) {
		return $output;
	}

	if (preg_match ('/post_author_override_replaced/', $output)) {
		return $output;
	}

    $output = wp_dropdown_users(array(
    	'echo' => 0,
        'name' => 'post_author_override_replaced',
        'selected' => empty($post->ID) ? $user_ID : $post->post_author,
        'include_selected' => true
    ));

    $output = preg_replace('/post_author_override_replaced/', 'post_author_override', $output);

  	return $output;
}
add_filter('wp_dropdown_users', 'sobreescribe_post_author_dropdown');

/**
 * Redirecciona a un template
 * @param  string $template El path del template
 * @return string           El path modificado del template
 */
function redireccionar_template($template) {
	if (is_tax('categoriagaleria')) {
		$template = get_query_template('archive-galeria');
	}

	return $template;
}
add_filter('template_include', 'redireccionar_template');

//home lateral
add_filter('ze_most_visited_conf', function($conf) {
		$conf['2days'] = $conf ['default'];
		$conf['2days']['from_date'] = '2daysAgo';
		$conf['2days']['no_elements'] = '6';
		return $conf ;
});

//single noticia
add_filter('ze_most_visited_conf', function($conf) {
		$conf['2days_2'] = $conf ['default'];
		$conf['2days_2']['from_date'] = '2daysAgo';
		$conf['2days_2']['no_elements'] = '5';
		return $conf ;
});


//home los más vistos semana
add_filter('ze_most_visited_conf', function($conf) {
	$conf['weekly'] = $conf ['default'];
	$conf['weekly']['from_date'] = '7daysAgo';
	$conf['weekly']['no_elements'] = '8';
	return $conf ;
});

add_filter('ze_most_visited_output_2days', function ($data) {
	$html = '<ul class="fototextosimple">';

	if (is_array ($data)) {
		while (list (, $entry) = each ($data)) {
			$id_post = $entry['id'];
			$url = $entry['link'];
			$titulo = $entry['title'];
			$html .= '<li>';

			if (get_field('imagen_principal', $id_post) || has_post_thumbnail($id_post) || get_field('imagen_destacada_migracion', $id_post)) {
                if ($imagen = get_field('imagen_principal', $id_post)) {
                    $tamano_imagen = 'thumbnail';
                    $url_imagen = $imagen['sizes'][$tamano_imagen];
                } elseif (has_post_thumbnail($id_post)) {
                    $tamano_imagen = 'thumbnail';
                    $thumb_imagen = wp_get_attachment_image_src(get_post_thumbnail_id($id_post), $tamano_imagen);
                    $url_imagen = $thumb_imagen['0'];
                } elseif (get_field('imagen_destacada_migracion', $id_post)) {
                    $url_imagen = get_field('imagen_destacada_migracion', $id_post);
                }
            } else {
                $url_imagen = s3uri() . '/img/sin_imagen.png';
            }

            $html .= "<a href='{$url}' title='"  . esc_attr($titulo) . "'><div class='img'><img class='lazy' src='{$url_imagen}' alt='{$titulo}' width='380' height='270'></div></a>" . "\n";
   			$html .= '<h4><a href="'.$url.'">'.$titulo.'</a></h4></li>';
		}
	}

	$html .= '</ul>';

	return $html;
});

add_filter('ze_most_visited_output_2days_2', function ($data) {
	$html = '<ul class="fototexto-verti">';

	if (is_array ($data)) {
		while (list (, $entry) = each ($data)) {
			$id_post = $entry['id'];
			$url = $entry['link'];
			$titulo = $entry['title'];
            $lista_categorias = get_the_term_list($id_post, 'categoria', '', ', ');
            $html .= '<li><div>';

            if (get_field('imagen_principal', $id_post) || has_post_thumbnail($id_post) || get_field('imagen_destacada_migracion', $id_post)) {
                if ($imagen = get_field('imagen_principal', $id_post)) {
                    $tamano_imagen = 'formato-xxs';
                    $url_imagen = $imagen['sizes'][$tamano_imagen];
                } elseif (has_post_thumbnail($id_post)) {
                    $tamano_imagen = 'formato-xxs';
                    $thumb_imagen = wp_get_attachment_image_src(get_post_thumbnail_id($id_post), $tamano_imagen);
                    $url_imagen = $thumb_imagen['0'];
                } elseif (get_field('imagen_destacada_migracion', $id_post)) {
                    $url_imagen = get_field('imagen_destacada_migracion', $id_post);
                }

                $html .= "<a href='{$url}' title='{$titulo}'><div class='img'><img class='lazy' src='{$url_imagen}' alt='{$titulo}' width='390' height='260'></div></a>" . "\n";
            }

            if ($lista_categorias) {
                if (!get_field('imagen_principal', $id_post) && !has_post_thumbnail($id_post) && !get_field('imagen_destacada_migracion', $id_post)) {
                    $html .= "<span class='not_cat no_imagen'>{$lista_categorias}</span>" . "\n";
                } else {
                    $html .= "<span class='not_cat'>{$lista_categorias}</span>" . "\n";
                }
            }

			$html .= '<h4><a href="'.$url.'">'.$titulo.'</a></h4></div></li>';
		}
	}

	$html .= '</ul>';

	return $html;
});

add_filter('ze_most_visited_output_weekly', function ($data) {
	$html = '<ul class="fototextosimple">';

	if (is_array ($data)) {
		$i = 0;

		while (list (, $entry) = each ($data)) {
			if ($i == 2) {
                $html .= "<div class='column four'>" . "\n";
                $html .= "</div>" . "\n";
                $html .= "</div>" . "\n";
                $i++;
            }

            if ($i % 3 == 0) {
                $html .= "<div class='row'>" . "\n";
            }

            $id_post = $entry['id'];
            $url = $entry['link'];
			$titulo = $entry['title'];
            $lista_categorias = get_the_term_list($id_post, 'categoria', '', ', ');

            $html .= "<div class='column four'>" . "\n";
            $html .= "<article class='noticia_bl " . wp_get_post_terms($id_post, 'categoria', array('fields' => 'slugs'))[0] . "'>" . "\n";
            $html .= "<div class='bl'>" . "\n";

            if (get_field('key_video', $id_post)) {
                $clase = ' puntero noticia-imagen-' . $id_post;
            } else {
                $clase = '';
            }

            $html .= "<a href='" . $url . "'><div class='img{$clase}'>" . "\n";

            if (get_field('fotogaleria', $id_post)) {
                $html .= "<span class='fotogaleria'></span>" . "\n";
            }

            if (get_field('imagen_principal', $id_post) || has_post_thumbnail($id_post) || get_field('imagen_destacada_migracion', $id_post)) {
                if ($imagen = get_field('imagen_principal', $id_post)) {
                    $tamano_imagen = 'formato-xxs';
                    $url_imagen = $imagen['sizes'][$tamano_imagen];
                } elseif (has_post_thumbnail($id_post)) {
                    $tamano_imagen = 'formato-xxs';
                    $thumb_imagen = wp_get_attachment_image_src(get_post_thumbnail_id($id_post), $tamano_imagen);
                    $url_imagen = $thumb_imagen['0'];
                } elseif (get_field('imagen_destacada_migracion', $id_post)) {
                    $url_imagen = get_field('imagen_destacada_migracion', $id_post);
                }
            } else {
                $url_imagen = s3uri() . '/img/sin_imagen.png';
            }

            $html .= "<img class='lazy' src='" . s3uri() . "/img/transparent.gif' data-original='" . $url_imagen . "' alt='" . $titulo . "' width='390' height='260' />" . "\n";
            $html .= "</div></a>" . "\n";
            $html .= "<div class='text'>" . "\n";

            if (get_the_term_list($id_post, 'categoria', '', ', ')) {
                $html .= "<span class='not_cat'>" . get_the_term_list($id_post, 'categoria', '', ', ') . "</span>" . "\n";
            }

            $html .= "<h3><a href='" . $url . "'>" . $titulo . "</a></h3>" . "\n";
            $html .= "</div>" . "\n";
            $html .= "</div>" . "\n";
            $html .= "</article>" . "\n";
            $html .= "</div>" . "\n";

            if (($i + 1) % 3 == 0 && $i > 0) {
                $html .= "</div>" . "\n";
            }

            $i++;
		}
	}

	$html .= '</ul>';

	return $html;
});

/**
 * Cache de la imagen destacada
 * @param  array  $posts Los posts
 * @param  object $query La query
 * @return array         Los posts modificados
 */
/*function cache_post_thumbnails($posts, $query) {
  if (!$query->is_main_query() && $query->get('update_post_thumbnail_cache')) {
    update_post_thumbnail_cache($query);
  }

  return $posts;
}
add_filter('the_posts', 'cache_post_thumbnails', 10, 2);*/

/**
 * Anade metodos de contacto para los usuarios
 * @param  array $metodos_contacto Los metodos de contacto
 * @return array                   Los metodos de contacto modificados
 */
function anadir_metodos_contacto_usuario($metodos_contacto) {
	$metodos_contacto['twitter'] = 'Usuario Twitter (sin @)';

	return $metodos_contacto;
}
add_filter('user_contactmethods','anadir_metodos_contacto_usuario', 10, 1);

/**
 * Guarda el HTML de los Insertos publicitarios en la tabla wp_options
 * @param  int $post_id El id del post de portada
 */
/**
 * Guarda el HTML de los Insertos publicitarios en la tabla wp_options
 * @param  int $post_id El id del post de portada
 */
function guardar_insertos_publicitarios($post_id) {
	global $wpdb;

	if (get_post_type($post_id) != 'portada') {
		return;
	}

	if (have_rows('modulos', $post_id)) {
		while (have_rows('modulos', $post_id)) {
			the_row();
			$modulo_tipo = get_row_layout();

			switch ($modulo_tipo) {
				case 'insertos_publicitarios':
					if (have_rows('insertos_publicitarios')) {
						$lista_paginas = [];

						while (have_rows('insertos_publicitarios')) {
							the_row();

							$lista_paginas[] = get_sub_field('numero_pagina');
						}

						$contexto = stream_context_create(array(
								'http' => array(
							   		'timeout' => 30
							   	)
							)
						);
						
						$html = '<div class="row"><header><h3>Insertos Publicitarios</h3></header></div><div style="text-align: center; padding: 2rem 0;" class="row"><div class="slider-insertos-publicitarios owl-carousel">';
						$fecha_edicion = date("Ymd");
						foreach ($lista_paginas as $numero_pagina) {
							if ($numero_pagina < 10) {
								$numero_pagina_ceros = '0' . $numero_pagina;
							} elseif ($numero_pagina < 100) {
								$numero_pagina_ceros = $numero_pagina;
							} else {
								$numero_pagina_ceros = $numero_pagina;
							}
							$html .= '<div><a href="http://diariolacuarta.pressreader.com/la-cuarta-insertos/" target="_blank" title="Ver en pressreader"><img class="lazyOwl" src="' . s3uri() . '/img/transparent.gif" data-src="http://cache-thumb1.pressdisplay.com/pressdisplay/docserver/getimage.aspx?cid=ebbw&date=&width=180&page=' . $numero_pagina_ceros .'&nocache='.date('dmYH').'" title="Página ' . $numero_pagina . '" alt="Página ' . $numero_pagina . '" height="333" width="267"></a></div>';

						}

						$html .= '</div></div><script>jq(document).ready(function() { jq(".slider-insertos-publicitarios").owlCarousel({ items: 4, lazyLoad: true, addClassActive: true, navigation: true});});</script>';

						update_site_option('html_insertos_publicitarios', $html);
					}

				break;
			}
		}
	}
}
function guardar_insertos_publicitarios_dos($post_id) {
	global $wpdb;

	if (get_post_type($post_id) != 'portada') {
		return;
	}

	if (have_rows('modulos', $post_id)) {
		while (have_rows('modulos', $post_id)) {
			the_row();
			$modulo_tipo = get_row_layout();

			switch ($modulo_tipo) {
				case 'insertos_segundo_bloque':
					if (have_rows('insertos_segundo_bloque')) {
						$lista_paginas = [];

						while (have_rows('insertos_segundo_bloque')) {
							the_row();

							$lista_paginas[] = get_sub_field('numero_pagina');
						}

						$contexto = stream_context_create(array(
								'http' => array(
							   		'timeout' => 30
							   	)
							)
						);
						
						$html = '<div class="row"><header><h3>Insertos Publicitarios</h3></header></div><div style="text-align: center; padding: 2rem 0;" class="row"><div class="slider-insertos-publicitarios owl-carousel">';
						$fecha_edicion = date("Ymd");
						foreach ($lista_paginas as $numero_pagina) {
							if ($numero_pagina < 10) {
								$numero_pagina_ceros = '0' . $numero_pagina;
							} elseif ($numero_pagina < 100) {
								$numero_pagina_ceros = $numero_pagina;
							} else {
								$numero_pagina_ceros = $numero_pagina;
							}
							$html .= '<div><a href="http://diariolacuarta.pressreader.com/la-cuarta-insertos/" target="_blank" title="Ver en pressreader"><img class="lazyOwl" src="' . s3uri() . '/img/transparent.gif" data-src="http://cache-thumb1.pressdisplay.com/pressdisplay/docserver/getimage.aspx?cid=ebbw&date=&width=300&page=' . $numero_pagina_ceros .'&nocache='.date('dmYH').'" title="Página ' . $numero_pagina . '" alt="Página ' . $numero_pagina . '" height="333" width="267"></a></div>';

						}

						$html .= '</div></div><script>jq(document).ready(function() { jq(".slider-insertos-publicitarios").owlCarousel({ items: 4, lazyLoad: true, addClassActive: true, navigation: true});});</script>';

						update_site_option('html_insertos_segundo_bloque', $html);
					}

				break;
			}
		}
	}
}



function lacuarta_phpmailer_init( PHPMailer $phpmailer ) {
    $phpmailer->Host = 'manzana.copesa.cl';
    $phpmailer->Port = 25; // could be different
    $phpmailer->Username = 'promocion'; // if required
    $phpmailer->Password = 'promo2013'; // if required
    $phpmailer->SMTPAuth = true; // if required
    //$phpmailer->SMTPSecure = 'tls'; // enable if required, 'tls' is another possible value

    $phpmailer->IsSMTP();
}
add_action( 'phpmailer_init', 'lacuarta_phpmailer_init' );
if (get_current_blog_id() == 1) {
	add_action('save_post', 'guardar_insertos_publicitarios');
}
if (get_current_blog_id() == 1) {
	add_action('save_post', 'guardar_insertos_publicitarios_dos');
}

/**
 * Anade soporte para custom post types para crear contenido AMP
 */

function anade_soporte_custom_post_types_amp() {
    add_post_type_support('noticia', AMP_QUERY_VAR);
}
add_action('amp_init', 'anade_soporte_custom_post_types_amp');

add_filter( 'amp_post_template_analytics', 'xyz_amp_add_custom_analytics' );
function xyz_amp_add_custom_analytics( $analytics ) {
    if ( ! is_array( $analytics ) ) {
        $analytics = array();
    }

    $analytics['xyz-googleanalytics'] = array(
        'type' => 'googleanalytics',
        'attributes' => array(
            // 'data-credentials' => 'include',
        ),
        'config_data' => array(
            'vars' => array(
                'account' => "UA-80728886-11"
            ),
            'triggers' => array(
                'trackPageview' => array(
                    'on' => 'visible',
                    'request' => 'pageview',
                ),
            ),
        ),
    );
	
    return $analytics;
}	

//DOC: https://www.advancedcustomfields.com/resources/acf_add_options_page
if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Funciones Wordpress',
		'menu_title'	=> 'FUNCIONES',
		'menu_slug' 	=> 'l4-funciones-panel',
		'capability'	=> 'edit_posts',
		'position' 		=> 1.1,
		'icon_url' 		=> 'http://'.$_SERVER['HTTP_HOST'].'/wp-content/themes/lacuarta/img/logo-20x20.png',
		'redirect'		=> true
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Paneles Autoadministrables',
		'menu_title'	=> 'Paneles',
		'parent_slug'	=> 'l4-funciones-panel',
	));

	//solo los administradores tienen acceso
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Papel Digital',
		'menu_title'	=> 'Diario',
		'capability'	=> 'edit_themes',
		'parent_slug'	=> 'l4-funciones-panel',
	));

}

//DOC: https://codex.wordpress.org/Quicktags_API
function shortcode_agregar_boton_marcador_toolbar() {
    if(wp_script_is("quicktags")): ?>
        <script type="text/javascript">
            QTags.addButton( 
                "shortcode_marcador_deportes", 
                "Marcador Deporte", 
                callback
            );

            function callback()
            {
                var txt;
			    var identificadorMarcador = prompt("Por favor ingrese el identificador del marcador, EJ: cobresal-huechipato", "cobresal-huechipato");
			    
			    if (identificadorMarcador == null || identificadorMarcador == "") {
			        txt = "SIN-IDENTIFICADOR";
			    } 
			    else {
			        txt = identificadorMarcador;
			    }

                QTags.insertContent("<a href='#' id='"+txt+"' name='"+txt+"' class='marcador-deporte'></a>");
            }
        </script>
    <?php endif;
}
add_action("admin_print_footer_scripts", "shortcode_agregar_boton_marcador_toolbar");


require get_template_directory() . '/inc/ajax.php';
//require get_template_directory() . '/inc/posts_populares.php';
require get_template_directory() . '/inc/publicidad.php';
require get_template_directory() . '/inc/utils.php';
require get_template_directory() . '/inc/acf.php';
require get_template_directory() . '/inc/rest-api.php';
require get_template_directory() . '/inc/cache.php';