<?php
get_header();

if (have_posts()) :
	while (have_posts()) : the_post();
		$ruleta = get_post(get_the_id());
?>

<div id="content" class="portada portada_seccion">
	<section class="modulo superruleta">
		<div class="row">

		<?php
		if (have_rows('diosas')) {
			$genero = 'diosas';
			include(locate_template('portada/modulos/content-superruleta-genero.php'));
		}

		if (have_rows('dioses')) {
			$genero = 'dioses';
			include(locate_template('portada/modulos/content-superruleta-genero.php'));
		}
		?>

			<div class="wrapviewmodal">
				<button id="closemodal">close</button>
				<div id="viewModal"></div>
			</div>
		</div>
	</section>
</div>

<?php
	endwhile;
endif;
?>

<?php get_footer(); ?>