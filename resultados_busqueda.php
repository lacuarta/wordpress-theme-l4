<?php
/*
Template Name: Resultados busqueda
*/

get_header();
?>

<div class="bloque_tags">
    <div class="ficha_tag row">
        <div class="cuerpo central column eight">
            <gcse:searchresults-only></gcse:searchresults-only>
        </div>
        <div class="sidebar column four">

<?php
$publicidad_lateral_derecho = obtener_publicidad('publicida_lateral_derecho');

if ($publicidad_lateral_derecho) :
?>

            <div class="bl_publi">
                <?php echo $publicidad_lateral_derecho; ?>
            </div>

<?php
endif;
?>

            <span class="titulo calentitos">Los más calentitos</span>
            <ul>
                <li><a id="mas-vistos" class="tab-los-mas" href="#" class="tit">Los más vistos</a></li>
                <li class="activo"><a id="mas-mejores" class="tab-los-mas" href="#" class="tit">Los más mejores</a></li>
            </ul>
            <script>
            jq(document).on('click', '.tab-los-mas', function(e) {
                e.preventDefault();

                jq(this).parent().parent().find('.activo').removeClass('activo');
                jq(this).parent().addClass('activo');

                if (jq(this).attr('id') == 'mas-vistos') {
                    jq('.los-mas-mejores').fadeOut();
                    jq('.los-mas-vistos').fadeIn();
                }

                if (jq(this).attr('id') == 'mas-mejores') {
                    jq('.los-mas-vistos').fadeOut();
                    jq('.los-mas-mejores').fadeIn();
                }
            });
            </script>
            <div class="los-mas-vistos" style="display:none;">

<?php
$html = apply_filters('ze_most_visited_html',array('conf'=>'2days_2'));
echo $html;
?>

            </div>
            <div class="los-mas-mejores">
                <?php get_template_part('content-los_mas_mejores', 'page'); ?>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
<script>
jq(document).on('click', '.acordeon', function(e) {
    e.preventDefault();
    var lista = jq(this).parent().next('ul');

    if (!lista.is(':visible')) {
        jq(this).parent().parent().parent().find('ul').slideUp();
        lista.slideDown();
    }
});
</script>
</div>
<?php get_footer(); ?>