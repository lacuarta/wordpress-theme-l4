<?php
date_default_timezone_set('America/Santiago');

$arrayConfigDiario = array(
	'ahora' => date('His'),
	'opciones_diario_hora_de_inicio' => get_field('opciones_diario_hora_de_inicio', 'option'),
	'opciones_diario_hora_de_termino' => get_field('opciones_diario_hora_de_termino', 'option'),
	'opciones_diario_activar_redireccion' => get_field('opciones_diario_activar_redireccion', 'option'),
	'opciones_diario_url_redireccion' => get_field('opciones_diario_url_redireccion', 'option')
);

//si esta activado entramos
if($arrayConfigDiario['opciones_diario_activar_redireccion']){

	//comprobamos que estemos dentro de la franja horaria
	if($arrayConfigDiario['ahora'] > $arrayConfigDiario['opciones_diario_hora_de_inicio'] && $arrayConfigDiario['ahora'] < $arrayConfigDiario['opciones_diario_hora_de_termino']){

		//si tiene url redireccionamos
		if(!empty($arrayConfigDiario['opciones_diario_url_redireccion'])){
			header('Location: ' . $arrayConfigDiario['opciones_diario_url_redireccion']);
		}
	}
}
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

<?php
if (is_home()|| is_singular('portada') || is_tax('categoria') || is_singular('noticia')) :
?>

    <meta http-equiv="refresh" content="300">

<?php
endif;
?>

	<title><?php wp_title(' | ', true, 'right'); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	<link rel="shortcut icon" sizes="16x16 24x24 32x32 48x48 64x64" href="http://d2mhmd1y9dvsiv.cloudfront.net/wp-content/themes/lacuarta/favicon.png">

	<!--[if lt IE 9]>
  	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  	<![endif]-->
	<?php wp_head(); ?>
 	<script>
 	var jq = $.noConflict();

  	jq(document).ready(function() {
  		if (jq('body').css('background-image').search('google') > 0) {
  			jq('body').addClass('publiskin');
  		}

  		jq('img.lazy').lazyload({
  			threshold: 500,
  			failure_limit: 50
  		});

<?php
if (is_home() || is_post_type_archive('galeria') || is_post_type_archive('lacuartav') || is_singular('portada') || is_singular('lacuartatv') || is_tax('categoria')) :
?>

		jq('.igualar').matchHeight();

<?php
endif;
?>
	});

  	// Publicidad
	var googletag = googletag || {};
	googletag.cmd = googletag.cmd || [];

	(function() {
		var useSSL = 'https:' == document.location.protocol;
	    var src = (useSSL ? 'https:' : 'http:') +
	        '//www.googletagservices.com/tag/js/gpt.js';
	    document.write('<scr' + 'ipt src="' + src + '"></scr' + 'ipt>');
	})();

	// Google Analytics
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function() {
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-27744179-44', 'auto');
	ga('send', 'pageview');
	ga('require', 'displayfeatures');

<?php
if (is_home() || is_404() || is_singular('portada') || is_tax('categoria') || is_singular('noticia') || is_post_type_archive('lacuartatv') || is_singular('lacuartatv') || is_post_type_archive('galeria') || is_singular('galeria')) :
?>

    //Cxense
    var cX = cX || {}; cX.callQueue = cX.callQueue || [];
	cX.callQueue.push(['setSiteId', '1145197310351156334']);
	cX.callQueue.push(['sendPageViewEvent']);

<?php
endif;
?>

	</script>

<?php
if (is_home() || is_404() || is_singular('portada') || is_tax('categoria') || is_singular('noticia') || is_post_type_archive('lacuartatv') || is_singular('lacuartatv') || is_post_type_archive('galeria') || is_singular('galeria')) :
?>

	<script type='text/javascript' src='http://cdn.cxense.com/cx.js'></script>

<?php
endif;

if (is_home()) :
?>

	<meta name='cXenseParse:pageclass' content='frontpage'>

<?php
elseif (is_singular('portada') || is_tax('categoria') || is_singular('noticia') || is_post_type_archive('lacuartatv') || is_singular('lacuartatv') || is_post_type_archive('galeria') || is_singular('galeria')) :
	if (is_singular('portada')) {
		if (get_field('es_portada_categoria')) {
			$termino = get_field('categoria');
			$id = $termino->term_id;
			$fecha_modificacion = date('Y-m-d\TH:i:s');
		}
	} elseif (is_tax('categoria')) {
		$id_termino_categoria_padre = get_queried_object()->parent;

		if ($id_termino_categoria_padre == 0) { // Categoria padre
			$id = get_queried_object()->term_id;
		} else { // Subcategoria
			$id = get_queried_object()->parent;
		}
		$fecha_modificacion = date('Y-m-d\TH:i:s');
	} elseif (is_post_type_archive('lacuartatv'))  {
		$id = 'archivecuartatv';
		$fecha_modificacion = get_the_modified_date('Y-m-d\TH:i:s');
	} elseif (is_post_type_archive('galeria'))  {
		$id = 'galeria';
		$fecha_modificacion = get_the_modified_date('Y-m-d\TH:i:s');
	} else {
		$id = get_the_id();
		$fecha_modificacion = get_the_modified_date('Y-m-d\TH:i:s');
	}
?>

	<meta name="cXenseParse:pageclass" content="article">
	<meta name="cXenseParse:recs:articleid" content="<?php echo $id; ?>">
	<meta name="cXenseParse:recs:publishtime" content="<?php echo $fecha_modificacion; ?>">

	<?php
	if (is_singular('noticia')) :
		$terms = get_the_terms(get_the_id(), 'categoria');
		$slug_categoria = $terms[0]->slug;
	?>

	<meta name="cXenseParse:cps-articletype " content="<?php echo $slug_categoria; ?>"/>

	<?php
	endif;
	?>

	<script type="text/javascript" class="teads" async="true" src="//a.teads.tv/page/14445/tag"></script>

<?php
endif;
?>

<?php
if (is_home() || is_404()) {
	$nombre_archivo_js = 'home';
} elseif (is_post_type_archive('galeria') || is_singular('galeria')) {
	$nombre_archivo_js = 'galerias';
} elseif (is_post_type_archive('lacuartatv') || is_singular('lacuartatv')) {
	$nombre_archivo_js = 'lacuartatv';
}

if (is_home() || is_404() || is_post_type_archive('galeria') || is_singular('galeria') || is_post_type_archive('lacuartatv') || is_singular('lacuartatv')) :
?>

	<script src="<?php echo s3uri() . '/js/' . $nombre_archivo_js . '.js'; ?>" type="text/javascript"></script>



<?php
elseif (is_tax('categoria')) :
	echo get_field('script_categoria', 'categoria_' . get_queried_object()->term_id);
elseif (is_singular('portada') && get_field('es_portada_categoria')) :
	echo get_field('script_categoria', 'categoria_' . get_field('categoria')->term_id);
elseif (is_singular('noticia') || $post_type == 'noticia') :
	$terms = get_the_terms(get_the_id(), 'categoria');
	$id_termino_categoria = $terms[0]->term_id;
	$contenido_script = get_field('script_categoria', 'categoria_' . $id_termino_categoria);
	//echo get_field('script_categoria', 'categoria_' . $id_termino_categoria);
		
	if (is_singular('noticia')){
		if (get_field('validador_publicidad', $id_post)) {
			$value = get_field('validador_publicidad', $id_post);
			if($value[0] == 'SI'){
			   $aptoGoogle = 'SI';
			}else{
				$aptoGoogle = 'NO';
			}
		}
	}
	$resultado = str_replace('resultado',$aptoGoogle, $contenido_script);
	echo $resultado;
endif;
?>


</head>
<body <?php body_class(); ?>>
<?php
if (is_home()){
	
	echo '<div id="wrapper" class="wrapper_papel_digital">';

?>
	
<?php
}else{
echo "<div id='wrapper'>";
}

?>


<?php
if (is_home()) {
	get_template_part('content-header-home', 'page');
} elseif (is_singular('publirreportajes')) {
	get_template_part('content-header-publirreportaje', 'page');
} else {
	get_template_part('content-header-interior', 'page');
}
