<?php get_header(); ?>
<div id="content" class="portada portada_seccion">
	<div class="cap">
	<div class="row">
		<h1 class="portadilla">Galerías</h1>
	</div>
	</div>
	<section>
		<div class="row">

<?php
if (have_posts()) :
	while (have_posts()) : the_post();
	?>

			<div class="column four igualar">
				<article>
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
						<div class="img">

		<?php
		if (has_post_thumbnail()) {
			$tamano_imagen = 'thumbnail';
			$thumb_imagen = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()), $tamano_imagen);
			$url_imagen = $thumb_imagen['0'];
		} else {
			$imagenes = get_field('galeria');

			if ($imagenes) {
				$tamano = 'thumbnail';
				$url_imagen = $imagenes[0]['sizes'][$tamano];
			}
		}

		if (has_post_thumbnail() || $imagenes) :
		?>

							<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $url_imagen; ?>" alt="<?php the_title(); ?>" width="380" height="270" />

		<?php
		endif;
		?>

						</div>
					</a>
					<h3><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>
				</article>
			</div>

<?php
	endwhile;
	wp_reset_query();
endif;
?>

		</div>
		<?php wp_pagenavi(); ?>
	</section>
</div>
<?php get_footer(); ?>