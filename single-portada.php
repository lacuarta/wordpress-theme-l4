<!--
 * CATEGORIAS PORTADAS NUEVAS (ID)
 * 4 Ruedas [DEV: 130 | PRO: 19870]
 * La Matrix [DEV: 131 | PRO: 14896]
 * L4TV [DEV: 91 | PRO: 91]
-->
<?php get_header(); ?>
<?php if (have_posts()): ?>
	<?php while (have_posts()): ?>
		<?php the_post(); ?>
		<?php $es_portada_categoria = get_field('es_portada_categoria'); ?>

		<?php if($es_portada_categoria): ?>
			<?php $taxonomia_actual = 'categoria'; ?>
			<?php $id_termino_categoria_actual = get_field('categoria')->term_id; ?>
	
			<div id="content" class="portada portada_seccion">
				<?php 
				switch($id_termino_categoria_actual) {
					case 130:
					case 19870:
						//Portada 4 Ruedas
						include(locate_template('inc/portada-4ruedas.php'));
						break;

					case 131:
					case 14896:
						// Portada La Matrix
						include(locate_template('inc/portada-lamatrix.php'));
						break;

					case 91:
						//Portada L4TV
						include(locate_template('inc/portada-l4tv.php'));
						break;
					
					default:
						include(locate_template('inc/marcadores_deportes.php'));
						include(locate_template('content-portada-categoria.php'));
						break;
				} 
				?>
			</div>

		<?php else: ?>

			<div id="content" class="portada">
				<?php get_template_part('content-portada', 'page'); ?>
			</div>

		<?php endif; ?>
	<?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>