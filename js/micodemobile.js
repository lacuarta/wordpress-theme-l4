function configruleta1(elem) {
	var $roulette=jq(elem).find('.roulette');
	var $spinner=jq(elem).find('.mobile');
	var jQr = $roulette.fortune(pricesmobile);

	var clickHandler = function() {
	 	$spinner.off('click');

		jQr.spin().done(function(price) {
			$spinner.on('click', clickHandler);
			window.location.href=price.name;
		});
 	};

	$spinner.on('click', clickHandler);
}

function configruleta2(elem) {
	var $roulette=jq(elem).find('.roulette');
	var $spinner=jq(elem).find('.mobile');
	var jQr = $roulette.fortune(pricesmobile2);

	var clickHandler = function() {
	 	$spinner.off('click');

		jQr.spin().done(function(price) {
			$spinner.on('click', clickHandler);
			window.location.href=price.name;
		});
 	};

	$spinner.on('click', clickHandler);
}