(function(jQ) {

  jQ.fn.fortune = function(args) {

    if (args === undefined) {
      throw(new Error("You must define the options.pricesmobile"));
    }

    var options = jQ.extend({}, {
      pricesmobile: args,
      duration: 5000,
      separation: 1,
      min_spins: 10,
      max_spins: 30,
      onSpinBounce: function() {}
    }, args);

    var fortune = this;
    var pricesmobile_amount = Array.isArray(options.pricesmobile)?options.pricesmobile.length:options.pricesmobile;
    var pricesmobile_delta = 360 / pricesmobile_amount;
    var is_spinning = false;

    fortune.spin = function(price) {
      price = typeof price === "number"?price:Math.floor(Math.random() * pricesmobile_amount);
      var deferred = jQ.Deferred();
      var position = Math.floor(pricesmobile_delta * (price - 1/2) + randomBetween(options.separation, pricesmobile_delta - options.separation));
      var spins = randomBetween(options.min_spins, options.max_spins);
      var final_position = 360 * spins + position;
      var prev_position = 0;
      var is_bouncing = false;

      is_spinning = true;

      fortune
      .css({
        "transform": "rotate(" + final_position + "deg)",
        "-webkit-transform": "rotate(" + final_position + "deg)",
        "transition": "transform " + options.duration + "ms cubic-bezier(.17,.67,.12,.99)",
        "-webkit-transition": "-webkit-transform " + options.duration + "ms cubic-bezier(.17,.67,.12,.99)"
      })
      .siblings('.spin').removeClass('bounce');

      var bounceSpin = function() {
        var curPosition = Math.floor(getRotationDegrees(fortune)),
        mod = Math.floor((curPosition + pricesmobile_delta*0.5) % pricesmobile_delta),
        diff_position,
        position_threshold = pricesmobile_delta/5,
        distance_threshold = pricesmobile_delta/10;

        prev_position = Math.floor(curPosition < prev_position ? prev_position - 360 : prev_position);
        diff_position = curPosition - prev_position;

        if ((mod < position_threshold && diff_position < distance_threshold) ||
            (mod < position_threshold*3 && diff_position >= distance_threshold)) {
          if (!is_bouncing) {
            fortune.siblings('.spin').addClass('bounce');
            options.onSpinBounce(fortune.siblings('.spin'));
            is_bouncing = true;
          }
        } else {
          fortune.siblings('.spin').removeClass('bounce');
          is_bouncing = false;
        }

        if (is_spinning) {
          prev_position = curPosition;
          requestAnimationFrame(bounceSpin);
        }
      };

      var animSpin = requestAnimationFrame(bounceSpin);

      setTimeout(function() {
        fortune
        .css({
          "transform": "rotate(" + position + "deg)",
          "-webkit-transform": "rotate(" + position + "deg)",
          "transition": "",
          "-webkit-transition": ""
        })
        .siblings('.spin').removeClass('bounce');

        cancelAnimationFrame(animSpin);
        deferred.resolve(Array.isArray(options.pricesmobile)?options.pricesmobile[price]:price);
        is_spinning = false;
      }, options.duration);

      return deferred.promise();
    };

    var getRotationDegrees = function(obj) {
      var angle = 0,
      matrix = obj.css("-webkit-transform") ||
        obj.css("-moz-transform")    ||
        obj.css("-ms-transform")     ||
        obj.css("-o-transform")      ||
        obj.css("transform");
      if (matrix !== 'none') {
        var values = matrix.split('(')[1].split(')')[0].split(','),
        a = values[0],
        b = values[1],
        radians = Math.atan2(b, a);

        if ( radians < 0 ) {
          radians += (2 * Math.PI);
        }

        angle = Math.round( radians * (180/Math.PI));
      }

      return angle;
    };

    var randomBetween = function(min, max) {
      return Math.floor(Math.random() * (max - min + 1)) + min;
    };

    return fortune;
  };
}) (jQuery);