jQuery(document).ready(function ($) {
	$('#closemodal').on('click', function(){
		$('.wrapviewmodal').fadeOut('fast');
	});
});

function configruleta1(elem) {
	var $roulette=jq(elem).find('.roulette');
	var $spinner=jq(elem).find('.spinner');
	var $span=jq(elem).find('.spinner span');
	var jQr = $roulette.fortune(prices);

	var clickHandler = function() {
	 	$spinner.off('click');
	 	$span.hide();

		jQr.spin().done(function(price) {
			$spinner.on('click', clickHandler);
			$span.show();
			jq('#viewModal').html(price.name);
			jq('.wrapviewmodal').show('show');
		});
 	};

 	$spinner.on('click', clickHandler);
}

function configruleta2(elem) {
	var $roulette=jq(elem).find('.roulette');
	var $spinner=jq(elem).find('.spinner');
	var $span=jq(elem).find('.spinner span');
	var jQr = $roulette.fortune(prices2);

	var clickHandler = function() {
	 	$spinner.off('click');
	 	$span.hide();

		jQr.spin().done(function(price) {
			$spinner.on('click', clickHandler);
			$span.show();
			jq('#viewModal').html(price.name);
			jq('.wrapviewmodal').show('show');
		});
	};

	$spinner.on('click', clickHandler);
}