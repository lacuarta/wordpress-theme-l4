googletag.cmd.push(function() {
    var mappingBigbanner = googletag.sizeMapping().
        addSize([980, 735 ], [970, 90]).
        addSize([768, 576 ], [728, 90]).
        addSize([1, 1], [300, 50]).
        build();

     var mappingFullbanner = googletag.sizeMapping().
        addSize([980, 735 ], [970, 250]).
        addSize([768, 576 ], [728, 90]).
        addSize([1, 1 ], [300, 250]).
        build();

    googletag.defineSlot('/124506296/La_Cuarta/LC_4tv/4tv_970x90', [[970, 90], [728, 90], [300, 50]], 'div-gpt-ad-1468339996166-0').defineSizeMapping(mappingBigbanner).addService(googletag.pubads());
    googletag.defineSlot('/124506296/La_Cuarta/LC_4tv/4tv_300x250-D-net', [300, 250], 'div-gpt-ad-1468339996166-4').addService(googletag.pubads());
    googletag.defineSlot('/124506296/La_Cuarta/LC_4tv/4tv_970x250-A', [[970, 250], [728, 90],[300, 250]], 'div-gpt-ad-1468339996166-5').defineSizeMapping(mappingFullbanner).addService(googletag.pubads());
    googletag.defineSlot('/124506296/La_Cuarta/LC_4tv/4tv_970x250-B-net', [[970, 250], [728, 90],[300, 250]], 'div-gpt-ad-1468339996166-6').defineSizeMapping(mappingFullbanner).addService(googletag.pubads());
    googletag.defineSlot('/124506296/La_Cuarta/LC_4tv/4tv_barra-itt', [1, 1], 'div-gpt-ad-1468339996166-7').addService(googletag.pubads());
    googletag.defineOutOfPageSlot('/124506296/La_Cuarta/LC_4tv/4tv_itt',  'div-gpt-ad-1468339996166-8').addService(googletag.pubads());
    googletag.defineOutOfPageSlot('/124506296/La_Cuarta/LC_4tv/4tv_overlay', 'div-gpt-ad-1468339996166-9').addService(googletag.pubads());
    googletag.defineOutOfPageSlot('/124506296/La_Cuarta/LC_4tv/4tv_skin', 'div-gpt-ad-1468339996166-10').addService(googletag.pubads());
    googletag.defineSlot('/124506296/La_Cuarta/LC_4tv/4tv_skycraper', [[120, 600], [160, 600]], 'div-gpt-ad-1468339996166-11').addService(googletag.pubads());
    googletag.pubads().setTargeting('CxSegments',cX.getUserSegmentIds({ persistedQueryId: '1e71f5d217d2466a2c28a8f572d1e7cdb635b306' }));
    googletag.pubads().enableSingleRequest();
    googletag.pubads().enableSyncRendering();
    googletag.enableServices();
});