googletag.cmd.push(function() {
    var mappingBigbanner = googletag.sizeMapping().
        addSize([1024, 768 ], [970, 90]).
        addSize([768, 600 ], [728, 90]).
        addSize([1, 1 ], [300, 50]).
        build();

    var mappingFullbanner = googletag.sizeMapping().
        addSize([1024, 768 ], [970, 250]).
        addSize([768, 600 ], [728, 90]).
        addSize([1, 1 ], [300, 250]).
        build();

    googletag.defineSlot('/124506296/La_Cuarta/LC_portada/portada_970x90', [[728, 90], [970, 90], [300, 50]], 'div-gpt-ad-1468338424102-0').defineSizeMapping(mappingBigbanner).setForceSafeFrame(true).addService(googletag.pubads());
    googletag.defineSlot('/124506296/La_Cuarta/LC_portada/portada_300x100-A', [300, 100], 'div-gpt-ad-1468338424102-1').setForceSafeFrame(true).addService(googletag.pubads());
    googletag.defineSlot('/124506296/La_Cuarta/LC_portada/portada_300x250-A', [300, 250], 'div-gpt-ad-1468338424102-2').setForceSafeFrame(true).addService(googletag.pubads());
    googletag.defineSlot('/124506296/La_Cuarta/LC_portada/portada_970x250-A', [[970, 250], [300, 250], [728, 90]], 'div-gpt-ad-1468338424102-3').defineSizeMapping(mappingFullbanner).setForceSafeFrame(true).addService(googletag.pubads());
    googletag.defineSlot('/124506296/La_Cuarta/LC_portada/portada_300x250-B', [300, 250], 'div-gpt-ad-1468338424102-4').setForceSafeFrame(true).addService(googletag.pubads());
    googletag.defineSlot('/124506296/La_Cuarta/LC_portada/portada_300x250-C', [300, 250], 'div-gpt-ad-1468338424102-5').setForceSafeFrame(true).addService(googletag.pubads());
    googletag.defineSlot('/124506296/La_Cuarta/LC_portada/portada_300x250-D', [300, 250], 'div-gpt-ad-1468338424102-6').setForceSafeFrame(true).addService(googletag.pubads());
    googletag.defineSlot('/124506296/La_Cuarta/LC_portada/portada_970x250-B', [[970, 250], [300, 250], [728, 90]], 'div-gpt-ad-1468338424102-7').defineSizeMapping(mappingFullbanner).setForceSafeFrame(true).addService(googletag.pubads());
    googletag.defineSlot('/124506296/La_Cuarta/LC_portada/portada_300x250-E', [300, 250], 'div-gpt-ad-1468338424102-8').setForceSafeFrame(true).addService(googletag.pubads());
    googletag.defineSlot('/124506296/La_Cuarta/LC_portada/portada_300x250-F', [300, 250], 'div-gpt-ad-1468338424102-9').setForceSafeFrame(true).addService(googletag.pubads());
    googletag.defineSlot('/124506296/La_Cuarta/LC_portada/portada_300x100-B', [300, 100], 'div-gpt-ad-1468338424102-10').setForceSafeFrame(true).addService(googletag.pubads());
    googletag.defineSlot('/124506296/La_Cuarta/LC_portada/portada_300x250-G', [300, 250], 'div-gpt-ad-1468338424102-11').setForceSafeFrame(true).addService(googletag.pubads());
    googletag.defineSlot('/124506296/La_Cuarta/LC_portada/portada_970x250-C', [[970, 250], [300, 250], [728, 90]], 'div-gpt-ad-1468338424102-12').defineSizeMapping(mappingFullbanner).setForceSafeFrame(true).addService(googletag.pubads());
    googletag.defineSlot('/124506296/La_Cuarta/LC_portada/portada_300x250-H-net', [300, 250], 'div-gpt-ad-1468338424102-13').setForceSafeFrame(true).addService(googletag.pubads());
    googletag.defineSlot('/124506296/La_Cuarta/LC_portada/portada_970x250-D', [[970, 250], [300, 250], [728, 90]], 'div-gpt-ad-1468338424102-14').defineSizeMapping(mappingFullbanner).setForceSafeFrame(true).addService(googletag.pubads());
    googletag.defineSlot('/124506296/La_Cuarta/LC_portada/portada_970x250-E-net', [[970, 250], [300, 250], [728, 90]], 'div-gpt-ad-1468338424102-15').defineSizeMapping(mappingFullbanner).setForceSafeFrame(true).addService(googletag.pubads());
    googletag.defineSlot('/124506296/La_Cuarta/LC_portada/portada_barra-itt', [1, 1], 'div-gpt-ad-1468338424102-16').addService(googletag.pubads());
    googletag.defineOutOfPageSlot('/124506296/La_Cuarta/LC_portada/portada_itt', 'div-gpt-ad-1468338424102-17').addService(googletag.pubads());
    googletag.defineOutOfPageSlot('/124506296/La_Cuarta/LC_portada/portada_overlay', 'div-gpt-ad-1468338424102-18').addService(googletag.pubads());
    googletag.defineOutOfPageSlot('/124506296/La_Cuarta/LC_portada/portada_skin', 'div-gpt-ad-1468338424102-19').addService(googletag.pubads());
    googletag.defineOutOfPageSlot('/124506296/La_Cuarta/LC_portada/Portada_Header_scroll', 'div-gpt-ad-1496439657575-0').addService(googletag.pubads());
    googletag.defineSlot('/124506296/La_Cuarta/LC_portada/portada_skycraper', [[120, 600], [160, 600]], 'div-gpt-ad-1468338424102-20').setForceSafeFrame(true).addService(googletag.pubads());
    googletag.defineSlot('/124506296/La_Cuarta/LC_portada/portada_300x100', [300, 100], 'div-gpt-ad-1468338424102-21').setForceSafeFrame(true).addService(googletag.pubads());
    googletag.defineSlot('/124506296/La_Cuarta/LC_portada/portada_300x250', [300, 250], 'div-gpt-ad-1468338424102-22').setForceSafeFrame(true).addService(googletag.pubads());
    googletag.defineSlot('/124506296/La_Cuarta/LC_portada/portada_skycraper-B', [[120, 600], [160, 600]], 'div-gpt-ad-1468338424102-23').setForceSafeFrame(true).addService(googletag.pubads());
    googletag.pubads().setTargeting('CxSegments',cX.getUserSegmentIds({ persistedQueryId: '1e71f5d217d2466a2c28a8f572d1e7cdb635b306' }));
    googletag.pubads().enableSingleRequest();
    googletag.pubads().enableSyncRendering();
    googletag.enableServices();
});