jQuery(document).ready(function($) {
	$(".bot_nav").click(function() {
		$(".subheader .nav_v").toggleClass("muestra");
		$(".bot_nav").toggleClass("activo");

		$('#menu-superior-horizontal li').each(function() {
		    if ($('a', this).attr('href') === window.location.href) {
		        $(this).addClass('menu-item-type-custom');
		    } else {
		     	$(this).removeClass('menu-item-type-custom');
		    }
		});

		$('#menu-superior-horizontal li').on('click', function() {
		 	$('#menu-superior-horizontal li').removeClass('menu-item-type-custom');
		   	$(this).addClass('menu-item-type-custom');
		});
	});

	// Cierra el menu sticky si se hace click en cualquier lugar de la web
	$(document).click(function() {
	    $(".subheader .nav_v").removeClass("muestra");
	});

	$(".bot_nav").click(function(e) {
	    e.stopPropagation();

	    return false;
	});

  	// Google Custom Search
  	(function() {
	    var cx = '017661939087507927274:x0uevcwqbxg';
	    var gcse = document.createElement('script');
	    gcse.type = 'text/javascript';
	    gcse.async = true;
	    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
	    var s = document.getElementsByTagName('script')[0];
	    s.parentNode.insertBefore(gcse, s);
	})();

	$(".bot_buscar").click(function() {
	    $(".b_google").addClass("muestra");
	    $(".b_google_negro").addClass("muestra");
	});

   	$(".b_google .cerrar").click(function() {
		$(".b_google").removeClass("muestra");
	    $(".b_google_negro").removeClass("muestra");
	});
	
	//$('#textLiq').hide();
	//$('#textBrut').hide();
	$('#valor').keyup(function(){
		this.value = (this.value + '').replace(/[^0-9]/g, '');
		var val = $('#valor').val();
  		var bruto = 0.9;
  		var liquido = 1.1111111111111112;
  		var totBruto = val*bruto;
  		var totLiqui = val*liquido;
  		totLiqui = Math.round(totLiqui);
  		totBruto = Math.round(totBruto);
        totLiqui = totLiqui.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        totBruto = totBruto.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

  		$('#crea-liq').text(totLiqui);
  		$('#rec-liq').text(val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
  		$('#crea-brut').text(val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
  		$('#rec-brut').text(totBruto);
		
	})
	    $('#valor').val("");
		
		//$('#segundotbody').hide();
	$('#btntabla').click(function() {
  $('#btntabla').hide();
  $('#segundotbody').show();
});	
	
	
});

(function($, window, document) {
	$(function() {

		$('.sky').parents('#wrapper').addClass('con-sky');

	});//---fin:ready
}(window.jQuery, window, document));



