<article class="bl_tag noticia_bl column four igualar <?php echo wp_get_post_terms(get_the_id(), 'categoria', array('fields' => 'slugs'))[0]; ?>">

<?php
if (get_field('imagen_principal') || has_post_thumbnail() || get_field('imagen_destacada_migracion')) :
	if ($imagen = get_field('imagen_principal')) :
		$tamano_imagen = 'formato-xxs';
		$url_imagen = $imagen['sizes'][$tamano_imagen];
	elseif (has_post_thumbnail()) :
		$tamano_imagen = 'formato-xxs';
		$thumb_imagen = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()), $tamano_imagen);
		$url_imagen = $thumb_imagen['0'];
	elseif (get_field('imagen_destacada_migracion')) :
		$url_imagen = get_field('imagen_destacada_migracion');
	endif;
?>

	<div class="bl">
		<div class="img">
			<a href="<?php the_permalink(); if (isset($seccion)) : echo $seccion; endif; ?>"><img src="<?php echo $url_imagen; ?>" alt="<?php the_title(); ?>" width="320" height="190" /></a>
		</div>

<?php
endif;
?>
		<div class="text">
			<div class="not_cat">
				<?php echo get_the_term_list(get_the_id(), 'categoria', '', ', '); ?>
			</div>
			<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
			<div class="entradilla">
				<?php the_field('entradilla', get_the_id()); ?>
			</div>
			<ul class="redes row">
				<li class="facebook">
					<a href="https://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" title="Compartir en Facebook" target="_blank">Facebook</a>
				</li>
				<li class="twitter">
					<a href="https://twitter.com/share?url=<?php echo wp_get_shortlink(get_the_id()); ?>&via=lacuarta&text=<?php echo urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8')); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" title="Compartir en Twitter" target="_blank">Twitter</a>
				</li>
			</ul>
		</div>
	</div>
</article>