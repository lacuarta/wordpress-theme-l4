<?php
get_header();

$nombre_tag = $wp_query->queried_object->name;
$numero_posts_tag = $wp_query->queried_object->count;
?>

<div class="row">
    <div class="bloque_tags portada portada_seccion">
		<div class="row">
			<h1 class="portadilla"><span>Tag:</span> <?php echo $nombre_tag; ?></h1>
		</div>

<?php
if (have_posts()) :
	while (have_posts()) : the_post();
		get_template_part('content-etiqueta-noticia', 'page');
	endwhile;
endif;
?>

	</div>

<?php
wp_pagenavi();
wp_reset_query();
?>

</div>
<script>
jq(document).ready(function() {
	  jq('.igualar').matchHeight();
});
</script>
<?php get_footer(); ?>