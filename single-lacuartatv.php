<?php get_header(); ?>
<div id="content" class="portada lacuartatv">

<?php
$id_post = get_the_id();

$noticias = wp_cache_get('lacuarta_slider_lacuartatv');

if (false === $noticias) {
	$args = array(
		'post_type'                   => 'lacuartatv',
		'posts_per_page'              => 12,
		'post_status'                 => 'publish',
		'no_found_rows'               => true,
		'cache_results'               => false,
		'update_post_thumbnail_cache' => true
	);
	$noticias = new WP_Query($args);

	set_transient('lacuarta_slider_lacuartatv', $noticias, 0);
	wp_cache_set('lacuarta_slider_lacuartatv', $noticias);
}

if ($noticias->have_posts()) :
?>

	<div class="row">
		<p class="titseccion">La cuarta TV</p>
	</div>
	<div>
		<div id="slider-lacuartatv" class="owl-carousel wrapper row">

	<?php
	while ($noticias->have_posts()) : $noticias->the_post();
	?>

			<div class="item igualar<?php if (get_the_id() == $id_post) : echo ' seleccionada'; endif; ?>">

		<?php
		if (get_field('imagen_principal') || has_post_thumbnail()) :
			if ($imagen = get_field('imagen_principal')) :
				$tamano_imagen = 'thumbnail';
				$url_imagen = $imagen['sizes'][$tamano_imagen];
			elseif (has_post_thumbnail()) :
				$tamano_imagen = 'thumbnail';
				$thumb_imagen = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()), $tamano_imagen);
				$url_imagen = $thumb_imagen['0'];
			endif;
		?>

				<a href="<?php the_permalink();?>">
					<div>
						<span class="video"></span>
						<img class="lazyOwl" src="<?php echo s3uri(); ?>/img/transparent.gif" data-src="<?php echo $url_imagen; ?>" alt="<?php the_title(); ?>" width="380" height="270">
					</div>
					<p><?php the_title(); ?></p>
				</a>

		<?php
		endif;
		?>

			</div>

	<?php
	endwhile;
	?>

		</div>
		<script>
		jq(document).ready(function() {
		    jq("#slider-lacuartatv").owlCarousel({
		        lazyLoad: true,
		    	navigation: true,
		    	responsiveClass:true,
		    	itemsCustom : [
			        [0, 2],
			        [400, 3],
			        [600, 4],
			        [800, 5],
			        [1200, 6]
		      	],
			});
		});
		</script>
	</div>

<?php
	wp_reset_postdata();
endif;

$publicidad_pre_video = obtener_publicidad('publicidad_pre_video');

if ($publicidad_pre_video) :
?>

	<div class="bl_publi_pie row">
		<?php echo $publicidad_pre_video; ?>
	</div>

<?php
endif;
?>

	<div id="video-actual" class="row">

<?php
if (have_posts()) :
	while (have_posts()) : the_post();
		get_template_part('content-lacuartatv-video_actual');
	endwhile;
	wp_reset_postdata();
endif;
?>

	</div>

<?php
$publicidad_post_video = obtener_publicidad('publicidad_post_video');

if ($publicidad_post_video) :
?>

	<div class="bl_publi_pie row">
		<?php echo $publicidad_post_video; ?>
	</div>

<?php
endif;
?>

</div>
<?php get_footer(); ?>