<?php
get_header();

if (get_field('utiliza_opta')) :
	wp_enqueue_style('opta-style', 'http://widget.cloud.opta.net/2.0/css/widgets.opta.css');
	wp_enqueue_script('opta-script', 'http://widget.cloud.opta.net/2.0/js/widgets.opta.js');
?>

<script>
var _optaParams = {
    custID:     '83f090439770a6e6cc0cb324504524e7',
    language:   'es'
};
</script>

<?php
endif;
?>

<div id="content" class="portada portada_seccion especial">
	<div class="row cab_especial">

<?php
$id_imagen_cabecera = get_field('imagen_cabecera', 'especial_' . get_field('tipo_especial'));

if ($id_imagen_cabecera) :
	$tamano = 'full';
	$thumb = wp_get_attachment_image_src($id_imagen_cabecera, $tamano)[0];
?>

		<img class="lazy" src="<?php echo s3uri(); ?>/img/transparent.gif" data-original="<?php echo $thumb; ?>" alt="<?php the_title(); ?>" />

<?php
endif;

if (have_rows('menu', 'especial_' . get_field('tipo_especial'))) :
?>

	<ul>

	<?php
	while (have_rows('menu', 'especial_' . get_field('tipo_especial'))) : the_row();
	?>

		<li><a href="<?php the_sub_field('url'); ?>"><?php the_sub_field('titulo'); ?></a></li>

	<?php
	endwhile;
	?>

	</ul>

<?php
endif;
?>

	</div>
	<?php include(locate_template('content-portada.php')); ?>
</div>
<?php get_footer(); ?>